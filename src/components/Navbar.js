import React, { Fragment } from "react";
import { NavLink } from "react-router-dom";
import { NAVBAR } from "../assets/json/list";
import { logoutUser } from "../accessors/user";
import { CONFIG } from "../app.config";
import Echo from "laravel-echo";
import MedisparkLogo from "../assets/img/logo.png";
import TextOnlyMedisparkLogo from "../assets/img/text_only_logo.png";
import Tippy from "@tippyjs/react";
import "tippy.js/animations/perspective.css";

const Navbar = (props) => {
  // // get the current user details
  // let token = localStorage.getItem("token");

  // // set options for laravel echo
  // const options = {
  //   broadcaster: "pusher",
  //   key: CONFIG.PUSHER_APP_KEY,
  //   cluster: "eu",
  //   forceTLS: CONFIG.PUSHER_APP_TLS,
  //   authEndpoint: CONFIG.PUSHER_APP_ENDPOINT,
  //   // As I'm using JWT tokens, I need to manually set up the headers.
  //   auth: {
  //     headers: {
  //       Authorization: `Bearer ${token}`,
  //       Accept: "application/json",
  //     },
  //   },
  // };

  // const echo = new Echo(options);

  // console.log(echo);

  // echo
  //   .private("App.Models.User" + user.id)
  //   .listen(
  //     ".Illuminate\\Notifications\\Events\\BroadcastNotificationCreated",
  //     (notification) => {
  //       console.log(notification.type);
  //     }
  //   );

  const { user } = props;

  function handleLogout() {
    // delete token
    localStorage.removeItem("token");
    console.log(user);

    logoutUser(user.id);

    // redirect to home
    window.location = CONFIG.CLIENT_URL;
  }
  return (
    <nav className="bg-white border-b border-gray-200 fixed w-full z-50 -mt-16">
      <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div className="">
          <div className="flex items-center justify-between h-16 px-4 sm:px-0">
            <div className="flex items-center">
              <NavLink
                to="/"
                className="flex-shrink-0 grid grid-cols-6 relative"
              >
                <img
                  className="h-8 w-8 hidden md:inline col-span-1"
                  src={MedisparkLogo}
                  alt="Telehealth logo"
                />
                <img
                  className="h-8 col-span-5 ml-1"
                  src={TextOnlyMedisparkLogo}
                  alt="Telehealth logo"
                />
                <span className="absolute top-0 right-0 block px-2 inline-flex text-xs leading-5 font-bold rounded-lg bg-yellow-100 text-gray-900">
                  Beta
                </span>
              </NavLink>
            </div>

            {/* DESKTOP VIEW:: show profile and logout options if patient is logged in */}
            {user && user.id ? (
              <div className="hidden md:block">
                <div className="ml-4 flex items-center md:ml-6">
                  {/* <button
                    className="p-1 border-2 border-transparent text-gray-400 rounded-full hover:text-white focus:outline-none focus:text-white focus:bg-gray-700"
                    aria-label="Notifications"
                  >
                    <svg
                      className="h-6 w-6"
                      stroke="currentColor"
                      fill="none"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="1"
                      viewBox="0 0 24 24"
                    >
                      <path d="M21 3l-6 6m0 0V4m0 5h5M5 3a2 2 0 00-2 2v1c0 8.284 6.716 15 15 15h1a2 2 0 002-2v-3.28a1 1 0 00-.684-.948l-4.493-1.498a1 1 0 00-1.21.502l-1.13 2.257a11.042 11.042 0 01-5.516-5.517l2.257-1.128a1 1 0 00.502-1.21L9.228 3.683A1 1 0 008.279 3H5z"></path>
                    </svg>
                  </button> */}
                  <div className="ml-3">
                    <Tippy
                      content={
                        <div className="origin-top-right mt-2 w-48 rounded-md shadow-lg">
                          <div className="py-1 rounded-md bg-white shadow-xs">
                            <NavLink
                              to={`/settings/user`}
                              className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                            >
                              Your Profile
                            </NavLink>
                            <span
                              onClick={handleLogout}
                              className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 cursor-pointer"
                            >
                              Sign out
                            </span>
                          </div>
                        </div>
                      }
                      allowHTML={true}
                      interactive={true}
                      animation="perspective"
                      duration="[200, 500]"
                      hideOnClick={false}
                      interactiveDebounce={100}
                    >
                      <button
                        className="max-w-xs flex items-center text-sm rounded-full text-white focus:outline-none focus:shadow-solid"
                        id="user-menu"
                        aria-label="User menu"
                        aria-haspopup="true"
                      >
                        {user.pictures && user.pictures.length > 0 ? (
                          Object.keys(user.pictures).map((each, key) => {
                            let image = user.pictures[each];

                            if (image.type === "avatar") {
                              return (
                                <img
                                  key={image.id}
                                  className="h-10 w-10 rounded-full"
                                  src={image.url}
                                  alt="Medispark Telehealth"
                                />
                              );
                            }
                          })
                        ) : (
                          <svg
                            className="h-10 w-10 rounded-full text-gray-300"
                            fill="currentColor"
                            viewBox="0 0 24 24"
                          >
                            <path d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z" />
                          </svg>
                        )}
                      </button>
                    </Tippy>
                  </div>
                </div>
              </div>
            ) : (
              <Tippy
                content={
                  <div className="origin-top-right mt-2 w-48 rounded-md shadow-lg">
                    <div className="py-1 rounded-md bg-white shadow-xs">
                      {NAVBAR.options.map((option, i) => {
                        return (
                          <NavLink
                            to={option.pathName}
                            className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                          >
                            {option.name}
                          </NavLink>
                        );
                      })}
                    </div>
                  </div>
                }
                allowHTML={true}
                interactive={true}
                animation="perspective"
                duration="[200, 500]"
                hideOnClick={false}
                interactiveDebounce={100}
              >
                <div className="hidden md:block rounded-md shadow-sm m-2 ml-12">
                  <span className=" items-center px-4 py-2 border border-transparent text-sm leading-6 font-medium rounded-md text-white bg-teal-900 hover:bg-teal-500 focus:outline-none focus:border-teal-700 focus:shadow-outline-teal active:bg-teal-700 transition ease-in-out duration-150">
                    Sign In
                  </span>
                </div>
              </Tippy>
            )}

            {/* Mobile Menu Trigger Button */}
            <Tippy
              content={
                <div className="border-b border-gray-700 bg-teal-800 rounded pl-3 pr-6 py-3">
                  {/* display login options only if user isn't authenticated */}
                  {user && user.id
                    ? ""
                    : NAVBAR.options.map((option, i) => {
                        return (
                          <div className="px-2 py-3 sm:px-3">
                            <NavLink
                              key={i}
                              to={option.pathName}
                              className="mt-1 group flex items-center px-2 py-2 text-lg leading-5 font-medium text-gray-300 rounded-md hover:text-white hover:bg-teal-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150"
                            >
                              {option.name}
                            </NavLink>
                          </div>
                        );
                      })}

                  {/* MOBILE VIEW:: show profile and logout options if patient is logged in */}
                  {user && user.id ? (
                    <Fragment>
                      <div className="px-2 py-3 sm:px-3">
                        <div className="flex items-center flex-shrink-0 mb-4 pb-4 border-b border-teal-900">
                          {user.hospital &&
                          user.hospital.pictures &&
                          user.hospital.pictures.length > 0 ? (
                            Object.keys(user.hospital.pictures).map(
                              (each, key) => {
                                let image = user.hospital.pictures[each];

                                if (image.type === "logo") {
                                  return (
                                    <img
                                      key={image.id}
                                      className="h-8 w-auto"
                                      src={image.url}
                                      alt="Medispark Telehealth"
                                    />
                                  );
                                }
                              }
                            )
                          ) : (
                            <svg
                              className="h-8 w-auto text-gray-300"
                              fill="none"
                              viewBox="0 0 24 24"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="2"
                              stroke="currentColor"
                            >
                              <path d="M19 21V5a2 2 0 00-2-2H7a2 2 0 00-2 2v16m14 0h2m-2 0h-5m-9 0H3m2 0h5M9 7h1m-1 4h1m4-4h1m-1 4h1m-5 10v-5a1 1 0 011-1h2a1 1 0 011 1v5m-4 0h4"></path>
                            </svg>
                          )}

                          <Tippy
                            content={
                              <span className="inline-flex items-center px-2 py-0.5 rounded text-xs font-medium leading-4 bg-gray-100 text-indigo-800">
                                {user.hospital && user.hospital.name}
                              </span>
                            }
                          >
                            <span className="font-medium text-white text-sm pl-2">
                              {user.hospital
                                ? user.hospital.name.length > 21
                                  ? user.hospital.name.substring(0, 20) + "..."
                                  : user.hospital.name
                                : ""}
                            </span>
                          </Tippy>
                        </div>
                        {/* display menu options for doctor */}
                        {user.doctor_id && (
                          <NavLink
                            to="/doctor/select-patients"
                            className="mt-1 group flex items-center px-2 py-2 text-lg leading-5 font-medium text-gray-300 rounded-md hover:text-white hover:bg-teal-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150"
                            activeClassName="bg-gray-900"
                          >
                            <svg
                              className="mr-3 h-6 w-6 text-gray-400 group-hover:text-gray-300 group-focus:text-gray-300 transition ease-in-out duration-150"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 24 24"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="2"
                            >
                              <path d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z"></path>
                              <path d="M15 13a3 3 0 11-6 0 3 3 0 016 0z"></path>
                            </svg>
                            Consulting Room{" "}
                            {user.myWaitingVideoNotifications &&
                            user.myWaitingVideoNotifications.length > 0 ? (
                              <span className="inline-flex items-center ml-2 px-2.5 py-0.5 rounded-full text-xs font-bold leading-4 bg-red-700 text-white">
                                <div className="-ml-0.5 mr-1.5 text-white">
                                  {user.myWaitingVideoNotifications.length}
                                </div>
                                {user.myWaitingVideoNotifications.length ===
                                1 ? (
                                  <span>Call</span>
                                ) : (
                                  <span>Calls</span>
                                )}
                              </span>
                            ) : (
                              ""
                            )}
                          </NavLink>
                        )}

                        {/* display menu options for patient */}
                        {user.patient_id && (
                          <NavLink
                            to="/patient/select-doctors"
                            className="mt-1 group flex items-center px-2 py-2 text-lg leading-5 font-medium text-gray-300 rounded-md hover:text-white hover:bg-teal-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150"
                            activeClassName="bg-gray-900"
                          >
                            <svg
                              className="mr-3 h-6 w-6 text-gray-400 group-hover:text-gray-300 group-focus:text-gray-300 transition ease-in-out duration-150"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 24 24"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="2"
                            >
                              <path d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z"></path>
                              <path d="M15 13a3 3 0 11-6 0 3 3 0 016 0z"></path>
                            </svg>
                            Talk To Doctor
                          </NavLink>
                        )}

                        {/* display menu options based on permissions */}
                        {user.permissions &&
                        user.permissions.includes("see hospital earnings") ? (
                          <NavLink
                            to="/hospital/earnings"
                            className="mt-1 group flex items-center px-2 py-2 text-lg leading-5 font-medium text-gray-300 rounded-md hover:text-white hover:bg-teal-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150"
                          >
                            <svg
                              className="mr-3 h-6 w-6 text-gray-400 group-hover:text-gray-300 group-focus:text-gray-300 transition ease-in-out duration-150"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 24 24"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z"
                              />
                            </svg>
                            Earnings
                          </NavLink>
                        ) : (
                          ""
                        )}

                        {/* settings menu option */}
                        <NavLink
                          to="/settings"
                          className="mt-1 group flex items-center px-2 py-2 text-lg leading-5 font-medium text-gray-300 rounded-md hover:text-white hover:bg-teal-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150"
                          activeClassName="bg-gray-900"
                        >
                          <svg
                            className="mr-3 h-6 w-6 text-gray-400 group-hover:text-gray-300 group-focus:text-gray-300 transition ease-in-out duration-150"
                            stroke="currentColor"
                            fill="none"
                            viewBox="0 0 24 24"
                          >
                            <path
                              d="M11.49 3.17c-.38-1.56-2.6-1.56-2.98 0a1.532 1.532 0 01-2.286.948c-1.372-.836-2.942.734-2.106 2.106.54.886.061 2.042-.947 2.287-1.561.379-1.561 2.6 0 2.978a1.532 1.532 0 01.947 2.287c-.836 1.372.734 2.942 2.106 2.106a1.532 1.532 0 012.287.947c.379 1.561 2.6 1.561 2.978 0a1.533 1.533 0 012.287-.947c1.372.836 2.942-.734 2.106-2.106a1.533 1.533 0 01.947-2.287c1.561-.379 1.561-2.6 0-2.978a1.532 1.532 0 01-.947-2.287c.836-1.372-.734-2.942-2.106-2.106a1.532 1.532 0 01-2.287-.947zM10 13a3 3 0 100-6 3 3 0 000 6z"
                              strokeWidth="2"
                            ></path>
                          </svg>
                          Settings
                        </NavLink>
                      </div>
                      <div className="pt-4 pb-3 border-t border-gray-700">
                        <div className="flex items-center px-5">
                          <div className="flex-shrink-0">
                            {user.pictures && user.pictures.length > 0 ? (
                              Object.keys(user.pictures).map((each, key) => {
                                let image = user.pictures[each];

                                if (image.type === "avatar") {
                                  return (
                                    <img
                                      key={image.id}
                                      className="h-10 w-10 rounded-full"
                                      src={image.url}
                                      alt="Medispark Telehealth"
                                    />
                                  );
                                }
                              })
                            ) : (
                              <svg
                                className="h-6 w-6 rounded-full text-gray-300"
                                fill="currentColor"
                                viewBox="0 0 24 24"
                              >
                                <path d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z" />
                              </svg>
                            )}
                          </div>
                        </div>
                        <div
                          className="mt-3 px-2"
                          role="menu"
                          aria-orientation="vertical"
                          aria-labelledby="user-menu"
                        >
                          <NavLink
                            to={`/settings`}
                            className="block px-3 py-2 rounded-md text-base font-medium text-gray-200 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700"
                            role="menuitem"
                          >
                            Your Profile
                          </NavLink>
                          <span
                            onClick={handleLogout}
                            className="mt-1 block px-3 py-2 rounded-md text-base font-medium text-gray-200 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700"
                            role="menuitem"
                          >
                            Sign out
                          </span>
                        </div>
                      </div>
                    </Fragment>
                  ) : (
                    ""
                  )}
                </div>
              }
              allowHTML={true}
              interactive={true}
              animation="perspective"
              duration="[200, 500]"
              hideOnClick={false}
              interactiveDebounce={100}
            >
              <div className="-mr-2 flex md:hidden">
                <button className="inline-flex items-center justify-center p-2 rounded-md text-teal-900 hover:text-white hover:bg-gray-700 focus:outline-none focus:bg-gray-700 focus:text-white">
                  <svg
                    className="h-6 w-6"
                    stroke="currentColor"
                    fill="none"
                    viewBox="0 0 24 24"
                  >
                    <path
                      className="inline-flex"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M4 6h16M4 12h16M4 18h16"
                    />
                    <path
                      className="hidden"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M6 18L18 6M6 6l12 12"
                    />
                  </svg>
                </button>
              </div>
            </Tippy>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
