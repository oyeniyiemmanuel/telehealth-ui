import React, { Component, Fragment } from "react";
import axios from "axios";
import { Formik } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { authenticated } from "../../redux/actions";
import Error from "../widgets/error";
import { CONFIG } from "../../app.config";
import asset2_bg_img from "../../assets/img/asset2.jpg";
import { SmallFeedbackMessage } from "../widgets/Feedbacks";
import { MediumButton } from "../widgets/Buttons";
import Navbar from "../Navbar";
import { NavLink } from "react-router-dom";

const LoginSchema = Yup.object().shape({
  email: Yup.string().required("Please Enter Your Email"),
  password: Yup.string().required("Please Enter Your Password"),
});

class AwaitingEmailVerification extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showLoginAttemptError: false,
      showLoginServerError: false,
    };
  }

  submitForm() {}

  render() {
    return (
      <Fragment>
        {/* Navbar Component */}
        <Navbar />

        {/* other part of the page */}
        <div className="min-h-screen flex">
          <div className="flex-1 flex flex-col py-12 w-1/3 px-2 sm:px-3 lg:flex-none lg:px-10 xl:px-12">
            <h1>Please check your email, a verification link has been sent</h1>
          </div>
          <div className="hidden lg:block relative w-0 flex-1">
            <img
              className="absolute inset-0 h-full w-full object-cover"
              src={asset2_bg_img}
              alt=""
            />
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.userAuth.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    isLoggedIn: (user, token) => dispatch(authenticated(user, token)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AwaitingEmailVerification);
