import React, { Component, Fragment } from "react";
import axios from "axios";
import { Formik } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { authenticated } from "../../redux/actions";
import Error from "../widgets/error";
import { SmallFeedbackMessage } from "../widgets/Feedbacks";
import { CONFIG } from "../../app.config";
import asset2_bg_img from "../../assets/img/asset2.jpg";
import { MediumButton } from "../widgets/Buttons";
import Navbar from "../Navbar";

const LoginSchema = Yup.object().shape({
  email: Yup.string().required("Please Enter Your Email"),
  password: Yup.string().required("Please Enter Your Password"),
});

class DoctorLoginForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showLoginAttemptError: false,
      showLoginServerError: false,
    };
  }

  submitForm() {}

  render() {
    const { showLoginAttemptError, showLoginServerError } = this.state;
    return (
      <Fragment>
        {/* Navbar Component */}
        <Navbar />

        {/* other part of the page */}
        <div className="min-h-screen flex">
          <div className="flex-1 flex flex-col py-12 w-1/3 px-2 sm:px-3 lg:flex-none lg:px-10 xl:px-12">
            <Formik
              initialValues={{
                email: "",
                password: "",
              }}
              validationSchema={LoginSchema}
              onSubmit={(values, { setSubmitting }) => {
                setSubmitting(true);

                // set boolean not to show error messages
                this.setState({
                  showLoginAttemptError: false,
                  showLoginServerError: false,
                });

                // make a login post request
                axios
                  .post(CONFIG.SERVER_API + "login", {
                    email: values.email,
                    password: values.password,
                  })
                  .then(
                    (res) => {
                      // if api returns status 200 ok but the user login details isn't correct
                      if (res.data.message === "Invalid Credentials") {
                        this.setState({ showLoginAttemptError: true });
                        setSubmitting(false);
                      } else {
                        // dispatch action to save user and token in redux store
                        this.props.isLoggedIn(
                          res.data.user[0],
                          res.data.access_token
                        );
                        // save token in localstorage
                        localStorage.setItem("token", res.data.access_token);

                        // redirect user to create new hospital if there's no hospital data on the user, else redirect to dashboard
                        if (res.data.user.doctor_id !== null) {
                          this.props.history.push("/doctor/select-patients");
                        } else {
                          this.props.history.push("/settings");
                        }
                      }
                    },
                    (err) => {
                      console.log(err);
                      this.setState({ showLoginServerError: true });
                      setSubmitting(false);
                    }
                  );
              }}
            >
              {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
              }) => (
                <form onSubmit={handleSubmit}>
                  <div className="mx-auto w-full max-w-sm">
                    <div>
                      <h2 className="mt-6 text-center text-3xl leading-9 font-medium text-gray-900">
                        Doctor Sign In
                      </h2>
                      <p className="mt-2 text-center text-sm leading-5 text-gray-600">
                        Or &nbsp;
                        <SmallFeedbackMessage message="This is only for doctors who have gotten invitation link from their hospital and have verified their email address" />
                      </p>
                    </div>
                    <div className="mt-8">
                      <input type="hidden" name="remember" value="true" />
                      <div className="rounded-md shadow-sm">
                        <div>
                          <input
                            aria-label="Email address"
                            name="email"
                            type="email"
                            required
                            className={
                              touched.email && errors.email
                                ? "appearance-none rounded-none relative block w-full px-3 py-2 border border-red-300 placeholder-red-500 text-red-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                : "appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                            }
                            placeholder="Email address"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.name}
                          />
                        </div>
                        <div className="-mt-px">
                          <input
                            aria-label="Password"
                            name="password"
                            type="password"
                            required
                            className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                            placeholder="Password"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.password}
                          />
                        </div>
                      </div>

                      {showLoginAttemptError && (
                        <Error message="Invalid Username or Password" />
                      )}

                      {showLoginServerError && (
                        <Error message="Issues Connecting with the api server" />
                      )}

                      <div className="mt-6 flex items-center justify-between">
                        <div className="flex items-center">
                          <input
                            id="remember_me"
                            type="checkbox"
                            className="form-checkbox h-4 w-4 text-indigo-600 transition duration-150 ease-in-out"
                          />
                          <label
                            htmlFor="remember_me"
                            className="ml-2 block text-sm leading-5 text-gray-900"
                          >
                            Remember me
                          </label>
                        </div>

                        <div className="text-sm leading-5">
                          <a
                            href="/"
                            className="font-medium text-indigo-600 hover:text-indigo-500 focus:outline-none focus:underline transition ease-in-out duration-150"
                          >
                            Forgot your password?
                          </a>
                        </div>
                      </div>

                      <div className="mt-6">
                        <MediumButton
                          type="submit"
                          title={isSubmitting ? "Processing..." : "Sign In"}
                          extraClass={
                            isSubmitting ? "opacity-50 cursor-not-allowed" : ""
                          }
                        />
                      </div>
                    </div>
                  </div>
                </form>
              )}
            </Formik>
          </div>
          <div className="hidden lg:block relative w-0 flex-1">
            <img
              className="absolute inset-0 h-full w-full object-cover"
              src={asset2_bg_img}
              alt=""
            />
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.userAuth.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    isLoggedIn: (user, token) => dispatch(authenticated(user, token)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DoctorLoginForm);
