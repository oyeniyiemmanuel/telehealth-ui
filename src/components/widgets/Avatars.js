import React from "react";

const UnisexAvatar = (user) =>
  user.pictures && user.pictures.length > 0 ? (
    Object.keys(user.pictures).map((each, key) => {
      let image = user.pictures[each];

      if (image.type === "avatar") {
        return (
          <img
            key={user.id}
            className="h-full w-full rounded-full"
            src={image.url}
            alt="Medispark Telehealth"
          />
        );
      }
    })
  ) : (
    <svg
      className="h-full w-full rounded-full text-teal-300"
      fill="currentColor"
      viewBox="0 0 18 18"
    >
      <path
        d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
        clipRule="evenodd"
        fillRule="evenodd"
      ></path>
    </svg>
  );
const OnlineStatusAvatar = (user) =>
  user.pictures && user.pictures.length > 0 ? (
    Object.keys(user.pictures).map((each, key) => {
      let image = user.pictures[each];

      if (image.type === "avatar") {
        return (
          <span className="inline-block relative">
            <img
              key={user.id}
              className="h-full w-full rounded-full"
              src={image.url}
              alt="Medispark Telehealth"
            />
            {user.isOnline ? (
              <span
                key={user.id}
                className="absolute top-0 right-0 block h-2 w-2 rounded-full text-white shadow-solid bg-green-400"
              ></span>
            ) : (
              <span
                key={user.id}
                className="absolute top-0 right-0 block h-2 w-2 rounded-full text-white shadow-solid bg-red-400"
              ></span>
            )}
          </span>
        );
      }
    })
  ) : (
    <span className="inline-block relative">
      <svg
        className="h-full w-full rounded-full text-teal-300"
        fill="currentColor"
        viewBox="0 0 18 18"
      >
        <path
          d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
          clipRule="evenodd"
          fillRule="evenodd"
        ></path>
      </svg>

      {user.isOnline ? (
        <span
          key={user.id}
          className="absolute top-0 right-0 block h-2 w-2 rounded-full text-white shadow-solid bg-green-400"
        ></span>
      ) : (
        <span
          key={user.id}
          className="absolute top-0 right-0 block h-2 w-2 rounded-full text-white shadow-solid bg-red-400"
        ></span>
      )}
    </span>
  );

export { UnisexAvatar, OnlineStatusAvatar };
