import React, { Fragment } from "react";
import { NavLink } from "react-router-dom";
import { useAlert } from "react-alert";
import Tippy from "@tippyjs/react";

const SideNav = (props) => {
  const { user } = props;
  const alert = useAlert();

  // Notification of success
  const alertOptions = {
    type: "incoming-video-call",
    timeout: 15000,
  };

  if (
    props.myWaitingVideoNotifications &&
    props.myWaitingVideoNotifications.length > 0
  ) {
    if (props.myWaitingVideoNotifications.length === 1) {
      {
        alert.show("1 Incoming Call", alertOptions);
      }
    } else {
      alert.show(
        `${props.myWaitingVideoNotifications.length} Incoming Calls`,
        alertOptions
      );
    }
  }
  return (
    <Fragment>
      {/* //   <!-- Static sidebar for desktop --> */}
      <div className="hidden md:flex md:flex-shrink-0">
        <div className="flex flex-col w-64">
          <div className="flex items-center h-16 flex-shrink-0 px-4 bg-gray-800 border-b border-gray-900">
            {user &&
            user.hospital &&
            user.hospital.pictures &&
            user.hospital.pictures.length > 0 ? (
              Object.keys(user.hospital.pictures).map((each, key) => {
                let image = user.hospital.pictures[each];

                if (image.type === "logo") {
                  return (
                    <img
                      key={image.id}
                      className="h-8 w-auto"
                      src={image.url}
                      alt="Medispark Telehealth"
                    />
                  );
                }
              })
            ) : (
              <svg
                className="h-8 w-auto text-gray-300"
                fill="none"
                viewBox="0 0 24 24"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                stroke="currentColor"
              >
                <path d="M19 21V5a2 2 0 00-2-2H7a2 2 0 00-2 2v16m14 0h2m-2 0h-5m-9 0H3m2 0h5M9 7h1m-1 4h1m4-4h1m-1 4h1m-5 10v-5a1 1 0 011-1h2a1 1 0 011 1v5m-4 0h4"></path>
              </svg>
            )}

            <Tippy
              content={
                <span className="inline-flex items-center px-2 py-0.5 rounded text-xs font-medium leading-4 bg-gray-100 text-indigo-800">
                  {user && user.hospital ? user.hospital.name : ""}
                </span>
              }
            >
              <span className="font-medium text-white text-sm pl-2">
                {user && user.hospital
                  ? user.hospital.name.length > 21
                    ? user.hospital.name.substring(0, 20) + "..."
                    : user.hospital.name
                  : ""}
              </span>
            </Tippy>
          </div>
          <div className="h-0 flex-1 flex flex-col overflow-y-auto">
            {/* <!-- Sidebar component, swap this element with another sidebar if you like --> */}
            <nav className="flex-1 px-2 py-4 bg-teal-900">
              {/* <NavLink
                to="/hospital"
                className="mt-1 group flex items-center px-2 py-2 text-xs leading-5 font-medium text-gray-300 rounded-md hover:text-white hover:bg-teal-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150"
                activeClassName="bg-gray-900"
              >
                <svg
                  className="mr-3 h-6 w-6 text-gray-400 group-hover:text-gray-300 group-focus:text-gray-300 transition ease-in-out duration-150"
                  stroke="currentColor"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                >
                  <path d="M4 6a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2H6a2 2 0 01-2-2V6zM14 6a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V6zM4 16a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2H6a2 2 0 01-2-2v-2zM14 16a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z"></path>
                </svg>
                Dashboard
              </NavLink> */}
              {user && user.doctor_id && (
                <NavLink
                  to="/doctor/select-patients"
                  className="mt-1 group flex items-center px-2 py-2 text-xs leading-5 font-medium text-gray-300 rounded-md hover:text-white hover:bg-teal-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150"
                  activeClassName="bg-gray-900"
                >
                  <svg
                    className="mr-3 h-6 w-6 text-gray-400 group-hover:text-gray-300 group-focus:text-gray-300 transition ease-in-out duration-150"
                    stroke="currentColor"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                  >
                    <path d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z"></path>
                    <path d="M15 13a3 3 0 11-6 0 3 3 0 016 0z"></path>
                  </svg>
                  Consulting Room{" "}
                  {props.myWaitingVideoNotifications &&
                  props.myWaitingVideoNotifications.length > 0 ? (
                    <span className="inline-flex items-center ml-2 px-2.5 py-0.5 rounded-full text-xs font-bold leading-4 bg-red-700 text-white">
                      <div className="-ml-0.5 mr-1.5 text-white">
                        {props.myWaitingVideoNotifications.length}
                      </div>
                      {props.myWaitingVideoNotifications.length === 1 ? (
                        <span>Call</span>
                      ) : (
                        <span>Calls</span>
                      )}
                    </span>
                  ) : (
                    ""
                  )}
                </NavLink>
              )}

              {user && user.patient_id ? (
                <NavLink
                  to="/patient/select-doctors"
                  className="mt-1 group flex items-center px-2 py-2 text-xs leading-5 font-medium text-gray-300 rounded-md hover:text-white hover:bg-teal-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150"
                  activeClassName="bg-gray-900"
                >
                  <svg
                    className="mr-3 h-6 w-6 text-gray-400 group-hover:text-gray-300 group-focus:text-gray-300 transition ease-in-out duration-150"
                    stroke="currentColor"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                  >
                    <path d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z"></path>
                    <path d="M15 13a3 3 0 11-6 0 3 3 0 016 0z"></path>
                  </svg>
                  Talk To Doctor
                </NavLink>
              ) : (
                ""
              )}

              <NavLink
                to="/almost-ready"
                className="mt-1 group flex items-center px-2 py-2 text-xs leading-5 font-medium text-gray-300 rounded-md hover:text-white hover:bg-teal-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150"
              >
                <svg
                  className="mr-3 h-6 w-6 text-gray-400 group-hover:text-gray-300 group-focus:text-gray-300 transition ease-in-out duration-150"
                  stroke="currentColor"
                  fill="none"
                  viewBox="0 0 24 24"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                  />
                </svg>
                Appointments
              </NavLink>
              {user &&
              user.permissions &&
              user.permissions.includes("see hospital earnings") ? (
                <NavLink
                  to="/hospital/earnings"
                  className="mt-1 group flex items-center px-2 py-2 text-xs leading-5 font-medium text-gray-300 rounded-md hover:text-white hover:bg-teal-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150"
                  activeClassName="bg-gray-900"
                >
                  <svg
                    className="mr-3 h-6 w-6 text-gray-400 group-hover:text-gray-300 group-focus:text-gray-300 transition ease-in-out duration-150"
                    stroke="currentColor"
                    fill="none"
                    strokeWidth="2"
                    viewBox="0 0 24 24"
                  >
                    <path d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z"></path>
                  </svg>
                  Earnings
                </NavLink>
              ) : (
                ""
              )}
              <NavLink
                to="/settings"
                className="mt-1 group flex items-center px-2 py-2 text-xs leading-5 font-medium text-gray-300 rounded-md hover:text-white hover:bg-teal-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150"
                activeClassName="bg-gray-900"
              >
                <svg
                  className="mr-3 h-6 w-6 text-gray-400 group-hover:text-gray-300 group-focus:text-gray-300 transition ease-in-out duration-150"
                  stroke="currentColor"
                  fill="none"
                  viewBox="0 0 24 24"
                >
                  <path
                    d="M11.49 3.17c-.38-1.56-2.6-1.56-2.98 0a1.532 1.532 0 01-2.286.948c-1.372-.836-2.942.734-2.106 2.106.54.886.061 2.042-.947 2.287-1.561.379-1.561 2.6 0 2.978a1.532 1.532 0 01.947 2.287c-.836 1.372.734 2.942 2.106 2.106a1.532 1.532 0 012.287.947c.379 1.561 2.6 1.561 2.978 0a1.533 1.533 0 012.287-.947c1.372.836 2.942-.734 2.106-2.106a1.533 1.533 0 01.947-2.287c1.561-.379 1.561-2.6 0-2.978a1.532 1.532 0 01-.947-2.287c.836-1.372-.734-2.942-2.106-2.106a1.532 1.532 0 01-2.287-.947zM10 13a3 3 0 100-6 3 3 0 000 6z"
                    strokeWidth="2"
                  ></path>
                </svg>
                Settings
              </NavLink>
            </nav>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default SideNav;
