import React from "react";
import notFoundImage from "../../assets/img/404.png";

const NotFound = () => (
  <div>
    <div className="bg-gray-100 overflow-hidden shadow rounded-lg">
      <img
        className="w-2/3 shadow-lg m-auto"
        alt="Page Not Found"
        src={notFoundImage}
      />
    </div>
  </div>
);

export default NotFound;
