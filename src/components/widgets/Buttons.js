import React from "react";

const MediumButton = (props) => (
  <button
    type={props.type}
    className={`inline-flex ${props.extraClass} w-full justify-center items-center px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-teal-600 hover:bg-teal-500 focus:outline-none focus:border-teal-700 focus:shadow-outline-teal active:bg-teal-700 transition ease-in-out duration-150`}
  >
    {props.title}
  </button>
);

const LargeButton = (props) => (
  <button
    type={props.type}
    className={`inline-flex ${props.extraClass} w-full justify-center items-center px-6 py-3 border border-transparent text-lg leading-5 font-bold rounded-lg text-white bg-teal-600 hover:bg-teal-500 focus:outline-none focus:border-teal-700 focus:shadow-outline-teal active:bg-teal-700 transition ease-in-out duration-150`}
  >
    {props.title}
  </button>
);

export { MediumButton, LargeButton };
