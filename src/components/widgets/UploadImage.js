import React from "react";
import ImageUploader from "react-images-upload";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import axios from "axios";
import { CONFIG } from "../../app.config";
import { useAlert } from "react-alert";

const UploadImage = (props) => {
  const { user } = props;
  const alert = useAlert();

  function onDrop(picture) {
    let bodyFormData = new FormData();
    bodyFormData.set("user_id", user.id);
    bodyFormData.append(props.pictureType, picture[0]);

    axios({
      method: "post",
      url: CONFIG.SERVER_API + props.submitUrl,
      data: bodyFormData,
      headers: { "Content-Type": "multipart/form-data" },
    })
      .then((res) => {
        //handle success
        console.log(res);
        if (res.data.message == "uploaded") {
          // Notification of success
          let alertOptions = {
            type: "success",
          };
          alert.show("Hospital Logo Updated", alertOptions);

          // Redirect
          window.location = CONFIG.CLIENT_URL + props.callbackUrl;
        }
      })
      .catch((res) => {
        //handle error
        console.log(res);
      });
  }

  return (
    <ImageUploader
      {...props}
      withLabel={false}
      withIcon={false}
      onChange={onDrop}
      buttonText="Change"
      singleImage={true}
      imgExtension={[".jpg", ".gif", ".png", ".gif"]}
      maxFileSize={5242880}
    />
  );
};

export default compose(withRouter)(UploadImage);
