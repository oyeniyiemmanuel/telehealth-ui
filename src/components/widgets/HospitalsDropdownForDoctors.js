import React, { Component, Fragment } from "react";
import axios from "axios";
import { CONFIG } from "../../app.config";
import Select from "react-select";
import { getAllHospitals } from "../../accessors/hospital";
import { getLoggedInUser } from "../../accessors/user";
import { MediumButton } from "./Buttons";
import { withRouter } from "react-router-dom";
import { compose } from "redux";

class HospitalsDropdown extends Component {
  constructor(props) {
    super(props);

    console.log(props.user);
    this.state = {
      isSubmitting: false,
      hospitalOptions: [],
      selectedOption: null,
      user: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);

    // get the current user details
    let token = localStorage.getItem("token");
    getLoggedInUser(token).then((res) => {
      this.setState({ user: res });
    });

    // get hospitals
    getAllHospitals().then((res) => {
      console.log(res);
      const result = res.map((hospital) => {
        hospital.value = hospital.id;
        hospital.label = hospital.name;
        return hospital;
      });
      this.setState({ hospitalOptions: result });
    });
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    console.log(`Option selected:`, selectedOption);
  };

  handleSubmit(e) {
    e.preventDefault();
    this.setState({ isSubmitting: true });

    // send request to the server
    axios
      .post(CONFIG.SERVER_API + "doctor/set-hospital", {
        hospital_id: this.state.selectedOption.id,
        user_id: this.state.user.id,
      })
      .then(
        (res) => {
          console.log(res);
          //   // Notification of success
          //   let alertOptions = {
          //     type: "success",
          //   };
          //   useAlert().show("Your Hospital has Been Selected", alertOptions);
          this.setState({ isSubmitting: false });

          // Redirect
          this.props.history.push("/doctor/select-patients");
          //   reset form
        },
        (err) => {
          console.log(err);
        }
      );
  }

  render() {
    const { hospitalOptions, selectedOption } = this.state;

    return (
      <Fragment>
        <form onSubmit={this.handleSubmit}>
          <div className="">
            <div className=" sm:p-6">
              <div className="mt-6">
                <Select
                  required
                  menuPortalTarget={document.querySelector("body")}
                  placeholder="Select Hospital"
                  value={selectedOption}
                  onChange={this.handleChange}
                  options={hospitalOptions}
                />
              </div>
              <div className="mt-6">
                <MediumButton
                  type="submit"
                  title={this.state.isSubmitting ? "Processing..." : "Submit"}
                  extraClass={
                    this.state.isSubmitting
                      ? "opacity-50 cursor-not-allowed"
                      : ""
                  }
                />
              </div>
            </div>
          </div>
        </form>
      </Fragment>
    );
  }
}

export default compose(withRouter)(HospitalsDropdown);
