import React from "react";
import { Preloader, ThreeDots } from "react-preloader-icon";

const PreloaderLines = (props) => (
  <div className="h-screen -mt-20 pt-60 bg-gray-200">
    <div className="w-1/4 md:w-5 m-auto">
      <Preloader
        use={ThreeDots}
        size={100}
        strokeWidth={8}
        strokeColor="#1c64f2"
        duration={800}
      />
    </div>
  </div>
);

const PreloaderDots = (props) => (
  <div className="p-4 bg-gray-200 w-full text-center">{props.message}...</div>
);

export { PreloaderLines, PreloaderDots };
