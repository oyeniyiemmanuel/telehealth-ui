import React from "react";
import { Link } from "react-router-dom";

const Back = (props) => (
  <nav className="hidden sm:flex items-center text-sm leading-5 font-medium mb-6 underline">
    <svg
      className="flex-shrink-0 mx-2 h-5 w-5 text-gray-400"
      viewBox="0 0 20 20"
      fill="none"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
      viewBox="0 0 24 24"
      stroke="currentColor"
    >
      <path d="M15 19l-7-7 7-7"></path>
    </svg>
    <Link
      to={props.link}
      className="text-gray-500 hover:text-gray-700 transition duration-150 ease-in-out"
    >
      Back
    </Link>
  </nav>
);

export { Back };
