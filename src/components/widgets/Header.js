import React from "react";

const Header = (props) => (
  <div className="p-4 bg-gray-700">
    <h3 className="text-lg leading-6 font-medium text-white text-center">
      {props.title}
    </h3>
    <div className="mt-1 text-sm leading-5 text-white text-center">
      {props.subTitle}
    </div>
  </div>
);

export default Header;
