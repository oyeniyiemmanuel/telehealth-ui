import React, { Component, Fragment } from "react";
import axios from "axios";
import { CONFIG } from "../../app.config";
import Select from "react-select";
import { getLoggedInUser } from "../../accessors/user";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { getAllSpecialties } from "../../accessors/specialty";

class SelectDoctorSpecialty extends Component {
  constructor(props) {
    super(props);

    console.log(props);

    this.state = {
      isSubmitting: false,
      specialtyOptions: [],
      selectedOption: null,
      user: [],
    };

    // get the current user details
    let token = localStorage.getItem("token");
    getLoggedInUser(token).then((res) => {
      this.setState({ user: res });
    });

    // get specialties
    getAllSpecialties().then((res) => {
      const result = res.map((specialty) => {
        specialty.value = specialty.id;
        specialty.label = specialty.name;
        return specialty;
      });
      this.setState({ specialtyOptions: result });
    });
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    this.props.onSelectDoctorSpecialty(selectedOption);
  };

  handleSubmit(e) {
    e.preventDefault();
    this.setState({ isSubmitting: true });

    // send request to the server
    axios
      .post(CONFIG.SERVER_API + "patient/set-hospital", {
        hospital_id: this.state.selectedOption.id,
        user_id: this.state.user.id,
      })
      .then(
        (res) => {
          //   // Notification of success
          //   let alertOptions = {
          //     type: "success",
          //   };
          //   useAlert().show("Your Hospital has Been Selected", alertOptions);
          this.setState({ isSubmitting: false });

          // Redirect
          this.props.history.push("/patient/select-doctors");
          //   reset form
        },
        (err) => {
          console.log(err);
        }
      );
  }

  render() {
    const { specialtyOptions, selectedOption } = this.state;

    return (
      <Fragment>
        <div className="">
          <div className="mt-2">
            <Select
              required
              menuPortalTarget={document.querySelector("body")}
              placeholder="Select"
              value={selectedOption}
              onChange={this.handleChange}
              options={specialtyOptions}
            />
          </div>
        </div>
      </Fragment>
    );
  }
}

export default compose(withRouter)(SelectDoctorSpecialty);
