const NAVBAR = {
  options: [
    {
      name: "Patients",
      pathName: "/patient/login",
    },
    {
      name: "Hospitals & Doctors",
      pathName: "/login",
    },
  ],
};

const ROLES = {
  options: [
    {
      value: "doctor",
      label: "Doctor",
    },
    {
      value: "admin",
      label: "Admin",
    },
  ],
};

export { NAVBAR, ROLES };
