const CONFIG = {
  // general
  APP_NAME: "Telehealth",
  SERVER_API: "https://swiftmedispark.com/telehealth-api/public/api/",
  CLIENT_URL: "https://medispark-telehealth.netlify.app/",
  // SERVER_API: "http://localhost:8000/api/",
  // CLIENT_URL: "http://localhost:3000/",

  // vidyo
  VIDYO_DEV_KEY: "215e23d9d53c447faa9e15d8951254d4",
  VIDYO_APP_ID: "e549b6.vidyo.io",
  VIDYO_DEFAULT_EXPIRY_TIME: 3600,

  // sendbird
  SENDBIRD_APP_ID: "F6925D77-692E-4626-B61E-C3E24DC95B21",

  // paystack
  PAYSTACK_KEY: "pk_live_5857ce5046d272944d95838f2a35e622e52098f3",

  //Twilio
  TWILIO_ACCOUNT_SID: "ACd235f547f49a258425c797610d96e281",
  TWILIO_API_KEY_SID: "SK0a6515ef6ed5dca07879b8a8bdb95456",
  TWILIO_API_KEY_SECRET: "5q1gMsyH07J4Gd4Sqr9CMBHBdbg68IkX",

  //Pusher
  PUSHER_APP_ID: "1024220",
  PUSHER_APP_KEY: "0420b808eabf3a620c5a",
  PUSHER_APP_SECRET: "35a77099c3763a6d9fb1",
  PUSHER_APP_CLUSTER: "eu",
  PUSHER_APP_ENDPOINT: "http://localhost:8000/broadcasting/auth",
  PUSHER_APP_TLS: false,

  // Un-comment the following line to enable Google authentication with Firebase.
  // REACT_APP_SET_AUTH: "firebase",

  // Un-comment the following line to enable passcode authentication for use with the Twilio CLI rtc-plugin.
  // See: https://github.com/twilio-labs/plugin-rtc
  // REACT_APP_SET_AUTH: "passcode"

  REACT_APP_SET_AUTH: "",

  // The following values are used to configure the Firebase library.
  // See https://firebase.google.com/docs/web/setup#config-object
  // These variables must be set if FIREBASE_AUTH is enabled
  // REACT_APP_FIREBASE_API_KEY:
  // REACT_APP_FIREBASE_AUTH_DOMAIN:
  // REACT_APP_FIREBASE_DATABASE_URL:
  // REACT_APP_FIREBASE_STORAGE_BUCKET:
  // REACT_APP_FIREBASE_MESSAGING_SENDER_ID:
};

const ACTIONS = {
  // if there is a logged in user
  IS_LOGGED_IN: "IS_LOGGED_IN",
  // if new hospital has been created
  HOSPITAL_CREATED: "HOSPITAL_CREATED",
  // if currently logged in user details has been fetched from the api
  USER_IS_FETCHED: "USER_IS_FETCHED",
  // if video streams details have been fetched from the api
  STREAMS_ARE_FETCHED: "STREAMS_ARE_FETCHED",
  // if specialties have been fetched from the api
  SPECIALTIES_ARE_FETCHED: "SPECIALTIES_ARE_FETCHED",
  // if video stream details has been fetched from the api
  STREAM_IS_FETCHED: "STREAM_IS_FETCHED",
  // fetch doctors that belong to a hospital
  FETCH_HOSPITAL_DOCTORS: "FETCH_HOSPITAL_DOCTORS",
  // fetch patients that belong to a hospital
  FETCH_HOSPITAL_PATIENTS: "FETCH_HOSPITAL_PATIENTS",
  // fetch team members that belong to a hospital
  FETCH_HOSPITAL_TEAM_MEMBERS: "FETCH_HOSPITAL_TEAM_MEMBERS",
  // fetch earnings data of a hospital
  FETCH_HOSPITAL_EARNINGS_DATA: "FETCH_HOSPITAL_EARNINGS_DATA",
  // fetch user's waiting video notifications
  FETCH_MY_WAITING_VIDEO_NOTIFICATIONS: "FETCH_MY_WAITING_VIDEO_NOTIFICATIONS",
  // If the streams of patients waiting to see a particular doctor has been updated
  MY_PATIENTS_WAITING_STREAMS_ARE_FETCHED:
    "MY_PATIENTS_WAITING_STREAMS_ARE_FETCHED",
  // if all hospitals are fetched
  ALL_HOSPITALS_FETCHED: "ALL_HOSPITALS_FETCHED",
};

export { CONFIG, ACTIONS };
