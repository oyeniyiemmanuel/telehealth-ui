import React, { Component } from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
// import AppStateProvider from "./pages/callSession/twilio/state"
import LoginForm from "./components/auth/LoginForm";
import CallSession from "./pages/callSession";
import Home from "./pages/index";
import Hospital from "./pages/hospital";
import Settings from "./pages/settings";
import Doctors from "./pages/doctors";
import Patients from "./pages/patients";
import Register from "./pages/register";
import AlmostReady from "./pages/almost-ready";
import AwaitingEmailVerification from "./components/auth/AwaitingEmailVerification";
import VerifiedDoctor from "./pages/settings/VerifiedDoctor";
import OpenGeneralStream from "./pages/callSession/OpenGeneralStream";
import NotFound from "./components/widgets/NotFound";
import { AuthenticatedRoute, UnAuthenticatedRoute } from "./components/Routes";

class App extends Component {
  render() {
    return (
      <div className="">
        <Router basename="/">
          <div className="bg-gray-100 absolute w-full mt-16">
            <Switch>
              <AuthenticatedRoute path="/hospital" component={Hospital} />
              <AuthenticatedRoute
                exact
                path="/call-session"
                component={CallSession}
              />
              <AuthenticatedRoute
                path="/almost-ready"
                component={AlmostReady}
              />
              <AuthenticatedRoute
                path="/room/:resource_id"
                component={OpenGeneralStream}
              />
              <Route path="/settings" component={Settings} />
              <Route path="/doctor" component={Doctors} />
              <Route path="/patient" component={Patients} />
              <Route
                path="/awaiting-email-verification"
                component={AwaitingEmailVerification}
              />
              <Route
                path="/redirect-after-email-is-verified/:token"
                component={VerifiedDoctor}
              />
              <UnAuthenticatedRoute path="/login" component={LoginForm} />
              <UnAuthenticatedRoute path="/register" component={Register} />
              <UnAuthenticatedRoute exact path="/" component={Home} />
              <Route component={NotFound} />
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const userAuth = state.userAuth;
  return {
    user: userAuth.user,
    token: userAuth.token,
  };
};

export default connect(mapStateToProps, null)(App);
