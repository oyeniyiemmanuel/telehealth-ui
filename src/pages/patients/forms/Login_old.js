import React, { Component, Fragment } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import Header from "../../../components/widgets/Header";
import { authenticated, allHospitalsFetched } from "../../../redux/actions";
import axios from "axios";
import Error from "../../../components/widgets/error";
import { CONFIG } from "../../../app.config";
import { getAllHospitals } from "../../../accessors/hospital";
import { SmallFeedbackMessage } from "../../../components/widgets/Feedbacks";
import { MediumButton } from "../../../components/widgets/Buttons";
import { NavLink } from "react-router-dom";
import Select from "react-select";

const DoctorLoginSchema = Yup.object().shape({
  phone: Yup.string().required("Please Enter Your Phone Number"),
  last_name: Yup.string().required("Please Enter Your Surname"),
});

class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showSubmitError: false,
      showLoginForm: false,
      selectedHospitalOption: "",
    };

    // get all hospitals data
    getAllHospitals().then((res) => {
      const result = res.map((hospital) => {
        hospital.value = hospital.id;
        hospital.label = hospital.name;
        return hospital;
      });

      // trigger redux action to add all hospitals data to redux store
      this.props.allHospitalsFetched(result);
    });

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(selectedHospitalOption) {
    this.setState({
      selectedHospitalOption: selectedHospitalOption,
      showLoginForm: true,
    });
  }
  render() {
    const {
      showSubmitError,
      showLoginForm,
      selectedHospitalOption,
    } = this.state;
    const { hospitalsData } = this.props;
    return (
      <Fragment>
        <div>
          <Header
            title={
              showLoginForm
                ? `About to start consultation with a doctor at ${selectedHospitalOption.name}`
                : "Select Your Hospital"
            }
          />
          <div className="min-h-screen bg-gray-50 flex flex-col pt-6 sm:px-6 lg:px-8">
            <div className="mt-1 sm:mx-auto sm:w-full sm:max-w-md">
              <div className="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
                {showLoginForm ? (
                  <Fragment>
                    <div className="m-auto mb-2">
                      <SmallFeedbackMessage message="If you are a returning user, enter your exact previous details to be able to see past medical history" />
                    </div>
                    <Formik
                      initialValues={{ phone: "", last_name: "" }}
                      onSubmit={(
                        values,
                        { resetForm, initialValues, setSubmitting }
                      ) => {
                        setSubmitting(true);
                        this.setState({ showSubmitError: false });

                        axios
                          .post(CONFIG.SERVER_API + "patient/login", {
                            phone: values.phone,
                            last_name: values.last_name,
                            hospital_id: selectedHospitalOption.id,
                          })
                          .then(
                            (res) => {
                              // reset form values
                              // resetForm(initialValues);

                              if (res.data.message === "patient exists") {
                                this.setState({ showSubmitError: true });
                                setSubmitting(false);
                              } else {
                                // dispatch action to save user and token in redux store
                                this.props.isLoggedIn(
                                  res.data.user,
                                  res.data.access_token
                                );
                                // save token in localstorage
                                localStorage.setItem(
                                  "token",
                                  res.data.access_token
                                );
                                this.props.history.push("/patient/dashboard");
                              }
                            },
                            (err) => {
                              this.setState({ showSubmitError: true });
                              setSubmitting(false);
                              console.log(err);
                            }
                          );
                      }}
                      validationSchema={DoctorLoginSchema}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                      }) => (
                        <form onSubmit={handleSubmit}>
                          <div>
                            <label
                              htmlFor="phone"
                              className="block text-sm font-medium leading-5 text-gray-700"
                            >
                              Phone no.
                            </label>
                            <div className="mt-1 rounded-md shadow-sm">
                              <input
                                id="phone"
                                type="number"
                                required
                                className={
                                  touched.phone && errors.phone
                                    ? "appearance-none rounded-none relative block w-full px-3 py-2 border border-red-300 placeholder-red-500 text-red-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                    : "appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                }
                                placeholder="e.g. 08062680306"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.phone}
                              />
                            </div>
                          </div>

                          <div className="mt-6">
                            <label
                              htmlFor="last_name"
                              className="block text-sm font-medium leading-5 text-gray-700"
                            >
                              Surname
                            </label>
                            <div className="mt-1 rounded-md shadow-sm">
                              <input
                                id="last_name"
                                type="last_name"
                                required
                                className={
                                  touched.last_name && errors.last_name
                                    ? "appearance-none rounded-none relative block w-full px-3 py-2 border border-red-300 placeholder-red-500 text-red-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                    : "appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                }
                                placeholder="Enter surname"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.last_name}
                              />
                            </div>
                          </div>

                          {showSubmitError && (
                            <Error message="Invalid Credentials: Be Sure You have selected the correct Hospital. Also, the default password is in lower case" />
                          )}

                          <div className="mt-6">
                            <span className="block w-full rounded-md shadow-sm">
                              <MediumButton
                                type="submit"
                                title={
                                  isSubmitting
                                    ? "Finding Available Doctors..."
                                    : "Proceed"
                                }
                                extraClass={
                                  isSubmitting
                                    ? "opacity-50 cursor-not-allowed w-full flex"
                                    : "w-full flex"
                                }
                              />
                            </span>
                          </div>
                        </form>
                      )}
                    </Formik>
                  </Fragment>
                ) : (
                  <Select
                    value={selectedHospitalOption}
                    options={hospitalsData}
                    onChange={this.handleChange}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    hospitalsData: state.hospital.hospitals,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    isLoggedIn: (user, token) => dispatch(authenticated(user, token)),
    allHospitalsFetched: (hospitals) =>
      dispatch(allHospitalsFetched(hospitals)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
