import React, { Component, Fragment } from "react";
import axios from "axios";
import { Formik } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { authenticated } from "../../../redux/actions";
import Error from "../../../components/widgets/error";
import { CONFIG } from "../../../app.config";
import asset2_bg_img from "../../../assets/img/asset2.jpg";
import { MediumButton } from "../../../components/widgets/Buttons";
import Navbar from "../../../components/Navbar";
import { NavLink } from "react-router-dom";
import "react-step-progress-bar/styles.css";
import { ProgressBar, Step } from "react-step-progress-bar";

const RegisterSchema = Yup.object().shape({
  last_name: Yup.string().required("Please Enter Your Surname"),
  first_name: Yup.string().required("Please Enter Your First Name"),
  phone_no: Yup.string().required("Please Enter Your phone_no"),
});

class RegisterFormStep1 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showRegisterAttemptError: false,
      showRegisterServerError: false,
    };
  }

  submitForm() {}

  render() {
    const { showRegisterAttemptError, showRegisterServerError } = this.state;
    return (
      <Fragment>
        {/* Navbar Component */}
        <Navbar />

        {/* other part of the page */}
        <div className="min-h-screen flex">
          <div className="flex-1 flex flex-col py-12 w-1/3 px-2 sm:px-3 lg:flex-none lg:px-10 xl:px-12">
            <Formik
              initialValues={{
                first_name: "",
                last_name: "",
                phone_no: "",
              }}
              validationSchema={RegisterSchema}
              onSubmit={(values, { setSubmitting }) => {
                setSubmitting(true);

                // set boolean not to show error messages
                this.setState({
                  showRegisterAttemptError: false,
                  showRegisterServerError: false,
                });

                // make a Register post request
                axios
                  .post(CONFIG.SERVER_API + "patient/register/step-1", {
                    first_name: values.first_name,
                    last_name: values.last_name,
                    phone_no: values.phone_no,
                  })
                  .then(
                    (res) => {
                      if (res.data.message === "patient exists") {
                        this.setState({ showRegisterAttemptError: true });
                        setSubmitting(false);
                      } else {
                        // dispatch action to save user and token in redux store
                        this.props.isLoggedIn(
                          res.data.user,
                          res.data.access_token
                        );
                        // save token in localstorage
                        localStorage.setItem("token", res.data.access_token);
                        // redirect user to next step
                        this.props.history.push("/patient/register/step-2");
                      }
                    },
                    (err) => {
                      console.log(err);
                      this.setState({ showRegisterServerError: true });
                      setSubmitting(false);
                    }
                  );
              }}
            >
              {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
              }) => (
                <form onSubmit={handleSubmit}>
                  <div className="mx-auto w-full max-w-sm">
                    <ProgressBar
                      percent={25}
                      filledBackground="linear-gradient(to right, #f7fafc, #33476a)"
                    >
                      <Step>
                        {({ accomplished, index }) => (
                          <div
                            className={`indexedStep bg-gray-400 p-2 rounded-full text-white ${
                              accomplished ? "accomplished" : null
                            }`}
                          >
                            {index + 1}
                          </div>
                        )}
                      </Step>
                      <Step>
                        {({ accomplished, index }) => (
                          <div
                            className={`indexedStep bg-white shadow-md p-2 rounded-full text-gray-700 ${
                              accomplished ? "accomplished" : null
                            }`}
                          >
                            {index + 1}
                          </div>
                        )}
                      </Step>
                    </ProgressBar>
                    <div>
                      <h2 className="mt-6 text-center text-2xl leading-9 font-medium text-gray-900">
                        Register as a new Patient
                      </h2>
                      <p className="mt-2 text-center text-sm leading-5 text-gray-600">
                        Or &nbsp;
                        <NavLink
                          to="/patient/login"
                          className="font-medium text-indigo-600 hover:text-indigo-500 focus:outline-none focus:underline transition ease-in-out duration-150"
                        >
                          Sign In
                        </NavLink>
                      </p>
                    </div>
                    <div className="mt-8">
                      <div className="rounded-md shadow-sm">
                        <div>
                          <div className="flex justify-between">
                            <label
                              htmlFor="first_name"
                              className="block text-sm font-medium leading-5 text-gray-700"
                            >
                              First Name
                            </label>
                          </div>
                          <div className="mt-1 relative rounded-md shadow-sm">
                            <input
                              id="first_name"
                              name="first_name"
                              placeholder="e.g. Jane"
                              required
                              className={
                                touched.first_name && errors.first_name
                                  ? "form-input block w-full sm:text-sm sm:leading-5 relative block w-full px-3 py-2 border border-red-300 placeholder-red-500 text-red-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                  : "form-input block w-full sm:text-sm sm:leading-5 relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                              }
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.first_name}
                            />
                          </div>
                        </div>

                        <div className="mt-2">
                          <div className="flex justify-between">
                            <label
                              htmlFor="last_name"
                              className="block text-sm font-medium leading-5 text-gray-700"
                            >
                              Surname
                            </label>
                          </div>
                          <div className="mt-1 relative rounded-md shadow-sm">
                            <input
                              id="last_name"
                              name="last_name"
                              placeholder="e.g. Smith"
                              required
                              className={
                                touched.last_name && errors.last_name
                                  ? "form-input block w-full sm:text-sm sm:leading-5 relative block w-full px-3 py-2 border border-red-300 placeholder-red-500 text-red-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                  : "form-input block w-full sm:text-sm sm:leading-5 relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                              }
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.last_name}
                            />
                          </div>
                        </div>

                        <div className="mt-2">
                          <div className="flex justify-between">
                            <label
                              htmlFor="phone_no"
                              className="block text-sm font-medium leading-5 text-gray-700"
                            >
                              Phone Number
                            </label>
                          </div>
                          <div className="mt-1 relative rounded-md shadow-sm">
                            <input
                              id="phone_no"
                              name="phone_no"
                              placeholder="e.g. 08012345678"
                              required
                              className={
                                touched.phone_no && errors.phone_no
                                  ? "form-input block w-full sm:text-sm sm:leading-5 relative block w-full px-3 py-2 border border-red-300 placeholder-red-500 text-red-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                  : "form-input block w-full sm:text-sm sm:leading-5 relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                              }
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.phone_no}
                            />
                          </div>
                        </div>
                      </div>

                      {showRegisterAttemptError && (
                        <Error message="Sorry, Phone has already been used" />
                      )}

                      {showRegisterServerError && (
                        <Error message="Issues Connecting with the api server" />
                      )}

                      <div className="mt-6">
                        <MediumButton
                          type="submit"
                          title={isSubmitting ? "Processing..." : "Proceed"}
                          extraClass={
                            isSubmitting ? "opacity-50 cursor-not-allowed" : ""
                          }
                        />
                      </div>
                    </div>
                  </div>
                </form>
              )}
            </Formik>
          </div>
          <div className="hidden lg:block relative w-0 flex-1">
            <img
              className="absolute inset-0 h-full w-full object-cover"
              src={asset2_bg_img}
              alt=""
            />
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    isLoggedIn: (user, token) => dispatch(authenticated(user, token)),
  };
};

export default connect(null, mapDispatchToProps)(RegisterFormStep1);
