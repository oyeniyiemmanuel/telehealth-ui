import React, { Component, Fragment } from "react";
import axios from "axios";
import { Formik } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import Error from "../../../components/widgets/error";
import { CONFIG } from "../../../app.config";
import asset2_bg_img from "../../../assets/img/asset2.jpg";
import { getLoggedInUser } from "../../../accessors/user";
import { userFetched } from "../../../redux/actions";
import { MediumButton } from "../../../components/widgets/Buttons";
import Navbar from "../../../components/Navbar";
import "react-step-progress-bar/styles.css";
import { ProgressBar, Step } from "react-step-progress-bar";

const RegisterSchema = Yup.object().shape({
  password: Yup.string().required("Please Enter Your password"),
  password_confirmation: Yup.string().oneOf(
    [Yup.ref("password"), null],
    "Passwords must match"
  ),
});

class RegisterFormStep2 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showRegisterAttemptError: false,
      showRegisterServerError: false,
    };

    let token = localStorage.getItem("token");

    // get the current user details
    getLoggedInUser(token).then((res) => {
      // trigger redux action to add user data to store
      this.props.userFetched(res);
    });
  }

  submitForm() {}

  render() {
    const { showRegisterAttemptError, showRegisterServerError } = this.state;
    const user = this.props.user;
    return (
      <Fragment>
        {/* Navbar Component */}
        <Navbar />

        {/* other part of the page */}
        <div className="min-h-screen flex">
          <div className="flex-1 flex flex-col py-12 w-1/3 px-2 sm:px-3 lg:flex-none lg:px-10 xl:px-12">
            {user && (
              <Formik
                initialValues={{
                  password: "",
                  password_confirmation: "",
                  user_id: user.id,
                }}
                validationSchema={RegisterSchema}
                onSubmit={(values, { setSubmitting }) => {
                  setSubmitting(true);

                  // set boolean not to show error messages
                  this.setState({
                    showRegisterAttemptError: false,
                    showRegisterServerError: false,
                  });

                  // make a Register post request
                  axios
                    .post(CONFIG.SERVER_API + "patient/register/step-2", {
                      password: values.password,
                      password_confirmation: values.password_confirmation,
                      user_id: values.user_id,
                    })
                    .then(
                      (res) => {
                        console.log(res.data);
                        // redirect user to next step
                        this.props.history.push("/patient/dashboard");
                      },
                      (err) => {
                        console.log(err);
                        this.setState({ showRegisterServerError: true });
                        setSubmitting(false);
                      }
                    );
                }}
              >
                {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                }) => (
                  <form onSubmit={handleSubmit}>
                    <div className="mx-auto w-full max-w-sm">
                      <ProgressBar
                        percent={80}
                        filledBackground="linear-gradient(to right, #f7fafc, #33476a)"
                      >
                        <Step>
                          {({ accomplished, index }) => (
                            <div
                              className={`indexedStep bg-gray-400 p-2 rounded-full text-white ${
                                accomplished ? "accomplished" : null
                              }`}
                            >
                              {index + 1}
                            </div>
                          )}
                        </Step>
                        <Step>
                          {({ accomplished, index }) => (
                            <div
                              className={`indexedStep bg-gray-400 p-2 rounded-full text-white ${
                                accomplished ? "accomplished" : null
                              }`}
                            >
                              {index + 1}
                            </div>
                          )}
                        </Step>
                      </ProgressBar>
                      <div>
                        <h2 className="mt-6 text-center text-2xl leading-9 font-medium text-gray-900">
                          {user && `Hi ${user.last_name}, create your password`}
                          {/* Register as a new user */}
                        </h2>
                      </div>
                      <div className="mt-8">
                        <div className="mt-2">
                          <div className="flex justify-between">
                            <label
                              htmlFor="password"
                              className="block text-sm font-medium leading-5 text-gray-700"
                            >
                              password
                            </label>
                          </div>
                          <div className="mt-1 relative rounded-md shadow-sm">
                            <input
                              id="password"
                              name="password"
                              type="password"
                              placeholder="enter password"
                              required
                              className={
                                touched.password && errors.password
                                  ? "form-input block w-full sm:text-sm sm:leading-5 relative block w-full px-3 py-2 border border-red-300 placeholder-red-500 text-red-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                  : "form-input block w-full sm:text-sm sm:leading-5 relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                              }
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.password}
                            />
                          </div>
                        </div>

                        <div className="mt-2">
                          <div className="flex justify-between">
                            <label
                              htmlFor="password_confirmation"
                              className="block text-sm font-medium leading-5 text-gray-700"
                            >
                              Password Confirmation
                            </label>
                          </div>
                          <div className="mt-1 relative rounded-md shadow-sm">
                            <input
                              id="password_confirmation"
                              name="password_confirmation"
                              type="password"
                              placeholder="enter password again"
                              required
                              className={
                                touched.password_confirmation &&
                                errors.password_confirmation
                                  ? "form-input block w-full sm:text-sm sm:leading-5 relative block w-full px-3 py-2 border border-red-300 placeholder-red-500 text-red-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                  : "form-input block w-full sm:text-sm sm:leading-5 relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                              }
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.password_confirmation}
                            />
                          </div>
                        </div>
                      </div>

                      {showRegisterAttemptError && (
                        <Error message="Email has been used" />
                      )}

                      {errors.password_confirmation &&
                      touched.password_confirmation ? (
                        <Error message="password does not match" />
                      ) : null}

                      {showRegisterServerError && (
                        <Error message="Issues Connecting with the api server" />
                      )}

                      <div className="mt-6">
                        <MediumButton
                          type="submit"
                          title={isSubmitting ? "Processing..." : "Proceed"}
                          extraClass={
                            isSubmitting ? "opacity-50 cursor-not-allowed" : ""
                          }
                        />
                      </div>
                    </div>
                  </form>
                )}
              </Formik>
            )}
          </div>
          <div className="hidden lg:block relative flex-1">
            <img
              className="absolute inset-0 h-full w-full object-cover"
              src={asset2_bg_img}
              alt=""
            />
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.userAuth.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userFetched: (user) => dispatch(userFetched(user)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterFormStep2);
