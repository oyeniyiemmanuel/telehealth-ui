import React from "react";
import { Route, Switch } from "react-router-dom";
import NotFound from "../../components/widgets/NotFound";
import LoginForm from "./forms/Login";
import RegisterFormStep1 from "./forms/RegisterFormStep1";
import RegisterFormStep2 from "./forms/RegisterFormStep2";
import Dashboard from "./Dashboard";
import SelectDoctors from "./SelectDoctors";
import CallDoctor from "../callSession/twilio/CallDoctor";
import { AuthenticatedRoute } from "../../components/Routes";

const Patients = ({ match }) => {
  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${match.path}/register/step-1`}
          component={RegisterFormStep1}
        />
        <Route
          exact
          path={`${match.path}/register/step-2`}
          component={RegisterFormStep2}
        />
        <Route exact path={`${match.path}/login`} component={LoginForm} />
        <AuthenticatedRoute
          exact
          path={`${match.path}/dashboard`}
          component={Dashboard}
        />
        <AuthenticatedRoute
          exact
          path={`${match.path}/select-doctors`}
          component={SelectDoctors}
        />
        <AuthenticatedRoute
          exact
          path={`${match.path}/call-doctor/:doctor_id`}
          component={CallDoctor}
        />
        <Route component={NotFound} />
      </Switch>
    </div>
  );
};

export default Patients;
