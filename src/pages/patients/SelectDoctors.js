import React, { Fragment, Component } from "react";
import generalDash from "../../hoc/GeneralDashboard";
import { Html5Entities } from "html-entities";
import { getHospitalDoctors } from "../../accessors/hospital";
import { getLoggedInUser } from "../../accessors/user";
import { PreloaderDots } from "../../components/widgets/Preloader";
import { OnlineStatusAvatar } from "../../components/widgets/Avatars";
import { NavLink } from "react-router-dom";
import { SmallFeedbackMessage } from "../../components/widgets/Feedbacks";

class SelectDoctors extends Component {
  constructor(props) {
    super(props);

    this.state = {
      myHospitalDoctors: [],
      hospital_id: "",
      loading: true,
    };

    // get the current user details
    let token = localStorage.getItem("token");
    getLoggedInUser(token).then((res) => {
      // set state value of hospital_id
      this.setState({ hospital_id: res.hospital_id });

      // get all the doctors attached to patient's hospital
      getHospitalDoctors(this.state.hospital_id).then((res) => {
        this.setState({ myHospitalDoctors: res, loading: false });

        console.log(res);
      });
    });
  }

  render() {
    const { user } = this.props;
    const { myHospitalDoctors, loading } = this.state;
    const htmlEntities = new Html5Entities();
    return (
      <Fragment>
        <main
          className="flex-1 relative z-0 overflow-y-auto focus:outline-none"
          tabIndex="0"
        >
          <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
            <h1 className="text-2xl font-semibold text-gray-900">
              Talk To A Doctor
            </h1>
          </div>
          <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
            <div className="py-4">
              <div className="border-4 border-dashed border-gray-200 rounded-lg p-6">
                <div className="flex flex-col">
                  <div className="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
                    <div className="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                      <table className="min-w-full">
                        <thead>
                          <tr>
                            <th className="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                              Name
                            </th>
                            <th className="px-6 py-3 border-b border-gray-200 bg-gray-50"></th>
                          </tr>
                        </thead>
                        <tbody className="bg-white">
                          {loading ? (
                            <tr>
                              <td colSpan="4">
                                <PreloaderDots message="loading doctors" />
                              </td>
                            </tr>
                          ) : (
                            <Fragment>
                              {myHospitalDoctors.length > 0 ? (
                                myHospitalDoctors.map((doctor) => {
                                  return (
                                    <tr
                                      key={doctor.id}
                                      className="border-b border-gray-200"
                                    >
                                      <td className="px-2 py-4 md:px-6  whitespace-no-wrap">
                                        <div className="flex items-center">
                                          <div className="flex-shrink-0 h-10 w-10">
                                            <OnlineStatusAvatar {...doctor} />
                                          </div>
                                          <div className="ml-4">
                                            <div className="text-sm leading-5 font-medium text-gray-900">
                                              {doctor.last_name &&
                                                doctor.last_name}{" "}
                                              {doctor.first_name &&
                                                doctor.first_name}
                                            </div>
                                            <div className="text-xs leading-5 text-gray-500">
                                              <div className="text-xs leading-5 text-gray-900">
                                                {doctor.specialty ? (
                                                  doctor.specialty.name
                                                ) : (
                                                  <span className="ml-1 text-xs italic text-red-900">
                                                    (None Yet Selected)
                                                  </span>
                                                )}
                                              </div>
                                              <div className="text-xs leading-5 text-gray-500">
                                                <span className="font-bold">
                                                  {htmlEntities.decode(
                                                    "&#8358"
                                                  )}
                                                </span>{" "}
                                                {doctor.specialty &&
                                                doctor.specialty.prices.length >
                                                  0 ? (
                                                  <span className="text-green-900 font-bold">
                                                    {
                                                      doctor.specialty.prices[0]
                                                        .value
                                                    }
                                                  </span>
                                                ) : (
                                                  <span className="ml-1 text-xs italic text-red-900">
                                                    0.00
                                                  </span>
                                                )}
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </td>
                                      <td className="px-2 py-4 md:px-6  whitespace-no-wrap text-sm leading-5 text-gray-500">
                                        {doctor.specialty !== null &&
                                        doctor.specialty.prices.length > 0 ? (
                                          <span className="relative z-0 inline-flex shadow-sm">
                                            {doctor.isOnline ? (
                                              <NavLink
                                                to={`/patient/call-doctor/${doctor.id}`}
                                              >
                                                <span className="inline-flex rounded-md shadow-sm">
                                                  <button
                                                    type="button"
                                                    className="inline-flex items-center px-4 py-2 border border-teal-700 text-sm leading-5 font-medium rounded-md text-teal-900 bg-white hover:bg-teal-500 hover:text-white focus:outline-none focus:border-teal-700 focus:shadow-outline-teal active:bg-indigo-700 transition ease-in-out duration-150"
                                                  >
                                                    Call
                                                    <svg
                                                      className="ml-1 h-5 w-5"
                                                      fill="none"
                                                      stroke-linecap="round"
                                                      stroke-linejoin="round"
                                                      stroke-width="2"
                                                      viewBox="0 0 24 24"
                                                      stroke="currentColor"
                                                    >
                                                      <path d="M16 3h5m0 0v5m0-5l-6 6M5 3a2 2 0 00-2 2v1c0 8.284 6.716 15 15 15h1a2 2 0 002-2v-3.28a1 1 0 00-.684-.948l-4.493-1.498a1 1 0 00-1.21.502l-1.13 2.257a11.042 11.042 0 01-5.516-5.517l2.257-1.128a1 1 0 00.502-1.21L9.228 3.683A1 1 0 008.279 3H5z"></path>
                                                    </svg>
                                                  </button>
                                                </span>
                                              </NavLink>
                                            ) : (
                                              // <span className="inline-flex rounded-md shadow-sm">
                                              //   <button
                                              //     type="button"
                                              //     className="inline-flex items-center px-3 py-2 border border-gray-300 text-sm leading-4 font-medium rounded-md text-gray-700 bg-white hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:text-gray-800 active:bg-gray-50 transition ease-in-out duration-150"
                                              //   >
                                              //     Leave a Message
                                              //     <svg
                                              //       className="ml-2 -mr-1 h-5 w-5"
                                              //       fill="currentColor"
                                              //       viewBox="0 0 20 20"
                                              //     >
                                              //       <path
                                              //         fillRule="evenodd"
                                              //         d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884zM18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"
                                              //         clipRule="evenodd"
                                              //       />
                                              //     </svg>
                                              //   </button>
                                              // </span>
                                              ""
                                            )}

                                            {/* <button
                                            type="button"
                                            className="-ml-px relative inline-flex items-center px-4 py-2 rounded-r-md border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150"
                                          >
                                            Message
                                          </button> */}
                                          </span>
                                        ) : (
                                          <div className="bg-yellow-50 border-l-4 border-yellow-400 p-2">
                                            <div className="flex">
                                              <div className="flex-shrink-0">
                                                <svg
                                                  className="h-5 w-5 text-yellow-400"
                                                  fill="currentColor"
                                                  viewBox="0 0 20 20"
                                                >
                                                  <path
                                                    fillRule="evenodd"
                                                    d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                                                    clipRule="evenodd"
                                                  />
                                                </svg>
                                              </div>
                                              <div className="ml-3">
                                                <p className="text-xs leading-5 text-yellow-700">
                                                  {doctor.specialty === null &&
                                                    "Doctor has no speicialty"}

                                                  {doctor.specialty.prices
                                                    .length === 0 &&
                                                    "No Price Yet"}
                                                </p>
                                              </div>
                                            </div>
                                          </div>
                                        )}
                                      </td>
                                    </tr>
                                  );
                                })
                              ) : (
                                <tr>
                                  <td colSpan="4">
                                    <SmallFeedbackMessage message="No Doctors" />
                                  </td>
                                </tr>
                              )}
                            </Fragment>
                          )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                {/* <HospitalsDropdown /> */}

                {/* <VideoContainer /> */}
              </div>
            </div>
          </div>
        </main>
      </Fragment>
    );
  }
}

export default generalDash(SelectDoctors);
