import React, { Fragment } from "react";
import generalDash from "../../hoc/GeneralDashboard";
import HospitalsDropdown from "../../components/widgets/HospitalsDropdownForPatients";

const Dashboard = (props) => {
  const { user } = props;

  return (
    <Fragment>
      {user.hospital_id === null ? (
        <main
          className="flex-1 relative z-0 overflow-y-auto focus:outline-none"
          tabIndex="0"
        >
          <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
            <h1 className="text-2xl font-semibold text-gray-900">
              {user && user.last_name}
            </h1>
          </div>
          <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
            <div className="py-4">
              <div className="border-4 border-dashed border-gray-200 rounded-lg p-6">
                <div className="rounded-md bg-blue-50 p-4">
                  <div className="flex">
                    <div className="flex-shrink-0">
                      <svg
                        className="h-5 w-5 text-blue-400"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                      >
                        <path
                          fillRule="evenodd"
                          d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                          clipRule="evenodd"
                        />
                      </svg>
                    </div>
                    <div className="ml-3 flex-1 md:flex md:justify-between">
                      <p className="text-sm leading-5 text-blue-700">
                        We noticed you are not associated with any hospital yet,
                        you can select one below
                      </p>
                    </div>
                  </div>
                </div>

                <HospitalsDropdown />

                {/* <VideoContainer /> */}
              </div>
            </div>
          </div>
        </main>
      ) : (
        <Fragment>{props.history.push("/patient/select-doctors")}</Fragment>
      )}
    </Fragment>
  );
};

export default generalDash(Dashboard);
