import React, { Fragment } from "react";
import AlmostReadyImage from "../assets/img/almost-ready.png";
import Countdown from "react-countdown";
import generalDash from "../hoc/GeneralDashboard";

const readyDate = new Date(2020, 6, 1, 2, 33, 30, 0);

const renderer = ({ days, hours, minutes, seconds }) => {
  return (
    <dl className="-mx-8 -mt-8 flex flex-wrap">
      <div className="flex flex-col px-8 pt-8">
        <dt className="order-2 text-base leading-6 font-medium text-gray-500">
          Days
        </dt>
        <dd className="order-1 text-2xl leading-8 font-extrabold text-teal-600 sm:text-3xl sm:leading-9">
          {days}
        </dd>
      </div>
      <div className="flex flex-col px-8 pt-8">
        <dt className="order-2 text-base leading-6 font-medium text-gray-500">
          Hours
        </dt>
        <dd className="order-1 text-2xl leading-8 font-extrabold text-teal-600 sm:text-3xl sm:leading-9">
          {hours}
        </dd>
      </div>
      <div className="flex flex-col px-8 pt-8">
        <dt className="order-2 text-base leading-6 font-medium text-gray-500">
          Minutes
        </dt>
        <dd className="order-1 text-2xl leading-8 font-extrabold text-teal-600 sm:text-3xl sm:leading-9">
          {minutes}
        </dd>
      </div>
      <div className="flex flex-col px-8 pt-8">
        <dt className="order-2 text-base leading-6 font-medium text-gray-500">
          Seconds
        </dt>
        <dd className="order-1 text-2xl leading-8 font-extrabold text-teal-600 sm:text-3xl sm:leading-9">
          {seconds}
        </dd>
      </div>
    </dl>
  );
};

const AlmostReady = () => (
  <Fragment>
    <main
      className="flex-1 relative z-0 overflow-y-auto py-6 focus:outline-none"
      tabIndex="0"
    >
      <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
        {/* <!-- Replace with your content --> */}
        <div className="py-4">
          <div className="border-4 border-dashed border-gray-200 rounded-lg">
            <div>
              <div className="relative bg-teal-100">
                <div className="h-56 bg-teal-600 sm:h-72 lg:absolute lg:left-0 lg:h-full lg:w-1/2">
                  <img
                    className="w-full h-full object-cover"
                    src={AlmostReadyImage}
                    alt="Support team"
                  />
                </div>
                <div className="relative max-w-screen-xl mx-auto px-4 py-8 sm:py-12 sm:px-6 lg:py-16">
                  <div className="max-w-2xl mx-auto lg:max-w-none lg:mr-0 lg:ml-auto lg:w-1/2 lg:pl-10">
                    <div>
                      <div className="flex items-center justify-center h-12 w-12 text-teal-900">
                        <svg
                          className="h-12 w-12"
                          stroke="currentColor"
                          fill="none"
                          viewBox="0 0 24 24"
                        >
                          <path d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                        </svg>
                      </div>
                    </div>
                    <h2 className="mt-6 text-3xl leading-9 font-extrabold text-gray-900 sm:text-4xl sm:leading-10">
                      This Feature is being developed and tested
                    </h2>
                    <p className="mt-6 text-lg leading-7 text-gray-500">
                      It will be ready in...
                    </p>
                    <div className="mt-8 overflow-hidden">
                      <Countdown date={readyDate} renderer={renderer} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <!-- /End replace --> */}
      </div>
    </main>
  </Fragment>
);

export default generalDash(AlmostReady);
