import React, { Fragment, useState } from "react";
import generalDash from "../../hoc/GeneralDashboard";
import NewTeamMemberRegForm from "./forms/NewTeamMemberRegForm";
import HospitalTabWrapper from "./wrappers/HospitalTabWrapper";

const TeamSettings = (props) => {
  const [showNewMemberRegForm, setShowNewMemberRegForm] = useState(false);

  const toggleShowNewMemberRegForm = () => {
    setShowNewMemberRegForm(!showNewMemberRegForm);
  };

  return (
    <Fragment>
      <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
        <h1 className="text-2xl font-semibold text-gray-900 border-b border-gray-300 mb-4">
          Settings
        </h1>
      </div>

      <HospitalTabWrapper {...props}>
        <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
          {showNewMemberRegForm ? (
            <NewTeamMemberRegForm {...props} />
          ) : (
            <div className="grid grid-cols-4">
              <div className="col-span-3"></div>
              <div className="col-span-1">
                <button
                  onClick={toggleShowNewMemberRegForm}
                  className={`inline-flex w-full justify-center items-center px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-teal-600 hover:bg-teal-500 focus:outline-none focus:border-teal-700 focus:shadow-outline-teal active:bg-teal-700 transition ease-in-out duration-150`}
                >
                  New Team Member
                </button>
              </div>
            </div>
          )}

          <div className="hidden sm:block">
            <div className="py-5">
              <div className="border-t border-gray-200"></div>
            </div>
          </div>

          <div className="bg-white shadow overflow-hidden sm:rounded-md">
            <div className="flex flex-col">
              <div className="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
                <div className="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                  <table className="min-w-full">
                    <thead>
                      <tr>
                        <th className="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                          Name
                        </th>
                        <th className="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                          Specialty
                        </th>
                        <th className="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                          Status
                        </th>
                        <th className="px-6 py-3 border-b border-gray-200 bg-gray-50"></th>
                      </tr>
                    </thead>
                    <tbody className="bg-white">
                      {props.teamMembers !== undefined &&
                        Object.keys(props.teamMembers).map((each, key) => {
                          let member = props.teamMembers[each];
                          return (
                            <tr key={key}>
                              <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                <div className="flex items-center">
                                  <div className="flex-shrink-0 h-10 w-10">
                                    <svg
                                      className="h-10 w-10 rounded-full text-gray-300"
                                      fill="currentColor"
                                      viewBox="0 0 24 24"
                                    >
                                      <path d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z" />
                                    </svg>
                                  </div>
                                  <div className="ml-4">
                                    <div className="text-sm leading-5 font-medium text-gray-900">
                                      {member.last_name}
                                    </div>
                                    <div className="text-sm leading-5 text-gray-500">
                                      {member.phone}
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                <div className="text-sm leading-5 text-gray-500">
                                  {member.specialty ? (
                                    <span className="inline-flex items-center px-2.5 py-0.5 rounded-md text-sm font-medium leading-5 bg-indigo-100 text-indigo-800">
                                      <svg
                                        className="-ml-0.5 mr-1.5 h-2 w-2 text-indigo-400"
                                        fill="currentColor"
                                        viewBox="0 0 8 8"
                                      >
                                        <circle cx="4" cy="4" r="3" />
                                      </svg>
                                      {member.specialty.name}
                                    </span>
                                  ) : (
                                    <span className="ml-1 text-xs italic text-red-900">
                                      (None Yet Selected)
                                    </span>
                                  )}
                                </div>
                              </td>
                              <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                {member.email_verified_at ? (
                                  <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                    Verified
                                  </span>
                                ) : (
                                  <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">
                                    Unverified
                                  </span>
                                )}
                              </td>
                              <td className="px-6 py-4 whitespace-no-wrap text-right border-b border-gray-200 text-sm leading-5 font-medium">
                                {/* <a
                                href="/"
                                className="text-indigo-600 hover:text-indigo-900"
                              >
                                Edit
                              </a> */}
                              </td>
                            </tr>
                          );
                        })}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </HospitalTabWrapper>
    </Fragment>
  );
};
export default generalDash(TeamSettings);
