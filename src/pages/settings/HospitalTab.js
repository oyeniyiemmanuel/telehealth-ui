import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import generalDash from "../../hoc/GeneralDashboard";
import "react-step-progress-bar/styles.css";
import { ProgressBar } from "react-step-progress-bar";
import HospitalTabWrapper from "./wrappers/HospitalTabWrapper";

const HospitalTab = (props) => {
  const { user } = props;
  return (
    <Fragment>
      <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
        <h1 className="text-2xl font-semibold text-gray-900 border-b border-gray-300 mb-4">
          Settings
        </h1>
      </div>

      <HospitalTabWrapper {...props}>
        <Fragment>
          <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
            <h3 className="text-lg font-medium leading-6 text-gray-900 mb-6 md:w-2/4 md:mx-auto">
              {user.hospital.bank_account === null ||
              user.hospital.prices.length === 0 ? (
                <Fragment>
                  <div className="text-xs text-center">50% complete</div>
                  <ProgressBar
                    filledBackground="linear-gradient(to right, #f7fafc, #34a853)"
                    percent={50}
                  />
                </Fragment>
              ) : (
                <Fragment>
                  <div className="text-xs text-center">90% complete</div>
                  <ProgressBar
                    filledBackground="linear-gradient(to right, #f7fafc, #34a853)"
                    percent={90}
                  />
                  <div className="text-xs text-green-800 text-center">
                    Your hospital is ready to go LIVE and receive patients
                  </div>
                </Fragment>
              )}
            </h3>
            <div className="bg-white shadow overflow-hidden sm:rounded-md">
              <ul>
                <li>
                  <Link
                    to="/settings/hospital/edit-profile"
                    className="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out"
                  >
                    <div className="px-4 py-4 flex items-center sm:px-6">
                      <div className="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                        <div>
                          <div className="text-sm leading-5 font-medium text-indigo-600 truncate">
                            Hospital Details
                            <span className="ml-1 font-normal text-xs text-gray-500">
                              (Address, Website etc)
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className="ml-5 flex-shrink-0">
                        <svg
                          className="h-5 w-5 text-gray-400"
                          fill="currentColor"
                          viewBox="0 0 20 20"
                        >
                          <path
                            fillRule="evenodd"
                            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                            clipRule="evenodd"
                          />
                        </svg>
                      </div>
                    </div>
                  </Link>
                </li>
                <li className="border-t border-gray-200">
                  <Link
                    to="/settings/hospital/team"
                    className="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out"
                  >
                    <div className="px-4 py-4 flex items-center sm:px-6">
                      <div className="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                        <div>
                          <div className="text-sm leading-5 font-medium text-indigo-600 truncate">
                            Team Members
                            <span className="ml-1 font-normal text-xs text-gray-500">
                              (Invite doctors, admin, etc.)
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className="ml-5 flex-shrink-0">
                        <svg
                          className="h-5 w-5 text-gray-400"
                          fill="currentColor"
                          viewBox="0 0 20 20"
                        >
                          <path
                            fillRule="evenodd"
                            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                            clipRule="evenodd"
                          />
                        </svg>
                      </div>
                    </div>
                  </Link>
                </li>
                <li className="border-t border-gray-200">
                  <Link
                    to="/settings/hospital/tariff"
                    className="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out"
                  >
                    <div className="px-4 py-4 flex items-center sm:px-6">
                      <div className="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                        <div>
                          <div className="text-sm leading-5 font-medium text-indigo-600 truncate">
                            Set Your Tariff
                            <span className="ml-1 font-normal text-xs text-gray-500">
                              (Create Specialties and Prices)
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className="ml-5 flex-shrink-0">
                        <svg
                          className="h-5 w-5 text-gray-400"
                          fill="currentColor"
                          viewBox="0 0 20 20"
                        >
                          <path
                            fillRule="evenodd"
                            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                            clipRule="evenodd"
                          />
                        </svg>
                      </div>
                    </div>
                  </Link>
                </li>
                <li className="border-t border-gray-200">
                  <Link
                    to="/settings/hospital/patients"
                    className="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out"
                  >
                    <div className="px-4 py-4 flex items-center sm:px-6">
                      <div className="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                        <div>
                          <div className="text-sm leading-5 font-medium text-indigo-600 truncate">
                            Let Your Patients Know
                            <span className="ml-1 font-normal text-xs text-gray-500">
                              (Invite your patients)
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className="ml-5 flex-shrink-0">
                        <svg
                          className="h-5 w-5 text-gray-400"
                          fill="currentColor"
                          viewBox="0 0 20 20"
                        >
                          <path
                            fillRule="evenodd"
                            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                            clipRule="evenodd"
                          />
                        </svg>
                      </div>
                    </div>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </Fragment>
      </HospitalTabWrapper>
    </Fragment>
  );
};

export default generalDash(HospitalTab);
