import React, { Fragment } from "react";
import { Link, useLocation } from "react-router-dom";
import { Back } from "../../../components/widgets/DirectionNavs";

const LocationGetter = () => {
  let location = useLocation();
  return location.pathname;
};

const HospitalTabWrapper = (props) => {
  const { user } = props;
  let currentLocation = LocationGetter();
  return (
    <Fragment>
      <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
        <div className="sm:block">
          <div className="">
            <nav className="flex -mb-px">
              <Link
                to="/settings/user"
                className="ml-2 group inline-flex items-center py-4 px-3 border-b-2 border-transparent font-medium text-sm leading-5 text-gray-500 hover:text-gray-700 focus:outline-none focus:text-gray-700"
              >
                <svg
                  className="-ml-0.5 mr-2 h-5 w-5 text-gray-400 group-hover:text-gray-500 group-focus:text-gray-600"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  fill="none"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                </svg>
                <span>My Profile</span>
              </Link>

              {user.permissions &&
              user.permissions.includes("edit hospital profile") ? (
                <Link
                  to="/settings/hospital"
                  className="group bg-teal-100 shadow-inner inline-flex items-center py-4 px-3 rounded-t-md border-b-2 border-teal-100 font-medium text-sm leading-5 text-teal-800 focus:outline-none focus:text-teal-800"
                  aria-current="page"
                >
                  <svg
                    className="-ml-0.5 mr-2 h-5 w-5 text-teal-500 group-focus:text-teal-600"
                    viewBox="0 0 20 20"
                    fill="none"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="1"
                    stroke="currentColor"
                  >
                    <path d="M19 21V5a2 2 0 00-2-2H7a2 2 0 00-2 2v16m14 0h2m-2 0h-5m-9 0H3m2 0h5M9 7h1m-1 4h1m4-4h1m-1 4h1m-5 10v-5a1 1 0 011-1h2a1 1 0 011 1v5m-4 0h4"></path>
                  </svg>
                  <span>Hospital Profile</span>
                </Link>
              ) : (
                ""
              )}
            </nav>
          </div>
        </div>

        {/* content area */}
        <div className="bg-teal-100 py-6 w-full rounded-lg border border-gray-200">
          {/* only display backward link on content pages that aren't the same as the wrapper */}
          {currentLocation !== "/settings/hospital" &&
          currentLocation !== "/settings" ? (
            <Back link="/settings/hospital" />
          ) : (
            ""
          )}

          {/* show content here */}
          {props.children}
        </div>
      </div>
    </Fragment>
  );
};

export default HospitalTabWrapper;
