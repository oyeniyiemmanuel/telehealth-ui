import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import generalDash from "../../hoc/GeneralDashboard";
import "react-step-progress-bar/styles.css";
import { ProgressBar } from "react-step-progress-bar";
import UserTabWrapper from "./wrappers/UserTabWrapper";

const UserTab = (props) => {
  const { user } = props;
  return (
    <Fragment>
      <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
        <h1 className="text-2xl font-semibold text-gray-900 border-b border-gray-300 mb-4">
          Settings
        </h1>
      </div>

      <UserTabWrapper {...props}>
        <Fragment>
          <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
            <h3 className="text-lg font-medium leading-6 text-gray-900 mb-6 md:w-2/4 md:mx-auto">
              {user.phone === null || user.first_name === null ? (
                <Fragment>
                  <div className="text-xs text-center">50% complete</div>
                  <ProgressBar
                    filledBackground="linear-gradient(to right, #f7fafc, #34a853)"
                    percent={50}
                  />
                </Fragment>
              ) : (
                <Fragment>
                  <div className="text-xs text-center">100% complete</div>
                  <ProgressBar
                    filledBackground="linear-gradient(to right, #f7fafc, #34a853)"
                    percent={100}
                  />
                </Fragment>
              )}
            </h3>
            <div className="bg-white shadow overflow-hidden sm:rounded-md">
              <ul>
                <li>
                  <Link
                    to="/settings/user/edit-profile"
                    className="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out"
                  >
                    <div className="px-4 py-4 flex items-center sm:px-6">
                      <div className="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                        <div>
                          <div className="text-sm leading-5 font-medium text-indigo-600 truncate">
                            My BioData
                            <span className="ml-1 font-normal text-xs text-gray-500">
                              (Phone, Email etc)
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className="ml-5 flex-shrink-0">
                        <svg
                          className="h-5 w-5 text-gray-400"
                          fill="currentColor"
                          viewBox="0 0 20 20"
                        >
                          <path
                            fillRule="evenodd"
                            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                            clipRule="evenodd"
                          />
                        </svg>
                      </div>
                    </div>
                  </Link>
                </li>
                {user.patient_id && (
                  <li className="border-t border-gray-200">
                    <Link
                      to="/settings/user/my-associated-hospitals"
                      className="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out"
                    >
                      <div className="px-4 py-4 flex items-center sm:px-6">
                        <div className="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                          <div>
                            <div className="text-sm leading-5 font-medium text-indigo-600 truncate">
                              My Associated Hospitals
                              <span className="ml-1 font-normal text-xs text-gray-500">
                                ({user.hospital && user.hospital.name})
                              </span>
                            </div>
                          </div>
                        </div>
                        <div className="ml-5 flex-shrink-0">
                          <svg
                            className="h-5 w-5 text-gray-400"
                            fill="currentColor"
                            viewBox="0 0 20 20"
                          >
                            <path
                              fillRule="evenodd"
                              d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </div>
                      </div>
                    </Link>
                  </li>
                )}
              </ul>
            </div>
          </div>
        </Fragment>
      </UserTabWrapper>
    </Fragment>
  );
};

export default generalDash(UserTab);
