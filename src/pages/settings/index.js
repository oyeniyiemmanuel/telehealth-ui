/* eslint-disable react/prop-types */
import React from "react";
import { Route, Switch } from "react-router-dom";
import NotFound from "../../components/widgets/NotFound";
import ContentPane from "./ContentPane";
import HospitalTab from "./HospitalTab";
import UserTab from "./UserTab";
import TeamSettings from "./TeamSettings";
import TariffSettings from "./TariffSettings";
import PatientSettings from "./PatientSettings";
import MyAssociatedHospitalSettings from "./MyAssociatedHospitalSettings";
import VerifiedDoctor from "./VerifiedDoctor";
import EditHospital from "./forms/EditHospital";
import EditUserProfile from "./forms/EditUserProfile";
import CompleteDoctorProfileForm from "./forms/CompleteDoctorProfile";
import { AuthenticatedRoute } from "../../components/Routes";

const Settings = ({ match }) => {
  return (
    <div>
      <Switch>
        <AuthenticatedRoute
          exact
          path={`${match.path}/hospital/edit-profile`}
          component={EditHospital}
        />
        <AuthenticatedRoute
          exact
          path={`${match.path}/hospital/team`}
          component={TeamSettings}
        />
        <AuthenticatedRoute
          exact
          path={`${match.path}/hospital/tariff`}
          component={TariffSettings}
        />
        <AuthenticatedRoute
          exact
          path={`${match.path}/hospital/patients`}
          component={PatientSettings}
        />
        <AuthenticatedRoute
          exact
          path={`${match.path}/doctor/edit-profile`}
          component={EditUserProfile}
        />
        <Route
          exact
          path={`${match.path}/doctor/verified/:token`}
          component={VerifiedDoctor}
        />
        <Route
          exact
          path={`${match.path}/doctor/complete-profile`}
          component={CompleteDoctorProfileForm}
        />
        <AuthenticatedRoute
          exact
          path={`${match.path}/hospital`}
          component={HospitalTab}
        />
        <AuthenticatedRoute
          exact
          path={`${match.path}/user`}
          component={UserTab}
        />
        <AuthenticatedRoute
          exact
          path={`${match.path}/user/edit-profile`}
          component={EditUserProfile}
        />
        <AuthenticatedRoute
          exact
          path={`${match.path}/user/my-associated-hospitals`}
          component={MyAssociatedHospitalSettings}
        />
        <AuthenticatedRoute exact path={`${match.path}`} component={UserTab} />
        <Route component={NotFound} />
      </Switch>
    </div>
  );
};

export default Settings;
