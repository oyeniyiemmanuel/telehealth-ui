import React, { Fragment } from "react";
import newlyVerifiedUserDash from "../../hoc/NewlyVerifiedUserDashboard";
import { PreloaderDots } from "../../components/widgets/Preloader";

const VerifiedDoctor = (props) => {
  return (
    <Fragment>
      <PreloaderDots message="Redirecting..." />
    </Fragment>
  );
};
export default newlyVerifiedUserDash(VerifiedDoctor);
