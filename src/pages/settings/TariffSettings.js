import React, { Fragment } from "react";
import generalDash from "../../hoc/GeneralDashboard";
import axios from "axios";
import { Formik } from "formik";
import { CONFIG } from "../../app.config";
import { MediumButton } from "../../components/widgets/Buttons";
import { useAlert } from "react-alert";
import HospitalTabWrapper from "./wrappers/HospitalTabWrapper";

const TariffSettings = (props) => {
  const { specialties, user } = props;
  const alert = useAlert();

  return (
    <Fragment>
      <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
        <h1 className="text-2xl font-semibold text-gray-900 border-b border-gray-300 mb-4">
          Settings
        </h1>
      </div>

      <HospitalTabWrapper {...props}>
        <div className="ml-8 px-4 sm:px-0">
          <h3 className="text-lg font-medium leading-6 text-gray-900">
            Tariff
          </h3>
          <p className="mt-1 text-sm leading-5 text-gray-600">
            Set prices for physicians' specialties who will be consulting
            patients through your hospital
          </p>
        </div>

        <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
          <div className="hidden sm:block">
            <div className="py-5">
              <div className="border-t border-gray-200"></div>
            </div>
          </div>

          <div className="bg-white shadow overflow-hidden sm:rounded-md">
            <div className="flex flex-col">
              <div className="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
                <div className="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                  <Fragment>
                    <Formik
                      initialValues={{
                        prices: [],
                      }}
                      onSubmit={(values, { setSubmitting }) => {
                        setSubmitting(true);
                        console.log(values);

                        // make attempt to submit prices
                        axios
                          .post(CONFIG.SERVER_API + "specialty/set-prices", {
                            pricesData: values,
                            hospital_id: user.hospital_id,
                            user_id: user.id,
                          })
                          .then(
                            (res) => {
                              // console.log(res.data.specialties);
                              // Notification of success
                              let alertOptions = {
                                type: "success",
                              };
                              alert.show("Prices Updated", alertOptions);

                              // dispatch specialties in redux store
                              props.specialtiesFetched(res.data.specialties);

                              setSubmitting(false);
                            },
                            (err) => {
                              console.log(err);
                              setSubmitting(false);
                            }
                          );
                      }}
                    >
                      {({
                        values,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                      }) => (
                        <form onSubmit={handleSubmit}>
                          <table className="min-w-full">
                            <thead>
                              <tr>
                                <th className="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                  Name
                                </th>
                                <th className="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                  Price (NGN)
                                </th>
                                <th className="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                  No. of verified Drs
                                </th>
                              </tr>
                            </thead>
                            <tbody className="bg-white">
                              {specialties.map((specialty) => {
                                return (
                                  <tr key={specialty.name}>
                                    <td className="px-4 py-3 whitespace-no-wrap border-b border-gray-200">
                                      <div className="flex items-center">
                                        <div className="">
                                          <div className="text-sm leading-5 font-medium text-gray-900">
                                            {specialty.name}
                                          </div>
                                        </div>
                                      </div>
                                    </td>
                                    <td className="px-4 py-3 whitespace-no-wrap border-b border-gray-200">
                                      <div className="text-sm leading-5 text-gray-900">
                                        <div className="">
                                          <input
                                            aria-label="price"
                                            name={specialty.name}
                                            type="number"
                                            className="appearance-none rounded-none relative block px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                            placeholder={
                                              specialty.prices[0]
                                                ? specialty.prices[0].value
                                                : "0.00"
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={
                                              values.prices[specialty.name]
                                            }
                                          />
                                        </div>
                                      </div>
                                    </td>
                                    <td className="px-4 py-3 whitespace-no-wrap border-b border-gray-200">
                                      <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">
                                        0
                                      </span>
                                    </td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>

                          <div className="my-6  md:w-1/2 md:mx-auto">
                            <MediumButton
                              type="submit"
                              title={
                                isSubmitting ? "Processing..." : "Update Prices"
                              }
                              extraClass={
                                isSubmitting
                                  ? "opacity-50 cursor-not-allowed"
                                  : ""
                              }
                            />
                          </div>
                        </form>
                      )}
                    </Formik>
                  </Fragment>
                </div>
              </div>
            </div>
          </div>
        </div>
      </HospitalTabWrapper>
    </Fragment>
  );
};
export default generalDash(TariffSettings);
