import React, { Fragment, useState } from "react";
import generalDash from "../../hoc/GeneralDashboard";
import { CONFIG } from "../../app.config";
import NewPatientAddHospitalForm from "./forms/NewPatientAddHospitalForm";
import UserTabWrapper from "./wrappers/UserTabWrapper";
import axios from "axios";

const MyAssociatedHospitalSettings = (props) => {
  const [showSwitchHospitalForm, setShowSwitchHospitalForm] = useState(false);

  const toggleShowSwitchHospitalForm = () => {
    setShowSwitchHospitalForm(!showSwitchHospitalForm);
  };

  const makeDefaultHospital = (hospitalId) => () => {
    // send request to the server
    axios
      .post(CONFIG.SERVER_API + "patient/make-default-hospital", {
        hospital_id: hospitalId,
        user_id: props.user.id,
      })
      .then(
        (res) => {
          window.location = `${CONFIG.CLIENT_URL}settings/user/my-associated-hospitals`;
        },
        (err) => {
          console.log(err);
        }
      );
  };

  const removeHospitalFromMyList = (hospitalId) => () => {
    // send request to the server
    axios
      .post(CONFIG.SERVER_API + "patient/remove-hospital-from-my-list", {
        hospital_id: hospitalId,
        user_id: props.user.id,
      })
      .then(
        (res) => {
          window.location = `${CONFIG.CLIENT_URL}settings/user/my-associated-hospitals`;
        },
        (err) => {
          console.log(err);
        }
      );
  };

  const { user } = props;

  return (
    <Fragment>
      <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
        <h1 className="text-2xl font-semibold text-gray-900 border-b border-gray-300 mb-4">
          Settings
        </h1>
      </div>

      <UserTabWrapper {...props}>
        <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
          {showSwitchHospitalForm ? (
            <NewPatientAddHospitalForm {...props} />
          ) : (
            <div className="grid grid-cols-4 mb-4">
              <div className="col-span-3 sm:col-span-1"></div>
              <div className="col-span-1 sm:col-span-3">
                <button
                  onClick={toggleShowSwitchHospitalForm}
                  className={`inline-flex w-full justify-center items-center px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-teal-600 hover:bg-teal-500 focus:outline-none focus:border-teal-700 focus:shadow-outline-teal active:bg-teal-700 transition ease-in-out duration-150`}
                >
                  Add New Hospital
                </button>
              </div>
            </div>
          )}

          <div className="hidden sm:block">
            <div className="py-5">
              <div className="border-t border-gray-200"></div>
            </div>
          </div>

          <div className="bg-white shadow overflow-hidden sm:rounded-md">
            <div className="flex flex-col">
              <div className="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
                <div className="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                  <table className="min-w-full">
                    <thead>
                      <tr>
                        <th className="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                          Hospital Name
                        </th>
                        <th className="px-6 py-3 border-b border-gray-200 bg-gray-50"></th>
                      </tr>
                    </thead>
                    <tbody className="bg-white">
                      {user.hospitals !== undefined &&
                        Object.keys(user.hospitals).map((each, key) => {
                          let hospital = user.hospitals[each];
                          return (
                            <tr key={key}>
                              <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                <div className="flex items-center">
                                  <div className="flex-shrink-0 h-10 w-10">
                                    <svg
                                      className="h-10 w-10 rounded-full text-gray-300"
                                      viewBox="0 0 24 24"
                                      fill="none"
                                      strokeLinecap="round"
                                      strokeLinejoin="round"
                                      strokeWidth="2"
                                      stroke="currentColor"
                                    >
                                      <path d="M19 21V5a2 2 0 00-2-2H7a2 2 0 00-2 2v16m14 0h2m-2 0h-5m-9 0H3m2 0h5M9 7h1m-1 4h1m4-4h1m-1 4h1m-5 10v-5a1 1 0 011-1h2a1 1 0 011 1v5m-4 0h4"></path>{" "}
                                    </svg>
                                  </div>
                                  <div className="ml-4">
                                    <div className="text-sm leading-5 font-medium text-gray-900">
                                      {hospital.name}
                                    </div>
                                    <div className="text-sm leading-5 text-gray-500">
                                      {hospital.street && hospital.street}{" "}
                                      {hospital.city && hospital.city}{" "}
                                      {hospital.state && hospital.state}
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td className="px-6 py-4 whitespace-no-wrap text-center border-b border-gray-200 text-sm leading-5 font-medium">
                                {hospital.id !== user.hospital.id ? (
                                  <span className="relative z-0 inline-flex shadow-sm rounded-md">
                                    <button
                                      onClick={makeDefaultHospital(hospital.id)}
                                      className="relative inline-flex items-center px-4 py-2 rounded-l-md border border-gray-300 bg-green-100 text-xs leading-5 font-medium text-gray-700 hover:text-gray-500 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150"
                                    >
                                      <svg
                                        className="-ml-1 mr-2 mb-1 h-4 w-4 text-green-800"
                                        viewBox="0 0 20 20"
                                        fill="none"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth="2"
                                        stroke="currentColor"
                                      >
                                        <path d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z"></path>
                                      </svg>
                                      Make Default
                                    </button>
                                    <button
                                      onClick={removeHospitalFromMyList(
                                        hospital.id
                                      )}
                                      className="-ml-px relative inline-flex items-center px-3 py-2 rounded-r-md border border-gray-300 bg-red-100 text-xs leading-5 font-medium text-gray-700 hover:text-gray-500 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150"
                                    >
                                      Remove From My List
                                      <svg
                                        className="-mr-1 ml-2 mb-1 h-4 w-4 text-red-800"
                                        viewBox="0 0 20 20"
                                        fill="none"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth="2"
                                        stroke="currentColor"
                                      >
                                        <path d="M6 18L18 6M6 6l12 12"></path>{" "}
                                      </svg>
                                    </button>
                                  </span>
                                ) : (
                                  <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                    Current Default Hospital
                                  </span>
                                )}
                              </td>
                            </tr>
                          );
                        })}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </UserTabWrapper>
    </Fragment>
  );
};
export default generalDash(MyAssociatedHospitalSettings);
