import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import generalDash from "../../hoc/GeneralDashboard";
import "react-step-progress-bar/styles.css";
import { ProgressBar } from "react-step-progress-bar";

const ContentPane = (props) => {
  const { user } = props;
  return (
    <Fragment>
      <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
        <h1 className="text-2xl font-semibold text-gray-900 border-b border-gray-300 mb-4">
          Settings
        </h1>
      </div>

      {/* Display hospital settings if user has permission */}
      {user.permissions.includes("edit hospital profile") && (
        <Fragment>
          <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
            <h3 className="text-lg font-medium leading-6 text-gray-900 mb-6 md:w-1/3">
              Hospital Profile
              {user.hospital.bank_account === null ||
              user.hospital.prices.length === 0 ? (
                <Fragment>
                  <span className="ml-4 text-xs">50% complete</span>
                  <ProgressBar
                    filledBackground="linear-gradient(to right, #f7fafc, #34a853)"
                    percent={50}
                  />
                </Fragment>
              ) : (
                <Fragment>
                  <span className="ml-4 text-xs">90% complete</span>
                  <ProgressBar
                    filledBackground="linear-gradient(to right, #f7fafc, #34a853)"
                    percent={90}
                  />
                  <span className="text-xs text-green-800">
                    Your hospital is ready to go LIVE and receive patients
                  </span>
                </Fragment>
              )}
            </h3>

            <div className="bg-white shadow overflow-hidden sm:rounded-md">
              <ul>
                <li>
                  <Link
                    to="/settings/hospital/edit-profile"
                    className="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out"
                  >
                    <div className="px-4 py-4 flex items-center sm:px-6">
                      <div className="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                        <div>
                          <div className="text-sm leading-5 font-medium text-indigo-600 truncate">
                            Hospital Details
                            <span className="ml-1 font-normal text-xs text-gray-500">
                              (Address, Website etc)
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className="ml-5 flex-shrink-0">
                        <svg
                          className="h-5 w-5 text-gray-400"
                          fill="currentColor"
                          viewBox="0 0 20 20"
                        >
                          <path
                            fillRule="evenodd"
                            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                            clipRule="evenodd"
                          />
                        </svg>
                      </div>
                    </div>
                  </Link>
                </li>
                <li className="border-t border-gray-200">
                  <Link
                    to="/settings/hospital/team"
                    className="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out"
                  >
                    <div className="px-4 py-4 flex items-center sm:px-6">
                      <div className="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                        <div>
                          <div className="text-sm leading-5 font-medium text-indigo-600 truncate">
                            Team Members
                            <span className="ml-1 font-normal text-xs text-gray-500">
                              (Invite doctors, admin, etc.)
                            </span>
                          </div>
                        </div>
                        <div className="mt-4 flex-shrink-0 sm:mt-0">
                          <div className="flex overflow-hidden">
                            <img
                              className="inline-block h-6 w-6 rounded-full text-white shadow-solid"
                              src="https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                              alt=""
                            />
                            <img
                              className="-ml-1 inline-block h-6 w-6 rounded-full text-white shadow-solid"
                              src="https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                              alt=""
                            />
                            <img
                              className="-ml-1 inline-block h-6 w-6 rounded-full text-white shadow-solid"
                              src="https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.25&w=256&h=256&q=80"
                              alt=""
                            />
                            <img
                              className="-ml-1 inline-block h-6 w-6 rounded-full text-white shadow-solid"
                              src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                              alt=""
                            />
                          </div>
                        </div>
                      </div>
                      <div className="ml-5 flex-shrink-0">
                        <svg
                          className="h-5 w-5 text-gray-400"
                          fill="currentColor"
                          viewBox="0 0 20 20"
                        >
                          <path
                            fillRule="evenodd"
                            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                            clipRule="evenodd"
                          />
                        </svg>
                      </div>
                    </div>
                  </Link>
                </li>
                <li className="border-t border-gray-200">
                  <Link
                    to="/settings/hospital/tariff"
                    className="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out"
                  >
                    <div className="px-4 py-4 flex items-center sm:px-6">
                      <div className="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                        <div>
                          <div className="text-sm leading-5 font-medium text-indigo-600 truncate">
                            Set Your Tariff
                            <span className="ml-1 font-normal text-xs text-gray-500">
                              (Create Specialties and Prices)
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className="ml-5 flex-shrink-0">
                        <svg
                          className="h-5 w-5 text-gray-400"
                          fill="currentColor"
                          viewBox="0 0 20 20"
                        >
                          <path
                            fillRule="evenodd"
                            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                            clipRule="evenodd"
                          />
                        </svg>
                      </div>
                    </div>
                  </Link>
                </li>
                <li className="border-t border-gray-200">
                  <Link
                    to="/settings/hospital/patients"
                    className="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out"
                  >
                    <div className="px-4 py-4 flex items-center sm:px-6">
                      <div className="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                        <div>
                          <div className="text-sm leading-5 font-medium text-indigo-600 truncate">
                            Let Your Patients Know
                            <span className="ml-1 font-normal text-xs text-gray-500">
                              (Invite your patients)
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className="ml-5 flex-shrink-0">
                        <svg
                          className="h-5 w-5 text-gray-400"
                          fill="currentColor"
                          viewBox="0 0 20 20"
                        >
                          <path
                            fillRule="evenodd"
                            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                            clipRule="evenodd"
                          />
                        </svg>
                      </div>
                    </div>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
          <div className="hidden sm:block">
            <div className="py-5">
              <div className="border-t border-gray-200"></div>
            </div>
          </div>{" "}
        </Fragment>
      )}

      {/* Display patient profile settings if user is a patient */}
      {user.roles.includes("patient") && (
        <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
          <h3 className="text-lg font-medium leading-6 text-gray-900 mb-6 md:w-1/3">
            Your Profile
            {user.email === null ? (
              <Fragment>
                <span className="ml-4 text-xs">50% complete</span>
                <ProgressBar
                  filledBackground="linear-gradient(to right, #f7fafc, #34a853)"
                  percent={50}
                />
              </Fragment>
            ) : (
              <Fragment>
                <span className="ml-4 text-xs">90% complete</span>
                <ProgressBar
                  filledBackground="linear-gradient(to right, #f7fafc, #34a853)"
                  percent={90}
                />
              </Fragment>
            )}
          </h3>

          <div className="bg-white shadow overflow-hidden sm:rounded-md">
            <ul>
              <li>
                <Link
                  to={`/almost-ready`}
                  className="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out"
                >
                  <div className="px-4 py-4 flex items-center sm:px-6">
                    <div className="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                      <div>
                        <div className="text-sm leading-5 font-medium text-indigo-600 truncate">
                          Personal Details
                          <span className="ml-1 font-normal text-xs text-gray-500">
                            (Name, Phone, Email etc)
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="ml-5 flex-shrink-0">
                      <svg
                        className="h-5 w-5 text-gray-400"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                      >
                        <path
                          fillRule="evenodd"
                          d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                          clipRule="evenodd"
                        />
                      </svg>
                    </div>
                  </div>
                </Link>
              </li>
              <li className="border-t border-gray-200">
                <Link
                  to="/almost-ready"
                  className="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out"
                >
                  <div className="px-4 py-4 flex items-center sm:px-6">
                    <div className="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                      <div>
                        <div className="text-sm leading-5 font-medium text-indigo-600 truncate">
                          Medical Details
                          <span className="ml-1 font-normal text-xs text-gray-500">
                            (Blood Group, Allergies etc.)
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="ml-5 flex-shrink-0">
                      <svg
                        className="h-5 w-5 text-gray-400"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                      >
                        <path
                          fillRule="evenodd"
                          d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                          clipRule="evenodd"
                        />
                      </svg>
                    </div>
                  </div>
                </Link>
              </li>
              <li className="border-t border-gray-200">
                <Link
                  to="/almost-ready"
                  className="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out"
                >
                  <div className="px-4 py-4 flex items-center sm:px-6">
                    <div className="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                      <div>
                        <div className="text-sm leading-5 font-medium text-indigo-600 truncate">
                          Payment Details
                          <span className="ml-1 font-normal text-xs text-gray-500">
                            (Set Your Preferred Payment Methods)
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="ml-5 flex-shrink-0">
                      <svg
                        className="h-5 w-5 text-gray-400"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                      >
                        <path
                          fillRule="evenodd"
                          d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                          clipRule="evenodd"
                        />
                      </svg>
                    </div>
                  </div>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      )}

      {/* Display doctor profile settings if user is a doctor */}
      {user.roles.includes("doctor") && (
        <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
          <h3 className="text-lg font-medium leading-6 text-gray-900 mb-6 md:w-1/3">
            Your Profile
            {user.phone === null ? (
              <Fragment>
                <span className="ml-4 text-xs">50% complete</span>
                <ProgressBar
                  filledBackground="linear-gradient(to right, #f7fafc, #34a853)"
                  percent={50}
                />
              </Fragment>
            ) : (
              <Fragment>
                <span className="ml-4 text-xs">100% complete</span>
                <ProgressBar
                  filledBackground="linear-gradient(to right, #f7fafc, #34a853)"
                  percent={100}
                />
              </Fragment>
            )}
          </h3>

          <div className="bg-white shadow overflow-hidden sm:rounded-md">
            <ul>
              <li>
                <Link
                  to="/settings/doctor/edit-profile"
                  className="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out"
                >
                  <div className="px-4 py-4 flex items-center sm:px-6">
                    <div className="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                      <div>
                        <div className="text-sm leading-5 font-medium text-indigo-600 truncate">
                          Personal Details
                          <span className="ml-1 font-normal text-xs text-gray-500">
                            (Name, Phone, Email etc)
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="ml-5 flex-shrink-0">
                      <svg
                        className="h-5 w-5 text-gray-400"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                      >
                        <path
                          fillRule="evenodd"
                          d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                          clipRule="evenodd"
                        />
                      </svg>
                    </div>
                  </div>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      )}
    </Fragment>
  );
};

export default generalDash(ContentPane);
