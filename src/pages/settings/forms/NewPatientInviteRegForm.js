import React, { Fragment, useState } from "react";
import { connect } from "react-redux";
import { Formik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import { CONFIG } from "../../../app.config";
import { fetchHospitalPatients } from "../../../redux/actions";
import { getHospitalPatients } from "../../../accessors/hospital";
import { MediumButton } from "../../../components/widgets/Buttons";
import Error from "../../../components/widgets/error";
import { useAlert } from "react-alert";

const NewPatientInviteRegForm = (props) => {
  const [showSubmitError1, setShowSubmitError1] = useState(false);
  const [showSubmitError2, setShowSubmitError2] = useState(false);
  const { user } = props;
  const alert = useAlert();

  const NewPatientInviteRegFormSchema = Yup.object().shape({
    last_name: Yup.string().required("Please Enter Doctor's Surname"),
    email: Yup.string().required("Please Enter Doctor's email"),
  });

  return (
    <Fragment>
      <div>
        <div className="md:grid md:grid-cols-4 md:gap-6">
          <div className="md:col-span-2">
            <div className="px-4 sm:px-0">
              <h3 className="text-lg font-medium leading-6 text-gray-900">
                Invite New Patient
              </h3>
              <p className="mt-1 text-sm leading-5 text-gray-600">
                This sends an invitation/verification link to their email.
              </p>
            </div>
          </div>
          <div className="mt-5 md:mt-0 md:col-span-2">
            <Formik
              initialValues={{
                hospital_id: user.hospital.id,
                last_name: "",
                email: "",
              }}
              onSubmit={(
                values,
                { initialValues, resetForm, setSubmitting }
              ) => {
                setSubmitting(true);
                setShowSubmitError1(false);
                setShowSubmitError2(false);
                // send request to the server
                axios
                  .post(CONFIG.SERVER_API + "hospital/invite/new-patient", {
                    hospital_id: values.hospital_id,
                    last_name: values.last_name,
                    email: values.email,
                  })
                  .then(
                    (res) => {
                      if (res.data.message === "user exists in your hospital") {
                        setSubmitting(false);
                        setShowSubmitError1(true);
                      } else if (
                        res.data.message ===
                        "user exists but not in your hospital"
                      ) {
                        setSubmitting(false);
                        setShowSubmitError2(true);
                      } else {
                        // Notification of success
                        let alertOptions = {
                          type: "success",
                        };
                        alert.show(
                          `Invitation email has been sent to ${values.last_name}`,
                          alertOptions
                        );
                        setSubmitting(false);
                        // get the current user's hospital patients
                        getHospitalPatients(user.hospital.id).then((res) => {
                          // trigger redux action to add patients data to store
                          props.dispatch(fetchHospitalPatients(res));
                        });

                        // reset form values
                        resetForm(initialValues);

                        // Redirect
                        props.history.push("/settings/hospital/patients");
                      }
                    },
                    (err) => {
                      console.log(err);
                    }
                  );
              }}
              validationSchema={NewPatientInviteRegFormSchema}
            >
              {({
                values,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
              }) => (
                <form onSubmit={handleSubmit}>
                  <div className="shadow sm:rounded-md sm:overflow-hidden">
                    <div className=" bg-white sm:p-6">
                      <div className="grid grid-cols-6 gap-6 mt-6">
                        <div className="col-span-6 sm:col-span-6 lg:col-span-6">
                          <label
                            htmlFor="last_name"
                            className="block text-sm font-medium leading-5 text-gray-700"
                          >
                            Surname
                          </label>
                          <input
                            required
                            id="last_name"
                            name="last_name"
                            placeholder="e.g Smith"
                            className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.last_name}
                          />
                        </div>

                        <div className="col-span-6 sm:col-span-6 lg:col-span-6">
                          <label
                            htmlFor="email"
                            className="block text-sm font-medium leading-5 text-gray-700"
                          >
                            Email Address
                          </label>
                          <input
                            required
                            id="email"
                            type="email"
                            name="email"
                            placeholder="e.g janesmith@gmail.com"
                            className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.email}
                          />
                        </div>

                        {showSubmitError1 && (
                          <div className="col-span-6 sm:col-span-6 lg:col-span-6">
                            <Error message="The user is already a member of your team" />
                          </div>
                        )}

                        {showSubmitError2 && (
                          <div className="col-span-6 sm:col-span-6 lg:col-span-6">
                            <Error message="The user with the above email already exists but is not a member of your hospital, multi-hospital membership feature is being developed at the moment." />
                          </div>
                        )}
                      </div>
                    </div>
                    <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                      <MediumButton
                        type="submit"
                        title={isSubmitting ? "Processing..." : "Send Invite"}
                        extraclassName={
                          isSubmitting ? "opacity-50 cursor-not-allowed" : ""
                        }
                      />
                    </div>
                  </div>
                </form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default connect()(NewPatientInviteRegForm);
