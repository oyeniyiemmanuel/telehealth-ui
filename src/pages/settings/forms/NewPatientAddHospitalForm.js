import React, { Fragment } from "react";
import { connect } from "react-redux";
import HospitalsDropdownForPatientToChangeDefaultHospital from "../widgets/HospitalsDropdownForPatientToChangeDefaultHospital";

const NewPatientAddHospitalForm = (props) => {
  return (
    <Fragment>
      <div>
        <div className="md:grid md:grid-cols-4 md:gap-6">
          <div className="md:col-span-2">
            <div className="px-4 sm:px-0">
              <h3 className="text-lg font-medium leading-6 text-gray-900">
                Add New Hospital
              </h3>
              <p className="mt-1 text-sm leading-5 text-gray-600">
                This allows you to increase your range of favorite doctors
                accros different hospitals
              </p>
            </div>
          </div>
          <div className="mt-5 md:mt-0 md:col-span-2">
            <HospitalsDropdownForPatientToChangeDefaultHospital />
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default connect()(NewPatientAddHospitalForm);
