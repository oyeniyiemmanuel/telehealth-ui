import React, { Fragment } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import { CONFIG } from "../../../app.config";
import generalDash from "../../../hoc/GeneralDashboard";
import { MediumButton } from "../../../components/widgets/Buttons";
import { useAlert } from "react-alert";
import "react-step-progress-bar/styles.css";
import { ProgressBar } from "react-step-progress-bar";

const CompleteDoctorProfileForm = (props) => {
  const { user } = props;
  const alert = useAlert();

  const DoctorUpdateFormSchema = Yup.object().shape({
    last_name: Yup.string().required("Please Enter Surname"),
    phone: Yup.string().required("Please Enter Phone"),
    password: Yup.string().required("Please Enter password"),
    password_confirmation: Yup.string().oneOf(
      [Yup.ref("password"), null],
      "Passwords must match"
    ),
  });

  const initialValues = {
    id: user.id,
    last_name: user.last_name,
    phone: user.phone,
    first_name: user.first_name,
    email: user.email,
    password: "",
    password_confirmation: "",
  };

  // const handlespecialtyChange = (selectedOption, values) => {
  //   values.specialty = selectedOption.value;
  // };

  // mutate specialties array to have value and label for react-select dropdown
  // if (specialties.length > 0) {
  //   specialties.map((specialty) => {
  //     specialty.value = specialty.id;
  //     specialty.label = specialty.name;
  //     return specialty;
  //   });
  // }

  return (
    <Fragment>
      <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
        <h1 className="text-2xl font-semibold text-gray-900 border-b border-gray-300 mb-4">
          Complete Your Profile
        </h1>
      </div>

      <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
        <div>
          <div className="md:grid md:grid-cols-3 md:gap-6">
            <div className="md:col-span-1">
              <div className="px-4 sm:px-0">
                <h3 className="text-lg font-medium leading-6 text-gray-900">
                  Personal Details{" "}
                  {user.phone === null ? (
                    <Fragment>
                      <span className="ml-4 text-xs">50% complete</span>
                      <ProgressBar
                        filledBackground="linear-gradient(to right, #f7fafc, #34a853)"
                        percent={50}
                      />
                    </Fragment>
                  ) : (
                    ""
                  )}
                </h3>
                <p className="mt-1 text-sm leading-5 text-gray-600">
                  You are verified as a{" "}
                  {user.specialty ? (
                    <span className="inline-flex items-center px-2.5 py-0.5 rounded-md text-sm font-medium leading-5 bg-indigo-100 text-indigo-800">
                      <svg
                        className="-ml-0.5 mr-1.5 h-2 w-2 text-indigo-400"
                        fill="currentColor"
                        viewBox="0 0 8 8"
                      >
                        <circle cx="4" cy="4" r="3" />
                      </svg>
                      {user.specialty.name}
                    </span>
                  ) : (
                    <span className="ml-1 text-xs italic text-red-900">
                      (None Yet Selected)
                    </span>
                  )}
                </p>
              </div>
            </div>
            <div className="mt-5 md:mt-0 md:col-span-2">
              {user.hospital && (
                <Formik
                  initialValues={initialValues}
                  onSubmit={(
                    values,
                    { initialValues, resetForm, setSubmitting }
                  ) => {
                    setSubmitting(true);
                    // reset form values
                    // resetForm(initialValues);

                    // send request to the server
                    axios
                      .post(CONFIG.SERVER_API + "doctor/edit", {
                        id: values.id,
                        last_name: values.last_name,
                        phone: values.phone,
                        first_name: values.first_name,
                        email: values.email,
                        specialty: values.specialty,
                        password: values.password,
                        password_confirmation: values.password_confirmation,
                      })
                      .then(
                        (res) => {
                          console.log(res.data.user[0]);
                          // Notification of success
                          let alertOptions = {
                            type: "success",
                          };
                          alert.show(
                            "Account Updated and Verified",
                            alertOptions
                          );
                          setSubmitting(false);

                          // update user in redux store
                          props.userFetched(res.data.user[0]);

                          props.history.push("/doctor/select-patients");
                        },
                        (err) => {
                          console.log(err);
                        }
                      );
                  }}
                  validationSchema={DoctorUpdateFormSchema}
                >
                  {({
                    values,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    touched,
                    errors,
                  }) => (
                    <form onSubmit={handleSubmit}>
                      <div className="shadow sm:rounded-md sm:overflow-hidden">
                        <div className=" bg-white sm:p-6">
                          <div className="grid grid-cols-6 gap-6 mt-6">
                            <div className="col-span-6 sm:col-span-3 lg:col-span-3">
                              <label
                                htmlFor="last_name"
                                className="block text-sm font-medium leading-5 text-gray-700"
                              >
                                Surname
                              </label>
                              <input
                                required
                                id="last_name"
                                name="last_name"
                                placeholder="e.g Smith"
                                className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.last_name}
                              />
                            </div>

                            <div className="col-span-6 sm:col-span-3 lg:col-span-3">
                              <label
                                htmlFor="first_name"
                                className="block text-sm font-medium leading-5 text-gray-700"
                              >
                                First Name
                              </label>
                              <input
                                required
                                id="first_name"
                                name="first_name"
                                placeholder="e.g Jane"
                                className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.first_name}
                              />
                            </div>
                          </div>

                          <div className="grid grid-cols-6 gap-6 mt-6">
                            <div className="col-span-6 sm:col-span-3 lg:col-span-3">
                              <label
                                htmlFor="phone"
                                className="block text-sm font-medium leading-5 text-gray-700"
                              >
                                Phone Number
                              </label>
                              <input
                                required
                                id="phone"
                                name="phone"
                                placeholder="e.g 08012345678"
                                className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.phone}
                              />
                            </div>

                            <div className="col-span-6 sm:col-span-3 lg:col-span-3">
                              <label
                                htmlFor="email"
                                className="block text-sm font-medium leading-5 text-gray-700"
                              >
                                Email
                              </label>
                              <input
                                required
                                disabled
                                id="email"
                                name="email"
                                type="email"
                                placeholder="e.g janesmith@gmail.com"
                                className="mt-1 form-input block w-full py-2 px-3 text-gray-500 italic opacity-50 cursor-not-allowed border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.email}
                              />
                            </div>
                          </div>

                          <div className="grid grid-cols-6 gap-6 mt-6">
                            <div className="col-span-6 sm:col-span-3 lg:col-span-3">
                              <div className="mt-2">
                                <div className="flex justify-between">
                                  <label
                                    htmlFor="password"
                                    className="block text-sm font-medium leading-5 text-gray-700"
                                  >
                                    password
                                  </label>
                                </div>
                                <div className="mt-1 relative rounded-md shadow-sm">
                                  <input
                                    id="password"
                                    name="password"
                                    type="password"
                                    placeholder="enter password"
                                    required
                                    className={
                                      touched.password && errors.password
                                        ? "form-input block w-full sm:text-sm sm:leading-5 relative block w-full px-3 py-2 border border-red-300 placeholder-red-500 text-red-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                        : "form-input block w-full sm:text-sm sm:leading-5 relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                    }
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.password}
                                  />
                                </div>
                              </div>
                            </div>

                            <div className="col-span-6 sm:col-span-3 lg:col-span-3">
                              <div className="mt-2">
                                <div className="flex justify-between">
                                  <label
                                    htmlFor="password_confirmation"
                                    className="block text-sm font-medium leading-5 text-gray-700"
                                  >
                                    Password Confirmation
                                  </label>
                                </div>
                                <div className="mt-1 relative rounded-md shadow-sm">
                                  <input
                                    id="password_confirmation"
                                    name="password_confirmation"
                                    type="password"
                                    placeholder="enter password again"
                                    required
                                    className={
                                      touched.password_confirmation &&
                                      errors.password_confirmation
                                        ? "form-input block w-full sm:text-sm sm:leading-5 relative block w-full px-3 py-2 border border-red-300 placeholder-red-500 text-red-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                        : "form-input block w-full sm:text-sm sm:leading-5 relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                    }
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.password_confirmation}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                          <MediumButton
                            type="submit"
                            title={isSubmitting ? "Processing..." : "Submit"}
                            extraClass={
                              isSubmitting
                                ? "opacity-50 cursor-not-allowed"
                                : ""
                            }
                          />
                        </div>
                      </div>
                    </form>
                  )}
                </Formik>
              )}
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default generalDash(CompleteDoctorProfileForm);
