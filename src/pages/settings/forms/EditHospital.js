import React, { Fragment, useState } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import { CONFIG } from "../../../app.config";
import generalDash from "../../../hoc/GeneralDashboard";
import { MediumButton } from "../../../components/widgets/Buttons";
import { useAlert } from "react-alert";
import "react-step-progress-bar/styles.css";
import UploadImage from "../../../components/widgets/UploadImage";
import HospitalTabWrapper from "../wrappers/HospitalTabWrapper";

const EditHospital = (props) => {
  const { user } = props;
  const alert = useAlert();

  const EditHospitalSchema = Yup.object().shape({
    name: Yup.string().required("Please Enter Hospital Name"),
    email: Yup.string().required("Please Enter Hospital Email"),
  });

  return (
    <Fragment>
      <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
        <h1 className="text-2xl font-semibold text-gray-900 border-b border-gray-300 mb-4">
          Settings
        </h1>
      </div>

      <HospitalTabWrapper {...props}>
        <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
          <div>
            <div className="md:grid md:grid-cols-3 md:gap-6">
              {user.hospital && (
                <Fragment>
                  <div className="md:col-span-1">
                    <div className="px-4 sm:px-0 pt-4">
                      <h3 className="text-lg font-medium leading-6 text-gray-900">
                        Edit Hospital Details
                        {/* {user.hospital.website === null ? (
                        <Fragment>
                          <span className="ml-4 text-xs">50% complete</span>
                          <ProgressBar
                            filledBackground="linear-gradient(to right, #f7fafc, #34a853)"
                            percent={50}
                          />
                        </Fragment>
                      ) : (
                        ""
                      )} */}
                      </h3>
                      <p className="mt-4 text-sm leading-5 text-gray-600">
                        This information will be displayed publicly.
                      </p>
                    </div>
                  </div>
                  <div className="mt-5 md:mt-0 md:col-span-2">
                    <Formik
                      initialValues={{
                        id: user.hospital.id,
                        name: user.hospital.name,
                        email: user.hospital.email,
                        website: user.hospital.website,
                        street: user.hospital.street,
                        city: user.hospital.city,
                        state: user.hospital.state,
                        bank_account: user.hospital.bank_account,
                      }}
                      onSubmit={(values, { setSubmitting }) => {
                        setSubmitting(true);

                        axios
                          .post(CONFIG.SERVER_API + "hospital/edit", {
                            id: values.id,
                            name: values.name,
                            email: values.email,
                            website: values.website,
                            street: values.street,
                            city: values.city,
                            state: values.state,
                            bank_account: values.bank_account,
                          })
                          .then(
                            (res) => {
                              console.log(res.data);

                              // Notification of success
                              let alertOptions = {
                                type: "success",
                              };
                              alert.show(
                                "Hospital Profle Updated",
                                alertOptions
                              );
                              setSubmitting(false);

                              // Redirect
                              props.history.push("/settings");
                            },
                            (err) => {
                              console.log(err);
                            }
                          );
                      }}
                      validationSchema={EditHospitalSchema}
                    >
                      {({
                        values,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                      }) => (
                        <form onSubmit={handleSubmit}>
                          <div className="shadow sm:rounded-md sm:overflow-hidden">
                            <div className="bg-white px-6 py-2">
                              <div className="text-center">
                                <label className="block text-sm leading-5 font-medium text-gray-700">
                                  Logo
                                </label>
                                <div className="mt-2 flex justify-center">
                                  <span className="inline-block h-24 w-24 rounded-full overflow-hidden bg-gray-100">
                                    {user.hospital &&
                                    user.hospital.pictures &&
                                    user.hospital.pictures.length > 0 ? (
                                      Object.keys(user.hospital.pictures).map(
                                        (each, key) => {
                                          let image =
                                            user.hospital.pictures[each];

                                          if (image.type === "logo") {
                                            return (
                                              <img
                                                className="h-full w-full"
                                                src={image.url}
                                                alt="Medispark Telehealth"
                                              />
                                            );
                                          }
                                        }
                                      )
                                    ) : (
                                      <svg
                                        className="h-full w-full text-gray-300"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth="2"
                                        stroke="currentColor"
                                      >
                                        <path d="M19 21V5a2 2 0 00-2-2H7a2 2 0 00-2 2v16m14 0h2m-2 0h-5m-9 0H3m2 0h5M9 7h1m-1 4h1m4-4h1m-1 4h1m-5 10v-5a1 1 0 011-1h2a1 1 0 011 1v5m-4 0h4"></path>
                                      </svg>
                                    )}
                                  </span>
                                  <span className="ml-5">
                                    <UploadImage
                                      callbackUrl="settings/hospital/edit-profile"
                                      submitUrl="hospital/upload-logo"
                                      pictureType="logo"
                                      {...props}
                                    />
                                  </span>
                                </div>
                              </div>

                              <div className="grid grid-cols-3 gap-6 mt-6">
                                <div className="col-span-3 sm:col-span-2">
                                  <label
                                    htmlFor="name"
                                    className="block text-sm leading-5 font-medium text-gray-700"
                                  >
                                    Full Hospital Name
                                  </label>
                                  <div className="rounded-md shadow-sm">
                                    <input
                                      id="name"
                                      name="name"
                                      required
                                      defaultValue={user.hospital.name}
                                      className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                      value={values.name}
                                    />
                                  </div>
                                </div>
                              </div>

                              <div className="grid grid-cols-3 gap-6 mt-6">
                                <div className="col-span-3 sm:col-span-2">
                                  <label
                                    htmlFor="website"
                                    className="block text-sm font-medium leading-5 text-gray-700"
                                  >
                                    Website
                                  </label>
                                  <div className="mt-1 flex rounded-md shadow-sm">
                                    <span className="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                      http://
                                    </span>
                                    <input
                                      id="website"
                                      name="website"
                                      className="form-input flex-1 block w-full rounded-none rounded-r-md transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                      placeholder="www.example.com"
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                      value={values.website}
                                    />
                                  </div>
                                </div>
                              </div>

                              <div className="grid grid-cols-3 gap-6">
                                <div className="col-span-3 sm:col-span-2 mt-6">
                                  <label
                                    htmlFor="email"
                                    className="block text-sm leading-5 font-medium text-gray-700"
                                  >
                                    Email
                                  </label>
                                  <div className="rounded-md shadow-sm">
                                    <input
                                      id="email"
                                      name="email"
                                      type="email"
                                      required
                                      defaultValue={user.hospital.email}
                                      className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                      value={values.email}
                                    />
                                  </div>
                                </div>
                              </div>

                              <div className="grid grid-cols-6 gap-6 mt-6">
                                <div className="col-span-6">
                                  <label
                                    htmlFor="street"
                                    className="block text-sm font-medium leading-5 text-gray-700"
                                  >
                                    Street address
                                  </label>
                                  <input
                                    id="street"
                                    name="street"
                                    placeholder="e.g. 10, James Adamu Street"
                                    className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.street}
                                  />
                                </div>

                                <div className="col-span-6 sm:col-span-6 lg:col-span-3">
                                  <label
                                    htmlFor="city"
                                    className="block text-sm font-medium leading-5 text-gray-700"
                                  >
                                    City
                                  </label>
                                  <input
                                    id="city"
                                    name="city"
                                    placeholder="e.g. Ikeja"
                                    className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.city}
                                  />
                                </div>

                                <div className="col-span-6 sm:col-span-3 lg:col-span-3">
                                  <label
                                    htmlFor="state"
                                    className="block text-sm font-medium leading-5 text-gray-700"
                                  >
                                    State
                                  </label>
                                  <input
                                    id="state"
                                    name="state"
                                    placeholder="e.g. Lagos"
                                    className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.state}
                                  />
                                </div>

                                <div className="col-span-6">
                                  <label
                                    htmlFor="bank_account"
                                    className="block text-sm font-medium leading-5 text-gray-700"
                                  >
                                    Bank Account Details
                                  </label>
                                  <input
                                    id="bank_account"
                                    name="bank_account"
                                    placeholder="e.g. ZENITH 0123456789 Abc Hospital Limited"
                                    className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.bank_account}
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                              <span className="inline-flex rounded-md shadow-sm">
                                <MediumButton
                                  type="submit"
                                  title={isSubmitting ? "Saving..." : "Save"}
                                />
                              </span>
                            </div>
                          </div>
                        </form>
                      )}
                    </Formik>
                  </div>
                </Fragment>
              )}
            </div>
          </div>
        </div>
      </HospitalTabWrapper>
    </Fragment>
  );
};

export default generalDash(EditHospital);
