import React from "react";
import { Route, Switch } from "react-router-dom";
import NotFound from "../../components/widgets/NotFound";
import RegisterFormStep1 from "./forms/RegisterFormStep1";
import RegisterFormStep2 from "./forms/RegisterFormStep2";
// import RegisterFormStep3 from "./forms/RegisterFormStep3_old";
// import RegisterFormStep3 from "./forms/RegisterFormStep3";
import RegisterFormStep4NewHospital from "./forms/RegisterFormStep4NewHospital";
const Register = ({ match }) => {
  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${match.path}/step-1`}
          component={RegisterFormStep1}
        />
        <Route
          exact
          path={`${match.path}/step-2`}
          component={RegisterFormStep2}
        />

        <Route
          exact
          path={`${match.path}/step-3`}
          component={RegisterFormStep4NewHospital}
        />
        <Route component={NotFound} />
      </Switch>
    </div>
  );
};

export default Register;
