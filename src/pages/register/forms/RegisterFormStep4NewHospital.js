import React, { Component, Fragment } from "react";
import axios from "axios";
import { Formik } from "formik";
import { connect } from "react-redux";
import * as Yup from "yup";
import Error from "../../../components/widgets/error";
import { CONFIG } from "../../../app.config";
import asset2_bg_img from "../../../assets/img/asset2.jpg";
import { getLoggedInUser } from "../../../accessors/user";
import { userFetched } from "../../../redux/actions";
import { MediumButton } from "../../../components/widgets/Buttons";
import { SmallFeedbackMessage } from "../../../components/widgets/Feedbacks";
import Navbar from "../../../components/Navbar";
import "react-step-progress-bar/styles.css";
import { ProgressBar, Step } from "react-step-progress-bar";
import { getAllHospitals } from "../../../accessors/hospital";

const RegisterHospitalSchema = Yup.object().shape({
  name: Yup.string().required("Please Enter Hospital Name"),
  email: Yup.string().required("Please Enter Hospital Email"),
});

class RegisterFormStep4NewHospital extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showRegisterAttemptError: false,
      showRegisterServerError: false,
      hospitalsData: {},
      selectedHospitalId: "",
    };

    let token = localStorage.getItem("token");

    // get the current user details
    getLoggedInUser(token).then((res) => {
      // trigger redux action to add user data to store
      this.props.userFetched(res);
    });

    // get all hospitals data
    getAllHospitals().then((res) => {
      const result = res.map((hospital) => {
        hospital.value = hospital.id;
        hospital.label = hospital.name;
        return hospital;
      });
      this.setState({ hospitalsData: result });
    });

    this.handleHospitalChange = this.handleHospitalChange.bind(this);
  }

  handleHospitalChange(e) {
    this.setState({ selectedHospitalId: e.target.value });
  }

  render() {
    const {
      showRegisterAttemptError,
      showRegisterServerError,
      selectedHospitalId,
    } = this.state;
    const user = this.props.user;
    return (
      <Fragment>
        {/* Navbar Component */}
        <Navbar />

        {/* other part of the page */}
        <div className="min-h-screen flex">
          <div className="flex-1 flex flex-col py-12 w-1/3 px-2 sm:px-3 lg:flex-none lg:px-10 xl:px-12">
            {user && (
              <Formik
                validationSchema={RegisterHospitalSchema}
                initialValues={{
                  hospital: selectedHospitalId,
                }}
                onSubmit={(values, { setSubmitting }) => {
                  setSubmitting(true);

                  // set boolean not to show error messages
                  this.setState({
                    showRegisterAttemptError: false,
                    showRegisterServerError: false,
                  });

                  // make a Register post request
                  axios
                    .post(CONFIG.SERVER_API + "hospital/new", {
                      name: values.name,
                      email: values.email,
                      user_id: user.id,
                    })
                    .then(
                      (res) => {
                        console.log(res.data);
                        // redirect user to next step
                        this.props.history.push(
                          "/settings/hospital/edit-profile"
                        );
                      },
                      (err) => {
                        console.log(err);
                        this.setState({ showRegisterAttemptError: true });
                        setSubmitting(false);
                      }
                    );
                }}
              >
                {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                }) => (
                  <form onSubmit={handleSubmit}>
                    <div className="mx-auto w-full max-w-sm">
                      <ProgressBar
                        percent={100}
                        filledBackground="linear-gradient(to right, #f7fafc, #33476a)"
                      >
                        <Step>
                          {({ accomplished, index }) => (
                            <div
                              className={`indexedStep bg-gray-400 p-2 rounded-full text-white ${
                                accomplished ? "accomplished" : null
                              }`}
                            >
                              {index + 1}
                            </div>
                          )}
                        </Step>
                        <Step>
                          {({ accomplished, index }) => (
                            <div
                              className={`indexedStep bg-gray-400 p-2 rounded-full text-white ${
                                accomplished ? "accomplished" : null
                              }`}
                            >
                              {index + 1}
                            </div>
                          )}
                        </Step>
                        <Step>
                          {({ accomplished, index }) => (
                            <div
                              className={`indexedStep bg-gray-400 p-2 rounded-full text-white ${
                                accomplished ? "accomplished" : null
                              }`}
                            >
                              {index + 1}
                            </div>
                          )}
                        </Step>
                      </ProgressBar>
                      <div>
                        <h2 className="mt-6 text-center text-2xl leading-9 font-medium text-gray-900">
                          Create Hospital
                        </h2>
                      </div>

                      {/* <SmallFeedbackMessage
                        message="If your hospital has already been created "
                        link={
                          <NavLink
                            to="/register/step-3"
                            className="pl-2 font-medium underline text-indigo-700 hover:text-indigo-600 transition ease-in-out duration-150"
                          >
                            Select from list
                          </NavLink>
                        }
                      /> */}

                      <div className="mt-2">
                        <SmallFeedbackMessage message="Please ensure your hospital details are entered correctly. The MediSpark verification team will contact the hospital to confirm that you have been officially authorised to create an account on behalf of the hospital management" />
                      </div>

                      <div className="mt-4">
                        <label
                          htmlFor="name"
                          className="block text-sm font-medium leading-5 text-gray-700"
                        >
                          Full Name of Hospital
                        </label>
                        <div className="mt-1 rounded-md shadow-sm">
                          <input
                            id="name"
                            type="name"
                            required
                            className={
                              touched.name && errors.name
                                ? "appearance-none rounded-none relative block w-full px-3 py-2 border border-red-300 placeholder-red-500 text-red-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                : "appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                            }
                            placeholder="e.g. ABC Hospital Limited"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.name}
                          />
                        </div>
                      </div>

                      <div className="mt-6">
                        <label
                          htmlFor="email"
                          className="block text-sm font-medium leading-5 text-gray-700"
                        >
                          Official Hospital Email
                        </label>
                        <div className="mt-1 rounded-md shadow-sm">
                          <input
                            id="email"
                            type="email"
                            required
                            className={
                              touched.email && errors.email
                                ? "appearance-none rounded-none relative block w-full px-3 py-2 border border-red-300 placeholder-red-500 text-red-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                                : "appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                            }
                            placeholder="e.g. info@abc.com"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.email}
                          />
                        </div>
                      </div>

                      {showRegisterAttemptError && (
                        <Error message="Email has been used by another hospital" />
                      )}

                      {showRegisterServerError && (
                        <Error message="Issues Connecting with the api server" />
                      )}

                      <div className="mt-6">
                        <MediumButton
                          type="submit"
                          title={
                            isSubmitting
                              ? "Processing..."
                              : "Create Hospital Profile"
                          }
                          extraClass={
                            isSubmitting ? "opacity-50 cursor-not-allowed" : ""
                          }
                        />
                      </div>
                    </div>
                  </form>
                )}
              </Formik>
            )}
          </div>
          <div className="hidden lg:block relative flex-1">
            <img
              className="absolute inset-0 h-full w-full object-cover"
              src={asset2_bg_img}
              alt=""
            />
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.userAuth.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userFetched: (user) => dispatch(userFetched(user)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterFormStep4NewHospital);
