import React, { Component, Fragment } from "react";
import axios from "axios";
import { Formik } from "formik";
import { connect } from "react-redux";
import Error from "../../../components/widgets/error";
import { CONFIG } from "../../../app.config";
import { ROLES } from "../../../assets/json/list";
import { getLoggedInUser } from "../../../accessors/user";
import { userFetched } from "../../../redux/actions";
import { MediumButton } from "../../../components/widgets/Buttons";
import Navbar from "../../../components/Navbar";
import "react-step-progress-bar/styles.css";
import { ProgressBar, Step } from "react-step-progress-bar";

class RegisterFormStep3 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showRegisterAttemptError: false,
      showRegisterServerError: false,
      rolesData: ROLES.options,
      selectedRole: "",
    };

    console.log(ROLES.options);

    let token = localStorage.getItem("token");

    // get the current user details
    getLoggedInUser(token).then((res) => {
      // trigger redux action to add user data to store
      this.props.userFetched(res);
    });

    this.handleRoleChange = this.handleRoleChange.bind(this);
  }

  handleRoleChange(e) {
    this.setState({ selectedRole: e.target.value });
  }

  render() {
    const {
      showRegisterAttemptError,
      showRegisterServerError,
      rolesData,
      selectedRole,
    } = this.state;
    const user = this.props.user;
    return (
      <Fragment>
        {/* Navbar Component */}
        <Navbar />

        {/* other part of the page */}
        <div className="min-h-screen flex">
          <div className="flex-1 flex flex-col py-12 w-1/3 px-2 sm:px-3 lg:flex-none lg:px-10 xl:px-12">
            {user && (
              <Formik
                initialValues={{
                  role: selectedRole,
                }}
                onSubmit={(values, { setSubmitting }) => {
                  setSubmitting(true);

                  // set boolean not to show error messages
                  this.setState({
                    showRegisterAttemptError: false,
                    showRegisterServerError: false,
                  });

                  // make a Register post request
                  axios
                    .post(CONFIG.SERVER_API + "register/step-3", {
                      role: selectedRole,
                      user_id: user.id,
                    })
                    .then(
                      (res) => {
                        console.log(res.data);
                        // redirect user to next step
                        this.props.history.push("/register/step-4");
                      },
                      (err) => {
                        console.log(err);
                        this.setState({ showRegisterServerError: true });
                        setSubmitting(false);
                      }
                    );
                }}
              >
                {({ values, handleChange, handleSubmit, isSubmitting }) => (
                  <form onSubmit={handleSubmit}>
                    <div className="mx-auto w-full max-w-sm">
                      <ProgressBar
                        percent={75}
                        filledBackground="linear-gradient(to right, #f7fafc, #33476a)"
                      >
                        <Step>
                          {({ accomplished, index }) => (
                            <div
                              className={`indexedStep bg-gray-400 p-2 rounded-full text-white ${
                                accomplished ? "accomplished" : null
                              }`}
                            >
                              {index + 1}
                            </div>
                          )}
                        </Step>
                        <Step>
                          {({ accomplished, index }) => (
                            <div
                              className={`indexedStep bg-gray-400 p-2 rounded-full text-white ${
                                accomplished ? "accomplished" : null
                              }`}
                            >
                              {index + 1}
                            </div>
                          )}
                        </Step>
                        <Step>
                          {({ accomplished, index }) => (
                            <div
                              className={`indexedStep bg-gray-400 p-2 rounded-full text-white ${
                                accomplished ? "accomplished" : null
                              }`}
                            >
                              {index + 1}
                            </div>
                          )}
                        </Step>
                        <Step>
                          {({ accomplished, index }) => (
                            <div
                              className={`indexedStep bg-white shadow-md p-2 rounded-full text-gray-700 ${
                                accomplished ? "accomplished" : null
                              }`}
                            >
                              {index + 1}
                            </div>
                          )}
                        </Step>
                      </ProgressBar>
                      <div>
                        <h2 className="mt-6 text-center text-2xl leading-9 font-medium text-gray-900">
                          Select Role
                        </h2>
                      </div>
                      <div className="mt-8 relative">
                        {rolesData && (
                          <select
                            className="block appearance-none w-full border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                            name="role"
                            required
                            onChange={this.handleRoleChange}
                            //   value={values.role}
                          >
                            <option value="">Select role</option>
                            {rolesData.map((option) => {
                              return (
                                <option value={option.value} key={option.value}>
                                  {option.label}
                                </option>
                              );
                            })}
                          </select>
                        )}

                        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                          <svg
                            className="fill-current h-4 w-4"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 20 20"
                          >
                            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                          </svg>
                        </div>
                      </div>

                      {showRegisterAttemptError && (
                        <Error message="Email has been used" />
                      )}

                      {showRegisterServerError && (
                        <Error message="Issues Connecting with the api server" />
                      )}

                      <div className="mt-6">
                        <MediumButton
                          type="submit"
                          title={isSubmitting ? "Processing..." : "Proceed"}
                          extraClass={
                            isSubmitting ? "opacity-50 cursor-not-allowed" : ""
                          }
                        />
                      </div>
                    </div>
                  </form>
                )}
              </Formik>
            )}
          </div>
          <div className="hidden lg:block relative flex-1">
            <img
              className="absolute inset-0 h-full w-full object-cover"
              src="https://images.unsplash.com/photo-1505904267569-f02eaeb45a4c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1908&q=80"
              alt=""
            />
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.userAuth.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userFetched: (user) => dispatch(userFetched(user)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterFormStep3);
