import React, { Component, Fragment } from "react";
import { NavLink } from "react-router-dom";
import axios from "axios";
import { Formik } from "formik";
import { connect } from "react-redux";
import Error from "../../../components/widgets/error";
import { CONFIG } from "../../../app.config";
import asset2_bg_img from "../../../assets/img/asset2.jpg";
import { getLoggedInUser } from "../../../accessors/user";
import { userFetched } from "../../../redux/actions";
import { MediumButton } from "../../../components/widgets/Buttons";
import { SmallFeedbackMessage } from "../../../components/widgets/Feedbacks";
import Navbar from "../../../components/Navbar";
import "react-step-progress-bar/styles.css";
import { ProgressBar, Step } from "react-step-progress-bar";
import { getAllHospitals } from "../../../accessors/hospital";

class RegisterFormStep3 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showRegisterAttemptError: false,
      showRegisterServerError: false,
      hospitalsData: {},
      selectedHospitalId: "",
    };

    let token = localStorage.getItem("token");

    // get the current user details
    getLoggedInUser(token).then((res) => {
      // trigger redux action to add user data to store
      this.props.userFetched(res);
    });

    // get all hospitals data
    getAllHospitals().then((res) => {
      const result = res.map((hospital) => {
        hospital.value = hospital.id;
        hospital.label = hospital.name;
        return hospital;
      });
      this.setState({ hospitalsData: result });
    });

    this.handleHospitalChange = this.handleHospitalChange.bind(this);
  }

  handleHospitalChange(e) {
    this.setState({ selectedHospitalId: e.target.value });
  }

  render() {
    const {
      showRegisterAttemptError,
      showRegisterServerError,
      hospitalsData,
      selectedHospitalId,
    } = this.state;
    const user = this.props.user;
    return (
      <Fragment>
        {/* Navbar Component */}
        <Navbar />

        {/* other part of the page */}
        <div className="min-h-screen flex">
          <div className="flex-1 flex flex-col py-12 w-1/3 px-2 sm:px-3 lg:flex-none lg:px-10 xl:px-12">
            {user && (
              <Formik
                initialValues={{
                  hospital: selectedHospitalId,
                }}
                onSubmit={(values, { setSubmitting }) => {
                  setSubmitting(true);

                  // set boolean not to show error messages
                  this.setState({
                    showRegisterAttemptError: false,
                    showRegisterServerError: false,
                  });

                  // make a Register post request
                  axios
                    .post(CONFIG.SERVER_API + "register/step-4", {
                      hospital_id: selectedHospitalId,
                      user_id: user.id,
                    })
                    .then(
                      (res) => {
                        console.log(res.data);
                        // redirect user to next step
                        if (user.role === "doctor") {
                          this.props.history.push(
                            "/settings/doctor/complete-profile"
                          );
                        }
                        if (user.role === "admin") {
                          this.props.history.push(
                            "/settings/hospital/edit-profile"
                          );
                        }
                      },
                      (err) => {
                        console.log(err);
                        this.setState({ showRegisterServerError: true });
                        setSubmitting(false);
                      }
                    );
                }}
              >
                {({ values, handleChange, handleSubmit, isSubmitting }) => (
                  <form onSubmit={handleSubmit}>
                    <div className="mx-auto w-full max-w-sm">
                      <ProgressBar
                        percent={100}
                        filledBackground="linear-gradient(to right, #f7fafc, #33476a)"
                      >
                        <Step>
                          {({ accomplished, index }) => (
                            <div
                              className={`indexedStep bg-gray-400 p-2 rounded-full text-white ${
                                accomplished ? "accomplished" : null
                              }`}
                            >
                              {index + 1}
                            </div>
                          )}
                        </Step>
                        <Step>
                          {({ accomplished, index }) => (
                            <div
                              className={`indexedStep bg-gray-400 p-2 rounded-full text-white ${
                                accomplished ? "accomplished" : null
                              }`}
                            >
                              {index + 1}
                            </div>
                          )}
                        </Step>
                        <Step>
                          {({ accomplished, index }) => (
                            <div
                              className={`indexedStep bg-gray-400 p-2 rounded-full text-white ${
                                accomplished ? "accomplished" : null
                              }`}
                            >
                              {index + 1}
                            </div>
                          )}
                        </Step>
                      </ProgressBar>
                      <div>
                        <h2 className="mt-6 text-center text-2xl leading-9 font-medium text-gray-900">
                          Select Hospital
                        </h2>
                      </div>

                      <SmallFeedbackMessage
                        message="This list only shows hospitals that have registered themselves on Medispark. If you cannot find your Hospital, please notify the hospital or "
                        link={
                          <NavLink
                            to="/register/step-4/new-hospital"
                            className="pl-4 font-medium underline text-indigo-700 hover:text-indigo-600 transition ease-in-out duration-150"
                          >
                            Create it
                          </NavLink>
                        }
                      />
                      <div className="mt-8 relative">
                        <select
                          className="block appearance-none w-full border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                          name="hospital"
                          required
                          onChange={this.handleHospitalChange}
                          //   value={values.hospital}
                        >
                          <option value="">Select hospital</option>
                          {Object.keys(hospitalsData).length !== 0
                            ? hospitalsData.map((hospital) => {
                                return (
                                  <option value={hospital.id} key={hospital.id}>
                                    {hospital.name}
                                  </option>
                                );
                              })
                            : "loading Hospitals"}
                        </select>

                        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                          <svg
                            className="fill-current h-4 w-4"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 20 20"
                          >
                            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                          </svg>
                        </div>
                      </div>

                      {showRegisterAttemptError && (
                        <Error message="Email has been used" />
                      )}

                      {showRegisterServerError && (
                        <Error message="Issues Connecting with the api server" />
                      )}

                      <div className="mt-6">
                        <MediumButton
                          type="submit"
                          title={
                            isSubmitting
                              ? "Processing..."
                              : "Proceed To Profile"
                          }
                          extraClass={
                            isSubmitting ? "opacity-50 cursor-not-allowed" : ""
                          }
                        />
                      </div>
                    </div>
                  </form>
                )}
              </Formik>
            )}
          </div>
          <div className="hidden lg:block relative flex-1">
            <img
              className="absolute inset-0 h-full w-full object-cover"
              src={asset2_bg_img}
              alt=""
            />
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.userAuth.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userFetched: (user) => dispatch(userFetched(user)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterFormStep3);
