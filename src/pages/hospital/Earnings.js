import React, { Fragment } from "react";
import generalDash from "../../hoc/GeneralDashboard";
import { Html5Entities } from "html-entities";
import { PreloaderDots } from "../../components/widgets/Preloader";
import { SmallFeedbackMessage } from "../../components/widgets/Feedbacks";

const Earnings = (props) => {
  const {
    totalCompletedStreams,
    avgStreamsPayment,
    sumPaidStreams,
    topEarningStreams,
    topEarningRoles,
    topEarningDoctors,
  } = props.hospitalEarningsData;
  console.log(props);
  const htmlEntities = new Html5Entities();

  return (
    <Fragment>
      <main
        className="flex-1 relative z-0 overflow-y-auto focus:outline-none"
        tabIndex="0"
      >
        <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
          <h1 className="text-2xl font-semibold text-gray-900">
            Hospital Earnings
          </h1>
        </div>
        <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
          <div className="py-4">
            <div className="border-4 border-dashed border-gray-200 rounded-lg p-6">
              {props.hospitalEarningsData.length === 0 ? (
                <PreloaderDots message="loading earnings info" />
              ) : (
                <Fragment>
                  {/* Summary */}

                  <div>
                    <h3 className="text-lg leading-6 font-medium text-gray-900">
                      Summary
                    </h3>
                    <div className="mt-5 grid grid-cols-1 gap-5 sm:grid-cols-2 lg:grid-cols-3">
                      <div className="bg-white overflow-hidden shadow rounded-lg">
                        <div className="px-4 py-5 sm:p-6">
                          <div className="flex items-center">
                            <div className="flex-shrink-0 bg-indigo-800 rounded-md p-3">
                              <svg
                                className="h-6 w-6 text-white"
                                fill="none"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                viewBox="0 0 24 24"
                                stroke="currentColor"
                              >
                                <path d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01"></path>{" "}
                              </svg>
                            </div>
                            <div className="ml-5 w-0 flex-1">
                              <dl>
                                <dt className="text-sm leading-5 font-medium text-gray-500 truncate">
                                  Total Completed Calls
                                </dt>
                                <dd className="flex items-baseline">
                                  <div className="text-2xl leading-8 font-semibold text-gray-900">
                                    {totalCompletedStreams}
                                  </div>
                                  <div className="ml-2 flex items-baseline text-sm leading-5 font-semibold text-green-600">
                                    <svg
                                      className="self-center flex-shrink-0 h-5 w-5 text-green-500"
                                      fill="currentColor"
                                      viewBox="0 0 20 20"
                                    >
                                      <path
                                        fillRule="evenodd"
                                        d="M5.293 9.707a1 1 0 010-1.414l4-4a1 1 0 011.414 0l4 4a1 1 0 01-1.414 1.414L11 7.414V15a1 1 0 11-2 0V7.414L6.707 9.707a1 1 0 01-1.414 0z"
                                        clipRule="evenodd"
                                      />
                                    </svg>
                                    <span className="sr-only">
                                      Increased by
                                    </span>
                                    {totalCompletedStreams}
                                  </div>
                                </dd>
                              </dl>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="bg-white overflow-hidden shadow rounded-lg">
                        <div className="px-4 py-5 sm:p-6">
                          <div className="flex items-center">
                            <div className="flex-shrink-0 bg-indigo-800 rounded-md p-3">
                              <svg
                                className="h-6 w-6 text-white"
                                fill="none"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                viewBox="0 0 24 24"
                                stroke="currentColor"
                              >
                                <path d="M3 5a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V19a2 2 0 01-2 2h-1C9.716 21 3 14.284 3 6V5z"></path>{" "}
                              </svg>
                            </div>
                            <div className="ml-5 w-0 flex-1">
                              <dl>
                                <dt className="text-sm leading-5 font-medium text-gray-500 truncate">
                                  Avg. Call Payment.
                                </dt>
                                <dd className="flex items-baseline">
                                  <div className="text-2xl leading-8 font-semibold text-gray-900">
                                    {htmlEntities.decode("&#8358")}
                                    {avgStreamsPayment}
                                  </div>
                                  <div className="ml-2 flex items-baseline text-sm leading-5 font-semibold text-green-600">
                                    <svg
                                      className="self-center flex-shrink-0 h-5 w-5 text-green-500"
                                      fill="currentColor"
                                      viewBox="0 0 20 20"
                                    >
                                      <path
                                        fillRule="evenodd"
                                        d="M5.293 9.707a1 1 0 010-1.414l4-4a1 1 0 011.414 0l4 4a1 1 0 01-1.414 1.414L11 7.414V15a1 1 0 11-2 0V7.414L6.707 9.707a1 1 0 01-1.414 0z"
                                        clipRule="evenodd"
                                      />
                                    </svg>
                                    <span className="sr-only">
                                      Increased by
                                    </span>
                                    {htmlEntities.decode("&#8358")}
                                    {avgStreamsPayment}
                                  </div>
                                </dd>
                              </dl>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="bg-white overflow-hidden shadow rounded-lg">
                        <div className="px-4 py-5 sm:p-6">
                          <div className="flex items-center">
                            <div className="flex-shrink-0 bg-indigo-800 rounded-md p-3">
                              <svg
                                className="h-6 w-6 text-white"
                                fill="none"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                viewBox="0 0 24 24"
                                stroke="currentColor"
                              >
                                <path d="M3 10h18M7 15h1m4 0h1m-7 4h12a3 3 0 003-3V8a3 3 0 00-3-3H6a3 3 0 00-3 3v8a3 3 0 003 3z"></path>{" "}
                              </svg>
                            </div>
                            <div className="ml-5 w-0 flex-1">
                              <dl>
                                <dt className="text-sm leading-5 font-medium text-gray-500 truncate">
                                  Total Payment
                                </dt>
                                <dd className="flex items-baseline">
                                  <div className="text-2xl leading-8 font-semibold text-gray-900">
                                    {htmlEntities.decode("&#8358")}
                                    {sumPaidStreams}
                                  </div>
                                  <div className="ml-2 flex items-baseline text-sm leading-5 font-semibold text-red-600">
                                    <svg
                                      className="self-center flex-shrink-0 h-5 w-5 text-red-500"
                                      fill="currentColor"
                                      viewBox="0 0 20 20"
                                    >
                                      <path
                                        fillRule="evenodd"
                                        d="M14.707 10.293a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 111.414-1.414L9 12.586V5a1 1 0 012 0v7.586l2.293-2.293a1 1 0 011.414 0z"
                                        clipRule="evenodd"
                                      />
                                    </svg>
                                    <span className="sr-only">
                                      Decreased by
                                    </span>
                                    {htmlEntities.decode("&#8358")}
                                    {sumPaidStreams}
                                  </div>
                                </dd>
                              </dl>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  {/* to earning roles */}
                  <h3 className="text-lg leading-6 mt-12 mb-6 font-medium text-gray-900">
                    Top Earning Roles
                  </h3>
                  <div className="flex flex-col">
                    <div className="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
                      <div className="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                        <table className="min-w-full">
                          <thead>
                            <tr>
                              <th className="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Role
                              </th>
                              <th className="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Amount
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {topEarningRoles.length > 0 ? (
                              topEarningRoles.map((role) => {
                                return (
                                  <tr className="bg-white">
                                    <td className="px-6 py-4 whitespace-no-wrap text-sm leading-5 font-medium text-gray-900">
                                      {role.name}
                                    </td>
                                    <td className="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                      {htmlEntities.decode("&#8358")}{" "}
                                      {role.total_paid}
                                    </td>
                                  </tr>
                                );
                              })
                            ) : (
                              <tr>
                                <td colSpan="4">
                                  <SmallFeedbackMessage message="No Data" />
                                </td>
                              </tr>
                            )}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>

                  {/* top earning doctors */}
                  <h3 className="text-lg leading-6 mt-12 mb-6 font-medium text-gray-900">
                    Top Earning Doctors
                  </h3>
                  <ul className="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-3">
                    {topEarningDoctors.length > 0 ? (
                      topEarningDoctors.map((doctor, index) => {
                        doctor.serial_no = index + 1;
                        console.log(doctor);
                        return (
                          <li className="col-span-1 bg-white rounded-lg shadow">
                            <div className="w-full flex items-center justify-between p-6 space-x-6">
                              <div className="flex-1 truncate">
                                <div className="flex items-center space-x-3">
                                  <span className="flex-shrink-0 inline-block mr-2 px-2 py-0.5 text-teal-800 text-xs leading-4 font-bold bg-teal-100 rounded-full">
                                    {doctor.serial_no}
                                  </span>
                                  <h3 className="text-gray-900 text-sm leading-5 font-medium truncate">
                                    {doctor.last_name} {doctor.first_name}
                                  </h3>
                                </div>
                                <p className="mt-1 text-gray-500 text-xs leading-5 truncate">
                                  {doctor.specialty}
                                </p>
                              </div>
                              {doctor.img_url ? (
                                <img
                                  key={doctor.user_id}
                                  className="w-10 h-10 bg-gray-300 rounded-full flex-shrink-0"
                                  src={doctor.img_url}
                                  alt="Medispark Telehealth"
                                />
                              ) : (
                                <svg
                                  className="w-10 h-10 rounded-full flex-shrink-0 text-teal-300"
                                  fill="currentColor"
                                  viewBox="0 0 18 18"
                                >
                                  <path
                                    d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
                                    clipRule="evenodd"
                                    fillRule="evenodd"
                                  ></path>
                                </svg>
                              )}
                            </div>
                            <div className="border-t border-gray-200">
                              <div className="-mt-px flex">
                                <div className="w-0 flex-1 flex border-r border-gray-200">
                                  <div className="relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm leading-5 text-gray-700 font-medium border border-transparent rounded-bl-lg hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 transition ease-in-out duration-150">
                                    {htmlEntities.decode("&#8358")}{" "}
                                    <span className="ml-1">
                                      {doctor.total_paid}
                                    </span>
                                  </div>
                                </div>
                                <div className="-ml-px w-0 flex-1 flex">
                                  <div className="relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm leading-5 text-gray-700 font-medium border border-transparent rounded-br-lg hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 transition ease-in-out duration-150">
                                    <svg
                                      className="w-5 h-5 text-green-400"
                                      viewBox="0 0 20 20"
                                      fill="currentColor"
                                    >
                                      <path d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
                                    </svg>
                                    <span className="ml-3">
                                      {doctor.total_calls} Calls
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </li>
                        );
                      })
                    ) : (
                      <li className="col-span-1 bg-white rounded-lg shadow">
                        <SmallFeedbackMessage message="No Data" />
                      </li>
                    )}
                  </ul>
                </Fragment>
              )}
            </div>
          </div>
        </div>
      </main>
    </Fragment>
  );
};

export default generalDash(Earnings);
