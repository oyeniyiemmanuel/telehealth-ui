import React, { Component } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import Header from "../../../components/widgets/Header";
import { MediumButton } from "../../../components/widgets/Buttons";
import { hospitalCreated, userFetched } from "../../../redux/actions";
import axios from "axios";
import Error from "../../../components/widgets/error";
import { CONFIG } from "../../../app.config";
import { getLoggedInUser } from "../../../accessors/user";
import { SmallFeedbackMessage } from "../../../components/widgets/Feedbacks";

const RegisterHospitalSchema = Yup.object().shape({
  name: Yup.string().required("Please Enter Hospital Name"),
  email: Yup.string().required("Please Enter Hospital Email"),
});

class NewHospital extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showSubmitError: false,
    };

    let token = localStorage.getItem("token");

    // get the current user details
    getLoggedInUser(token).then((res) => {
      // trigger redux action to add user data to store
      this.props.userFetched(res);
    });
  }
  render() {
    const { showSubmitError } = this.state;
    const user = this.props.user;
    return (
      <div>
        <Header
          title="Register New Hospital"
          subTitle="You will get a notification in the email entered below"
        />
        <div className="min-h-screen bg-gray-50 flex flex-col pt-6 sm:px-6 lg:px-8">
          <div className="mt-1 sm:mx-auto sm:w-full sm:max-w-md">
            <SmallFeedbackMessage message="If you’re a solo-practitioner, register yourself as a hospital e.g. Dr Ade’s clinic" />

            <div className="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10 mt-2">
              <Formik
                initialValues={{ name: "", email: "" }}
                onSubmit={(values, { setSubmitting }) => {
                  setSubmitting(true);
                  this.setState({ showSubmitError: false });

                  axios
                    .post(CONFIG.SERVER_API + "hospital/new", {
                      name: values.name,
                      email: values.email,
                      user_id: user.id,
                    })
                    .then(
                      (res) => {
                        this.props.storeHospitalData(res.data.hospital);
                        this.props.history.push("/hospital/dashboard");
                      },
                      (err) => {
                        this.setState({ showSubmitError: true });
                        setSubmitting(false);
                        console.log(err);
                      }
                    );
                }}
                validationSchema={RegisterHospitalSchema}
              >
                {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                }) => (
                  <form onSubmit={handleSubmit}>
                    <div>
                      <label
                        htmlFor="name"
                        className="block text-sm font-medium leading-5 text-gray-700"
                      >
                        Full Name of Hospital
                      </label>
                      <div className="mt-1 rounded-md shadow-sm">
                        <input
                          id="name"
                          type="name"
                          required
                          className={
                            touched.name && errors.name
                              ? "appearance-none rounded-none relative block w-full px-3 py-2 border border-red-300 placeholder-red-500 text-red-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                              : "appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                          }
                          placeholder="e.g. ABC Hospital Limited"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.name}
                        />
                      </div>
                    </div>

                    <div className="mt-6">
                      <label
                        htmlFor="email"
                        className="block text-sm font-medium leading-5 text-gray-700"
                      >
                        Official Hospital Email
                      </label>
                      <div className="mt-1 rounded-md shadow-sm">
                        <input
                          id="email"
                          type="email"
                          required
                          className={
                            touched.email && errors.email
                              ? "appearance-none rounded-none relative block w-full px-3 py-2 border border-red-300 placeholder-red-500 text-red-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                              : "appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                          }
                          placeholder="e.g. info@abc.com"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.email}
                        />
                      </div>
                    </div>

                    {showSubmitError && (
                      <Error message="Email has been used by another hospital" />
                    )}

                    <div className="mt-6">
                      <span className="block w-full rounded-md shadow-sm">
                        <MediumButton
                          type="submit"
                          title={isSubmitting ? "Processing..." : "Proceed"}
                          extraClass={
                            isSubmitting ? "opacity-50 cursor-not-allowed" : ""
                          }
                        />
                      </span>
                    </div>
                  </form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.userAuth.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    storeHospitalData: (hospital) => dispatch(hospitalCreated(hospital)),
    userFetched: (user) => dispatch(userFetched(user)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewHospital);
