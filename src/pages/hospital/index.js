/* eslint-disable react/prop-types */
import React from "react";
import { Route, Switch } from "react-router-dom";
import NotFound from "../../components/widgets/NotFound";
import LoginForm from "../../components/auth/LoginForm";
import ContentPane from "./ContentPane";
import Earnings from "./Earnings";

const Hospital = ({ match }) => {
  return (
    <div>
      <Switch>
        <Route exact path={`${match.path}/admin/login`} component={LoginForm} />
        <Route exact path={`${match.path}/earnings`} component={Earnings} />
        <Route exact path={`${match.path}/`} component={ContentPane} />
        <Route component={NotFound} />
      </Switch>
    </div>
  );
};

export default Hospital;
