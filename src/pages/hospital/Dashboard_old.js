import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import SideNav from "../../components/widgets/SideNav";
import ContentPane from "./ContentPane";
import { getLoggedInUser } from "../../accessors/user";
import { userFetched } from "../../redux/actions";
import { PreloaderLines } from "../../components/widgets/Preloader";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    let token = localStorage.getItem("token");

    // get the current user details
    getLoggedInUser(token).then((res) => {
      // trigger redux action to add user data to store
      this.props.userFetched(res);
    });
  }
  render() {
    const user = this.props.user;
    if (user !== undefined && user.hospital_id !== null) {
      return (
        <Fragment>
          <div className="h-screen flex overflow-hidden bg-gray-100">
            <SideNav {...user} />

            <ContentPane {...user} />
          </div>
        </Fragment>
      );
    } else {
      return <PreloaderLines />;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.userAuth.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userFetched: (user) => dispatch(userFetched(user)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
