import React, { Fragment } from "react";
import generalDash from "../../hoc/GeneralDashboard";

const ContentPane = (user) => (
  <Fragment>
    <main
      className="flex-1 relative z-0 overflow-y-auto py-6 focus:outline-none"
      tabIndex="0"
    >
      <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
        <h1 className="text-2xl font-semibold text-gray-900">
          {user && user.name}
        </h1>
      </div>
      <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
        {/* <!-- Replace with your content --> */}
        <div className="py-4">
          <div className="border-4 border-dashed border-gray-200 rounded-lg h-96"></div>
        </div>
        {/* <!-- /End replace --> */}
      </div>
    </main>
  </Fragment>
);

export default generalDash(ContentPane);
