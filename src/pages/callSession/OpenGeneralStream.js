import React, { Fragment, Component } from "react";
import generalDash from "../../hoc/GeneralDashboard";
import TwilioVideoEntry from "./twilio/TwilioVideoEntry";
import { PreloaderDots } from "../../components/widgets/Preloader";
import { getStreamByResourceId } from "../../accessors/stream";

class OpenGeneralStream extends Component {
  constructor(props) {
    super(props);

    let resource_id = this.props.match.params.resource_id;

    // get the current user details
    getStreamByResourceId(resource_id).then((res) => {
      // trigger redux action to add user data to store
      this.props.streamFetched(res);
    });

    this.state = {
      resourceId: resource_id,
    };
  }

  render() {
    const { user, stream } = this.props;
    const { resourceId } = this.state;
    return (
      <Fragment>
        <main
          className="flex-1 relative z-0 overflow-y-auto focus:outline-none"
          tabIndex="0"
        >
          <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
            <span class="inline-flex items-center px-2 py-0.5 rounded text-xs font-medium leading-4 bg-indigo-100 text-indigo-800">
              <svg
                class="mr-3 h-6 w-6 text-indigo-400"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeLidth="2"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z"></path>
                <path d="M15 13a3 3 0 11-6 0 3 3 0 016 0z"></path>
              </svg>
              {resourceId}
            </span>
          </div>
          <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
            <div className="py-4">
              <div className="border-4 border-dashed border-gray-200 rounded-lg p-6">
                {Array.isArray(stream) && stream.length == 0 ? (
                  <PreloaderDots message="Loading your video stream" />
                ) : stream && stream.isPaid == "1" ? (
                  <Fragment>
                    {/* <PreloaderDots message="Loading your video stream" /> */}
                    {resourceId && <TwilioVideoEntry {...this.props} />}
                  </Fragment>
                ) : (
                  <PreloaderDots message="Verifying Stream's Payment Status" />
                )}
              </div>
            </div>
          </div>
        </main>
      </Fragment>
    );
  }
}

export default generalDash(OpenGeneralStream);
