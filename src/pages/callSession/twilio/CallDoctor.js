import React, { Fragment, Component } from "react";
import axios from "axios";
import generalDash from "../../../hoc/GeneralDashboard";
import { getDoctorDetails } from "../../../accessors/doctor";
import { CONFIG } from "../../../app.config";
import { PreloaderDots } from "../../../components/widgets/Preloader";
import TwilioVideoEntry from "./TwilioVideoEntry";
import PaystackButton from "react-paystack";

class CallDoctor extends Component {
  constructor(props) {
    super(props);

    let doctor_id = this.props.match.params.doctor_id;
    let user_id = this.props.user.id;
    let reference_id = this.getReference();
    let resource_id =
      "consultationRoom" + "_" + doctor_id + "_" + user_id + "_" + reference_id;

    console.log(resource_id);

    this.state = {
      // token: token,
      resourceId: resource_id,
      referenceId: reference_id,
      displayName: this.props.user.last_name,
      userData: "",
      // paystack details
      key: CONFIG.PAYSTACK_KEY, //PAYSTACK PUBLIC KEY
      email: "oyeniyiemmanuel@gmail.com", // customer email
      amount: 0,
      specialty_id: "",
    };

    let stream_details = {
      resource_id: this.state.resourceId,
      hospital_id: this.props.user.hospital_id,
      doctor_id: this.props.match.params.doctor_id,
      initiator_id: this.props.user.id,
    };

    this.persistStreamDetails(stream_details);

    // get doctor asynchronously and wait till the promise gets resolved before assigning the variable
    (async () => {
      const result = await getDoctorDetails(this.props.match.params.doctor_id);
      console.log(result);
      // update the state
      this.setState({ amount: result.specialty.prices[0].value * 100 });
      this.setState({ specialty_id: result.specialty.id });
    })();
  }

  // VIDEO RELATED FUNCTIONS
  persistStreamDetails = (streamDetails) => {
    axios
      .post(CONFIG.SERVER_API + "stream/new-from-patient", streamDetails)
      .then(
        (res) => {
          // dispatch action to update stream details in redux
          this.props.streamFetched(res.data.stream);
        },
        (err) => {
          console.log(err);
        }
      );
  };

  // PAYSTACK RELATED FUNCTIONS
  paymentSuccessful = (response) => {
    // load the vidyo view
    // this.loadRemoteVidyoClientLib(this.state.useNativeWebRTC, false);

    // update stream payment details
    if (response.status === "success") {
      const stream_details = {
        stream_id: this.props.stream.id,
        paid_amount: this.state.amount / 100,
        specialty_id: this.state.specialty_id,
        reference_id: this.state.referenceId,
        resource_id: this.state.resourceId,
        hospital_id: this.props.user.hospital_id,
        doctor_id: this.props.match.params.doctor_id,
        initiator_id: this.props.user.id,
      };

      axios.post(CONFIG.SERVER_API + "stream/is-paid", stream_details).then(
        (res) => {
          // dispatch action to update stream details in redux
          this.props.streamFetched(res.data.stream[0]);
        },
        (err) => {
          console.log(err);
        }
      );
    }
  };

  close = () => {
    console.log("Payment closed");
  };

  getReference = () => {
    //you can put any unique reference implementation code here
    let text = "";
    let possible =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-.=";

    for (let i = 0; i < 15; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  };

  refreshPage = () => {
    window.location.reload(true);
  };

  render() {
    const { user, stream } = this.props;
    const { amount, email, key } = this.state;

    return (
      <Fragment>
        <main
          className="flex-1 -mt-6 relative z-0 overflow-y-auto focus:outline-none"
          tabIndex="0"
        >
          <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
            <h1 className="text-2xl font-semibold text-gray-900">
              {user && user.last_name}
            </h1>
          </div>
          <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
            <div className="py-4">
              <div className="border-4 border-dashed border-gray-200 rounded-lg p-6">
                {console.log(stream)}
                {Array.isArray(stream) && stream.length == 0 ? (
                  <PreloaderDots message="Loading your video stream" />
                ) : stream && stream.isPaid == "1" ? (
                  <Fragment>
                    {/* <Fragment>
                      <PreloaderDots message="Loading your video stream" />
                      <div className="bg-white">
                        <div className="max-w-screen-xl mx-auto text-center py-12 px-4 sm:px-6 lg:py-16 lg:px-8">
                          <p className="text-md leading-9 font-bold tracking-tight text-gray-900 sm:text-sm sm:leading-10">
                            You have paid for this consultation .
                            <br />{" "}
                            <span className="text-gray-600">
                              {" "}
                              if your video doesn't open in{" "}
                              <Countdown
                                date={Date.now() + 15000}
                                renderer={(props) => (
                                  <span>{props.seconds} seconds</span>
                                )}
                              />
                            </span>
                          </p>
                          <div className="mt-8 flex justify-center">
                            <div className="inline-flex rounded-md shadow">
                              <button
                                onClick={this.refreshPage}
                                className="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-teal-900 hover:bg-teal-500 focus:outline-none focus:shadow-outline transition duration-150 ease-in-out"
                              >
                                Click here
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Fragment> */}

                    {/* Twilio Video Wrapper */}

                    <TwilioVideoEntry {...this.props} />
                  </Fragment>
                ) : (
                  <Fragment>
                    <div
                      className="bg-white rounded-lg px-4 pt-5 pb-4 m-auto overflow-hidden shadow-xl transform transition-all sm:max-w-sm sm:w-full sm:p-6"
                      role="dialog"
                      aria-modal="true"
                      aria-labelledby="modal-headline"
                    >
                      <div>
                        <div className="mx-auto flex items-center justify-center h-12 w-12 rounded-full bg-green-100">
                          <svg
                            className="h-6 w-6 text-green-600"
                            stroke="currentColor"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                          >
                            <path d="M3 10h18M7 15h1m4 0h1m-7 4h12a3 3 0 003-3V8a3 3 0 00-3-3H6a3 3 0 00-3 3v8a3 3 0 003 3z"></path>
                          </svg>
                        </div>
                        <div className="mt-3 text-center sm:mt-5">
                          <h3
                            className="text-lg leading-6 font-medium text-gray-900"
                            id="modal-headline"
                          >
                            Payment Page
                          </h3>
                          <div className="mt-2">
                            <div className="text-sm leading-5 text-gray-500">
                              <PaystackButton
                                text="Make Payment"
                                className="payButton"
                                callback={this.paymentSuccessful}
                                close={this.close}
                                // disabled={true}
                                embed={true}
                                reference={this.getReference()}
                                email={email}
                                amount={amount}
                                paystackkey={key}
                                tag="button"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Fragment>
                )}
              </div>
            </div>
          </div>
        </main>
      </Fragment>
    );
  }
}

export default generalDash(CallDoctor);
