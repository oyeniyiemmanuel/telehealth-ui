import React, { ChangeEvent, useState, FormEvent, useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import TextField from "@material-ui/core/TextField";
import ToggleFullscreenButton from "./ToggleFullScreenButton/ToggleFullScreenButton";
import Toolbar from "@material-ui/core/Toolbar";
import Menu from "./Menu/Menu";

import axios from "axios";
import { useAlert } from "react-alert";
import { CONFIG } from "../../../../../app.config";

import { useAppState } from "../../state";
import { useParams } from "react-router-dom";
import useRoomState from "../../hooks/useRoomState/useRoomState";
import useVideoContext from "../../hooks/useVideoContext/useVideoContext";
import { Typography } from "@material-ui/core";
import FlipCameraButton from "./FlipCameraButton/FlipCameraButton";
import LocalAudioLevelIndicator from "./DeviceSelector/LocalAudioLevelIndicator/LocalAudioLevelIndicator";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      backgroundColor: theme.palette.background.default,
    },
    toolbar: {
      [theme.breakpoints.down("xs")]: {
        padding: 0,
      },
    },
    rightButtonContainer: {
      display: "flex",
      alignItems: "center",
      marginLeft: "auto",
    },
    form: {
      display: "flex",
      flexWrap: "wrap",
      alignItems: "center",
      [theme.breakpoints.up("md")]: {
        marginLeft: "2.2em",
      },
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      maxWidth: 200,
    },
    loadingSpinner: {
      marginLeft: "1em",
    },
    displayName: {
      margin: "1.1em 0.6em",
      minWidth: "200px",
      fontWeight: 600,
    },
    joinButton: {
      margin: "1em",
      backgroundColor: "#16bdca",
      textTransform: "capitalize",
      "&:hover": {
        backgroundColor: "#05505c",
      },
    },
  })
);

export default function MenuBar(props: any) {
  console.log(props);
  const classes = useStyles();
  const alert = useAlert();

  const { URLRoomName } = useParams();
  const { user, getToken, isFetching } = useAppState();
  const { isConnecting, connect, isAcquiringLocalTracks } = useVideoContext();
  const roomState = useRoomState();

  const [name, setName] = useState<string>(
    user?.displayName || props.user.last_name
  );
  const [roomName, setRoomName] = useState<string>(
    props.stream.vidyo_resource_id
  );

  const [streamId, setStreamId] = useState(props.stream.id);
  console.log(streamId);

  useEffect(() => {
    if (URLRoomName) {
      setRoomName(URLRoomName);
    }
  }, [URLRoomName]);

  const handleNameChange = (event: ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  };

  const handleRoomNameChange = (event: ChangeEvent<HTMLInputElement>) => {
    setRoomName(event.target.value);
  };

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    // If this app is deployed as a twilio function, don't change the URL because routing isn't supported.
    if (!window.location.origin.includes("twil.io")) {
      window.history.replaceState(
        null,
        "",
        window.encodeURI(`/room/${roomName}${window.location.search || ""}`)
      );
    }
    getToken(name, roomName).then((token) => connect(token));
  };

  const markStreamAsCompleted = () => {
    axios
      .post(CONFIG.SERVER_API + "stream/mark-as-completed", {
        stream_id: streamId,
      })
      .then(
        (res) => {
          console.log(res.data);
          // Redirect
          if (props.user.doctor_id) {
            props.history.push("/doctor/select-patients");
          } else {
            props.history.push("/patient/select-doctors");
          }
        },
        (err) => {
          console.log(err);
        }
      );
  };
  // useEffect(() => {
  //   handleSubmit();
  // }, []);

  const hidden = {
    display: "none",
  };

  return (
    <AppBar className={classes.container} position="static">
      <Toolbar className={classes.toolbar}>
        {roomState === "disconnected" ? (
          <form
            className={`${classes.form}`}
            id="connection_form"
            onSubmit={handleSubmit}
          >
            {window.location.search.includes("customIdentity=true") ||
            !user?.displayName ? (
              <TextField
                id="menu-name"
                label="Name"
                className={classes.textField}
                value={name}
                onChange={handleNameChange}
                margin="dense"
                style={hidden}
              />
            ) : (
              <Typography className={classes.displayName} variant="body1">
                {user.displayName}
              </Typography>
            )}
            <TextField
              id="menu-room"
              label="Room"
              className={classes.textField}
              value={roomName}
              onChange={handleRoomNameChange}
              margin="dense"
              style={hidden}
            />
            <Button
              className={classes.joinButton}
              type="submit"
              color="primary"
              variant="contained"
              disabled={
                isAcquiringLocalTracks ||
                isConnecting ||
                !name ||
                !roomName ||
                isFetching
              }
            >
              Join Room
            </Button>
            {(isConnecting || isFetching) && (
              <CircularProgress className={classes.loadingSpinner} />
            )}
          </form>
        ) : (
          <h3>Connected</h3>
        )}

        {/* show button to mark stream as completed */}

        <button
          onClick={markStreamAsCompleted}
          className="inline-flex items-center ml-2 px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-white bg-red-600 hover:bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-red active:bg-red-700 transition ease-in-out duration-150"
        >
          {/* <svg
            className="-ml-0.5 mr-2 h-4 w-4"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
            <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
          </svg> */}
          Mark as completed
        </button>

        <div className={classes.rightButtonContainer}>
          <FlipCameraButton />
          <LocalAudioLevelIndicator />
          {/* <ToggleFullscreenButton /> */}
          {/* <Menu /> */}
        </div>
      </Toolbar>
    </AppBar>
  );
}
