import React from "react";
import { CssBaseline } from "@material-ui/core";
import { MuiThemeProvider } from "@material-ui/core/styles";
import App from "./App";
import AppStateProvider, { useAppState } from "./state";
import ErrorDialog from "./components/ErrorDialog/ErrorDialog";
import generateConnectionOptions from "./utils/generateConnectionOptions/generateConnectionOptions";
import theme from "./theme";
import "./types";
import { VideoProvider } from "./components/VideoProvider";

const VideoApp = (props) => {
  const { error, setError, settings } = useAppState();
  const connectionOptions = generateConnectionOptions(settings);

  return (
    <VideoProvider options={connectionOptions} onError={setError}>
      <ErrorDialog dismissError={() => setError(null)} error={error} />
      <App {...props} />
    </VideoProvider>
  );
};

const TwilioVideoEntry = (props) => {
  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      <AppStateProvider>
        <VideoApp {...props} />
      </AppStateProvider>
    </MuiThemeProvider>
  );
};

export default TwilioVideoEntry;
