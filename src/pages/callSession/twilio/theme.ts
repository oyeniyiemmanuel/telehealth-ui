import { createMuiTheme } from "@material-ui/core";

declare module "@material-ui/core/styles/createMuiTheme" {
  interface Theme {
    sidebarWidth: number;
    sidebarMobileHeight: number;
  }

  // allow configuration using `createMuiTheme`
  interface ThemeOptions {
    sidebarWidth?: number;
    sidebarMobileHeight?: number;
  }
}

export default createMuiTheme({
  overrides: {
    MuiCssBaseline: {
      "@global": {
        body: {
          fontFamily: "Inter var",
          fontSize: "1em",
          letterSpacing: "normal",
        },
      },
    },
    MuiInputBase: {
      root: {
        fontFamily: "inter var",
      },
    },
    MuiFormLabel: {
      root: {
        fontFamily: "inter var",
      },
    },
    MuiButton: {
      root: {
        fontFamily: "inter var",
      },
    },
  },
  palette: {
    type: "dark",
    primary: {
      main: "#F22F46",
    },
  },
  sidebarWidth: 260,
  sidebarMobileHeight: 90,
});
