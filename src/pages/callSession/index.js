import React, { Component } from "react";
import VideoContainer from "./VideoContainer";
import { connect } from "react-redux";

class CallSession extends Component {
  render() {
    return (
      <div>
        <div>
          <div className="bg-gray-800 pb-32">
            <header className="py-10">
              <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <h1 className="text-3xl leading-9 font-bold text-white">
                  Telehealth
                </h1>
              </div>
            </header>
          </div>
          <main className="-mt-32">
            <div className="max-w-7xl mx-auto pb-12 px-4 sm:px-6 lg:px-8">
              <div className="bg-white rounded-lg shadow px-5 py-6 sm:px-6">
                <div className="">
                  <button
                    type="button"
                    className="inline-flex items-center px-6 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-gray-600 hover:bg-gray-500 focus:outline-none focus:border-gray-700 focus:shadow-outline-gray active:bg-gray-700 transition ease-in-out duration-150"
                  >
                    join a Session {this.props.user.id}
                  </button>
                  {/* VideoContainer Component */}
                  <VideoContainer />
                </div>
              </div>
            </div>
          </main>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const userAuth = state.userAuth;
  return {
    user: userAuth.user,
    token: userAuth.token,
  };
};

export default connect(mapStateToProps, null)(CallSession);
