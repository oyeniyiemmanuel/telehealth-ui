import React, { Component } from "react";
import pusher from "pusher-js";
import Peer from "simple-peer";
import MediaHandler from "../../MediaHandler.js";
import { connect } from "react-redux";
import { getHospitalDoctors } from "../../accessors/hospital";

const APP_KEY = "418c742e4ce5c10863b2";

class VideoContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasMedia: false,
      token: localStorage.getItem("token"),
      otherUserId: null,
      user: this.props.user,
      myHospitalDoctors: [],
    };

    this.peers = {};

    this.state.user.stream = null;

    this.mediaHandler = new MediaHandler();

    this.setupPusher();

    this.callTo = this.callTo.bind(this);
    this.setupPusher = this.setupPusher.bind(this);
    this.startPeer = this.startPeer.bind(this);
  }

  UNSAFE_componentWillMount() {
    // to get all doctors from hospital
    getHospitalDoctors(this.state.user.hospital_id).then((res) => {
      this.setState({ myHospitalDoctors: res });
    });

    // to get user video
    this.mediaHandler.getPermissions().then((stream) => {
      this.setState({ hasMedia: true });
      this.state.user.stream = stream;

      try {
        this.myVideo.srcObject = stream;
      } catch (e) {
        this.myVideo.src = URL.createObjectURL(stream);
      }

      this.myVideo.play();
    });
  }
  //find out why signal isn't entering the this.channel.bind
  //fix the issue with laravel endpoint
  setupPusher() {
    this.pusher = new pusher(APP_KEY, {
      authEndpoint: "http://localhost:8000/api/pusher/auth",
      cluster: "mt1",
      auth: {
        params: this.state.user.id,
        headers: {
          Authorization: "Bearer " + this.state.token,
        },
      },
    });

    pusher.logToConsole = true;

    console.log(this.pusher, "pusher return from api/pusher/auth");

    this.channel = this.pusher.subscribe("presence-video-channel");

    this.channel.bind(`client-signal-${this.state.user.id}`, (signal) => {
      console.log(signal, "pusher bind signal");
      let peer = this.peers[signal.userId];

      //   if peer does not already exist, there is an incoming call
      if (peer === undefined) {
        this.setState({ otherUserId: signal.userId });
        peer = this.startPeer(signal.userId, false);
      }

      peer.signal(signal.data);
    });
  }

  startPeer(userId, initiator = true) {
    console.log(userId, "start peer User Id");
    const peer = new Peer({
      initiator,
      stream: this.state.user.stream,
      trickle: false,
    });

    peer.on("signal", (data) => {
      console.log(data);
      this.channel.trigger(`client-signal-${userId}`, {
        type: "signal",
        userId: this.state.user.id,
        data: data,
      });
    });

    peer.on("stream", (stream) => {
      try {
        console.log(stream, "stream in peerOnStream");
        this.userVideo.srcObject = stream;
      } catch (e) {
        console.log(stream, "stream in peerOnStream");
        this.userVideo.src = URL.createObjectURL(stream);
      }

      this.userVideo.play();
    });

    peer.on("close", () => {
      let peer = this.peers[userId];

      if (peer !== undefined) {
        peer.destroy();
      }

      peer = undefined;
    });

    return peer;
  }

  callTo(userId) {
    this.peers[userId] = this.startPeer(userId);
  }

  render() {
    const { myHospitalDoctors } = this.state;
    return (
      <div className="relative bg-gray-300 w-1/2 h-96 m-auto">
        <div className="bg-white overflow-hidden shadow rounded-lg">
          <video
            className="border-2 border-dashed border-gray-500 rounded-lg absolute bottom-0 left-0 w-1/4"
            ref={(ref) => {
              this.myVideo = ref;
            }}
          ></video>
        </div>

        <video
          className="user-video"
          ref={(ref) => {
            this.userVideo = ref;
          }}
        ></video>
        {/* this should map through the hospial doctors' list */}
        {myHospitalDoctors.map((doctor) => {
          let doctorId = doctor.id;
          return this.state.user.id !== doctorId ? (
            <button
              onClick={() => this.callTo(doctorId)}
              type="button"
              className="inline-flex items-center m-3 px-6 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-green-600 hover:bg-green-500 focus:outline-none focus:border-green-700 focus:shadow-outline-green active:bg-green-700 transition ease-in-out duration-150"
            >
              Call Dr {doctor.last_name} {doctor.first_name}
            </button>
          ) : null;
        })}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const userAuth = state.userAuth;
  console.log(userAuth.user, "video container");
  return {
    user: userAuth.user,
    token: userAuth.token,
  };
};

export default connect(mapStateToProps, null)(VideoContainer);
