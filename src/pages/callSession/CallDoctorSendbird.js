import React, { Fragment, Component } from "react";
import axios from "axios";
import generalDash from "../../hoc/GeneralDashboard";
import SendBirdCall from "sendbird-calls";
import { CONFIG } from "../../app.config";

class CallDoctor extends Component {
  constructor(props) {
    super(props);

    SendBirdCall.init(CONFIG.SENDBIRD_APP_ID);
    const sendBirdAuthOptions = {
      userId: "gbenga",
      accessToken: "1bf790832fc1d96f8e88da2c34688b785243d85c",
    };

    SendBirdCall.authenticate(sendBirdAuthOptions, (res, error) => {
      if (error) {
        console.log(error);
      } else {
        console.log(res);
      }
    });
  }

  render() {
    const { user } = this.props;
    return (
      <Fragment>
        <main
          className="flex-1 -mt-6 relative z-0 overflow-y-auto focus:outline-none"
          tabIndex="0"
        >
          <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
            <h1 className="text-2xl font-semibold text-gray-900">
              {user && user.last_name}
            </h1>
          </div>
          <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
            <div className="py-4">
              <div className="border-4 border-dashed border-gray-200 rounded-lg p-6"></div>
            </div>
          </div>
        </main>
      </Fragment>
    );
  }
}

export default generalDash(CallDoctor);
