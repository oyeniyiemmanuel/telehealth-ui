import React from "react";
import { Route, Switch } from "react-router-dom";
import NotFound from "../../components/widgets/NotFound";
import DoctorLoginForm from "../../components/auth/DoctorLoginForm";
import Dashboard from "./Dashboard";
import SelectPatients from "./SelectPatients";
import DoctorOpenStream from "../callSession/DoctorOpenStream";
import { AuthenticatedRoute } from "../../components/Routes";

const Doctors = ({ match }) => {
  return (
    <div>
      <Switch>
        <AuthenticatedRoute
          exact
          path={`${match.path}/select-patients`}
          component={SelectPatients}
        />
        <Route exact path={`${match.path}/dashboard`} component={Dashboard} />
        <AuthenticatedRoute
          exact
          path={`${match.path}/open-stream/:stream_id/:resource_id`}
          component={DoctorOpenStream}
        />
        <Route exact path={`${match.path}/login`} component={DoctorLoginForm} />
        <Route component={NotFound} />
      </Switch>
    </div>
  );
};

export default Doctors;
