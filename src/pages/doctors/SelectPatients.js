import React, { Fragment, Component } from "react";
import generalDash from "../../hoc/GeneralDashboard";
import Moment from "react-moment";
import { SmallFeedbackMessage } from "../../components/widgets/Feedbacks";
import { getMyPatientsWaitingStreams } from "../../accessors/stream";
import { PreloaderDots } from "../../components/widgets/Preloader";
import { NavLink } from "react-router-dom";
import { UnisexAvatar } from "../../components/widgets/Avatars";

class SelectPatients extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
    };

    // get doctor's waiting streams
    getMyPatientsWaitingStreams(this.props.user.id).then((res) => {
      // update in redux
      this.props.myPatientsWaitingStreamsAreFetched(res);
      this.setState({
        loading: false,
      });
    });
  }

  render() {
    const { myPatientsWaitingStreams, user } = this.props;
    const { loading } = this.state;
    return (
      <Fragment>
        <main
          className="flex-1 relative z-0 overflow-y-auto focus:outline-none"
          tabIndex="0"
        >
          <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
            <h1 className="text-2xl font-semibold text-gray-900">
              Consulting Room
            </h1>
          </div>
          <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
            <div className="py-4">
              <div className="border-4 border-dashed border-gray-200 rounded-lg p-6">
                <div className="flex flex-col">
                  <div className="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
                    <div className="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                      <table className="min-w-full">
                        <thead>
                          <tr>
                            <th className="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                              Name
                            </th>
                            <th className="px-6 py-3 border-b border-gray-200 bg-gray-50"></th>
                          </tr>
                        </thead>
                        <tbody className="bg-white">
                          {loading ? (
                            <tr>
                              <td colSpan="4">
                                <PreloaderDots message="loading patients" />
                              </td>
                            </tr>
                          ) : (
                            <Fragment>
                              {myPatientsWaitingStreams &&
                              myPatientsWaitingStreams.length !== 0 ? (
                                myPatientsWaitingStreams.map((stream) => {
                                  return (
                                    <tr
                                      key={stream.id}
                                      className="border-b border-gray-200"
                                    >
                                      <td className="px-2 py-4 md:px-6 whitespace-no-wrap">
                                        <div className="flex items-center">
                                          <div className="flex-shrink-0 h-12 w-12">
                                            <UnisexAvatar
                                              {...stream.patient.user}
                                            />
                                          </div>
                                          <div className="ml-4">
                                            <div className="text-sm leading-5 font-medium text-gray-900">
                                              {stream.patient &&
                                                stream.patient.user
                                                  .first_name}{" "}
                                              {stream.patient &&
                                                stream.patient.user.last_name}
                                            </div>
                                            <div className="text-xs leading-5 text-gray-500">
                                              <Moment fromNow>
                                                {stream.created_at}
                                              </Moment>
                                              <br />
                                              {stream.isPaid ? (
                                                stream.status ===
                                                "completed" ? (
                                                  <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                                    Completed
                                                  </span>
                                                ) : (
                                                  <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-indigo-100 text-blue-800">
                                                    Paid
                                                  </span>
                                                )
                                              ) : (
                                                <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">
                                                  Not Paid
                                                </span>
                                              )}
                                            </div>
                                          </div>
                                        </div>
                                      </td>
                                      <td className="px-2 py-4 md:px-6 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                        <span className="relative z-0 inline-flex shadow-sm">
                                          {stream.isPaid ? (
                                            stream.status === "completed" ? (
                                              ""
                                            ) : (
                                              <NavLink
                                                to={{
                                                  pathname: `/doctor/open-stream/${stream.id}/${stream.vidyo_resource_id}`,
                                                }}
                                              >
                                                <button
                                                  type="button"
                                                  className="relative inline-flex items-center px-4 py-2 rounded-md border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150"
                                                >
                                                  Open Video
                                                </button>
                                              </NavLink>
                                            )
                                          ) : (
                                            ""
                                          )}
                                        </span>
                                      </td>
                                    </tr>
                                  );
                                })
                              ) : (
                                <tr>
                                  <td colSpan="4">
                                    <SmallFeedbackMessage message="You have no waiting streams" />
                                  </td>
                                </tr>
                              )}
                            </Fragment>
                          )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                {/* <HospitalsDropdown /> */}

                {/* <VideoContainer /> */}
              </div>
            </div>
          </div>
        </main>
      </Fragment>
    );
  }
}

export default generalDash(SelectPatients);
