import React, { Component, Fragment } from "react";
import { NavLink } from "react-router-dom";
import Navbar from "../components/Navbar";
import right_bg_img from "../assets/img/right_bg.jpg";
import Tippy from "@tippyjs/react";
import "tippy.js/animations/shift-away.css";

class Home extends Component {
  render() {
    return (
      <Fragment>
        {/* Navbar Component */}
        <Navbar />

        {/* other part of the page */}
        <div id="main">
          <div className="bg-gray-100 overflow-hidden">
            <div className="relative w-full">
              <div
                className="relative mx-auto px-4 py-16 sm:px-6 lg:px-8"
                style={{
                  minHeight: "100%",
                  background: `linear-gradient(0deg, rgb(51,71,106,0.9), rgb(51,71,106,0.9)), url(${right_bg_img})`,
                  backgroundSize: "cover",
                }}
              >
                <h3 className="text-center text-3xl leading-8 font-medium tracking-tight text-white sm:text-4xl sm:leading-10">
                  Super-fast & FREE* Standalone Telehealth Platform
                </h3>
                <p className="mt-4 max-w-3xl mx-auto text-center text-xl leading-7 text-white">
                  Convenient video and audio chat for patients and doctors.
                  Register and start talking within minutes, without downloading
                  any app.
                </p>

                <div className="rounded-md m-auto p-4">
                  <div className="flex md:w-1/2 m-auto">
                    <div className="flex-shrink-0">
                      <span className="h-6 w-6 text-white">*</span>
                    </div>
                    <div className="ml-3 flex-1 md:flex md:justify-between">
                      <p className="text-sm leading-5 text-gray-400 pt-1">
                        FREE for your first 2 months to help with COVID19
                        relief, cost 10% of earnings afterwards
                      </p>
                    </div>
                  </div>
                </div>

                <div className="bg-gray-800 md:w-3/4 md:mx-auto">
                  <div className="max-w-screen-xl mx-auto py-6 px-4 sm:px-6 lg:py-6 lg:px-8 lg:flex lg:items-center">
                    <div className="lg:w-0 lg:flex-1">
                      <div className="grid md:grid-cols-3">
                        <div className="col-span-2 md:col-span-1 p-4">
                          <Tippy
                            content={
                              <div className="origin-top-right -mt-1 w-full rounded-md shadow-lg">
                                <div className="py-1 rounded-md bg-white shadow-xs">
                                  <NavLink
                                    to="/patient/login"
                                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-200 hover:text-gray-800"
                                  >
                                    As a Patient
                                  </NavLink>
                                  <NavLink
                                    to="/login"
                                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-200 hover:text-gray-800"
                                  >
                                    Hospitals & Doctors
                                  </NavLink>
                                </div>
                              </div>
                            }
                            allowHTML={true}
                            interactive={true}
                            animation="shift-away"
                            duration="[200, 500]"
                            hideOnClick={false}
                            interactiveDebounce={100}
                            placement="bottom"
                          >
                            <div className="rounded-md shadow-sm w-full text-center px-4 py-2 border border-transparent text-sm leading-6 font-medium rounded-sm text-teal-900 bg-white hover:text-teal-500 focus:outline-none focus:border-teal-700 focus:shadow-outline-teal active:bg-teal-700 transition ease-in-out duration-150">
                              Sign In
                            </div>
                          </Tippy>
                        </div>
                        <div className="col-span-2 p-4">
                          <Tippy
                            content={
                              <div className="origin-top-right -mt-1 w-full rounded-md shadow-lg">
                                <div className="py-1 rounded-md bg-white shadow-xs">
                                  <NavLink
                                    to="/patient/register/step-1"
                                    className="block px-4 py-2 text-sm bg-blue-100 text-gray-700 hover:bg-gray-200 hover:text-gray-800"
                                  >
                                    As a Patient
                                  </NavLink>
                                  <NavLink
                                    to="/register/step-1"
                                    className="block px-4 py-2 text-sm bg-blue-100 border-t border-gray-300 text-gray-700 hover:bg-gray-200 hover:text-gray-800"
                                  >
                                    Hospitals & Doctors
                                  </NavLink>
                                </div>
                              </div>
                            }
                            allowHTML={true}
                            interactive={true}
                            animation="shift-away"
                            duration="[200, 500]"
                            hideOnClick={false}
                            interactiveDebounce={100}
                            placement="bottom"
                          >
                            <div className="rounded-md shadow-sm w-full text-center px-4 py-2 border border-transparent text-sm leading-6 font-medium rounded-sm text-white bg-indigo-800 hover:text-teal-200 focus:outline-none focus:border-teal-700 focus:shadow-outline-teal active:bg-teal-700 transition ease-in-out duration-150">
                              Sign Up For FREE
                            </div>
                          </Tippy>
                        </div>
                      </div>
                    </div>
                    <div className="mt-8 lg:mt-0"></div>
                  </div>
                </div>
              </div>

              <div className="relative bg-gray-200 px-6 py-12 lg:grid lg:grid-cols-2 lg:gap-8 lg:items-center">
                <div className="relative">
                  <h4 className="text-2xl leading-8 font-extrabold text-teal-900 tracking-tight sm:text-3xl sm:leading-9">
                    FOR PATIENTS
                  </h4>
                  <ul className="mt-10">
                    <li>
                      <div className="flex">
                        <div className="flex-shrink-0">
                          <div className="flex items-center justify-center h-12 w-12 text-teal-900">
                            <svg
                              className="h-8 w-8 font-extrabold"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 24 24"
                            >
                              <path d="M7 21a4 4 0 01-4-4V5a2 2 0 012-2h4a2 2 0 012 2v12a4 4 0 01-4 4zm0 0h12a2 2 0 002-2v-4a2 2 0 00-2-2h-2.343M11 7.343l1.657-1.657a2 2 0 012.828 0l2.829 2.829a2 2 0 010 2.828l-8.486 8.485M7 17h.01"></path>
                            </svg>
                          </div>
                        </div>
                        <div className="ml-4 mt-3">
                          <h5 className="text-lg leading-6 font-medium text-teal-900">
                            Access medical care conveniently from anywhere, in
                            minutes
                          </h5>
                        </div>
                      </div>
                    </li>
                    <li className="mt-1">
                      <div className="flex">
                        <div className="flex-shrink-0">
                          <div className="flex items-center justify-center h-12 w-12 text-teal-900">
                            <svg
                              className="h-8 w-8 font-extrabold"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 24 24"
                            >
                              <path d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z"></path>
                              <path d="M15 13a3 3 0 11-6 0 3 3 0 016 0z"></path>
                            </svg>
                          </div>
                        </div>
                        <div className="ml-4 mt-3">
                          <h5 className="text-lg leading-6 font-medium text-teal-900">
                            Speak to doctors from your favourite hospitals
                          </h5>
                        </div>
                      </div>
                    </li>
                    <li className="mt-1">
                      <div className="flex">
                        <div className="flex-shrink-0">
                          <div className="flex items-center justify-center h-12 w-12 text-teal-900">
                            <svg
                              className="h-8 w-8 font-extrabold"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 24 24"
                            >
                              <path d="M18.364 18.364A9 9 0 005.636 5.636m12.728 12.728A9 9 0 015.636 5.636m12.728 12.728L5.636 5.636"></path>
                            </svg>
                          </div>
                        </div>
                        <div className="ml-4 mt-3">
                          <h5 className="text-lg leading-6 font-medium text-teal-900">
                            No need to download anything
                          </h5>
                        </div>
                      </div>
                    </li>
                    <li className="mt-1">
                      <div className="flex">
                        <div className="flex-shrink-0">
                          <div className="flex items-center justify-center h-12 w-12 text-teal-900">
                            <svg
                              className="h-8 w-8 font-extrabold"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 24 24"
                            >
                              <path d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path>
                            </svg>
                          </div>
                        </div>
                        <div className="ml-4 mt-3">
                          <h5 className="text-lg leading-6 font-medium text-teal-900">
                            Stay home and safe amidst the COVID19 pandemic
                          </h5>
                        </div>
                      </div>
                    </li>
                    <li className="mt-1">
                      <div className="flex">
                        <div className="flex-shrink-0">
                          <div className="flex items-center justify-center h-12 w-12 text-teal-900">
                            <svg
                              className="h-8 w-8 font-extrabold"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 24 24"
                            >
                              <path d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"></path>
                            </svg>
                          </div>
                        </div>
                        <div className="ml-4 mt-3">
                          <h5 className="text-lg leading-6 font-medium text-teal-900">
                            Avoid hospital acquired infections
                          </h5>
                        </div>
                      </div>
                    </li>
                    <li className="mt-1">
                      <div className="flex">
                        <div className="flex-shrink-0">
                          <div className="flex items-center justify-center h-12 w-12 text-teal-900">
                            <svg
                              className="h-8 w-8 font-extrabold"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 24 24"
                            >
                              <path d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                            </svg>
                          </div>
                        </div>
                        <div className="ml-4 mt-3">
                          <h5 className="text-lg leading-6 font-medium text-teal-900">
                            Eliminate travel time and hospital waiting rooms
                          </h5>
                        </div>
                      </div>
                    </li>
                    <li className="mt-1">
                      <div className="flex">
                        <div className="flex-shrink-0">
                          <div className="flex items-center justify-center h-12 w-12 text-teal-900">
                            <svg
                              className="h-8 w-8 font-extrabold"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 24 24"
                            >
                              <path d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                            </svg>
                          </div>
                        </div>
                        <div className="ml-4 mt-3">
                          <h5 className="text-lg leading-6 font-medium text-teal-900">
                            Pay conveniently online
                          </h5>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>

                <div className="mt-10 -mx-4 relative lg:mt-0">
                  <div>
                    <h2 className="pb-4 text-3xl tracking-tight leading-10 font-extrabold text-teal-800 text-center sm:text-1xl sm:leading-none md:text-1xl">
                      How It Works
                      <span className="ml-3 inline-flex items-center px-6 py-2 rounded-full text-xs font-medium leading-4 bg-teal-100 text-teal-800 shadow-lg">
                        Patients
                      </span>
                    </h2>
                    <div className="flex grid md:grid-cols-4 sm:grid-cols-1">
                      <div className="md:flex mx-4 md:mx-0">
                        <div className="w-full text-center">
                          <div className="bg-teal-100 rounded-lg flex items-center justify-center border border-gray-200">
                            <div className="w-1/3 bg-transparent h-20 flex items-center justify-center icon-step">
                              <svg
                                className="h-12 w-12 text-teal-900"
                                stroke="currentColor"
                                fill="none"
                                viewBox="0 0 24 24"
                              >
                                <path d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01"></path>
                              </svg>
                            </div>
                            <div className="w-2/3 bg-white h-24 flex flex-col items-center justify-center px-1 rounded-r-lg body-step">
                              <h2 className="font-bold text-sm">Sign Up</h2>
                            </div>
                          </div>
                        </div>

                        <div className="mt-2 md:hidden">
                          <svg
                            className="h-12 w-12 text-teal-900 m-auto"
                            stroke="currentColor"
                            fill="none"
                            viewBox="0 0 24 24"
                          >
                            <path d="M19 9l-7 7-7-7"></path>
                          </svg>
                        </div>
                        <div className="mt-6 hidden md:inline flex-1 flex items-center justify-center">
                          <svg
                            className="h-12 w-6 text-teal-900 font-bold"
                            stroke="currentColor"
                            fill="none"
                            viewBox="0 0 24 24"
                          >
                            <path d="M9 5l7 7-7 7"></path>
                          </svg>
                        </div>
                      </div>

                      <div className="md:flex mx-4 md:mx-0">
                        <div className="w-full text-center">
                          <div className="bg-teal-100 rounded-lg flex items-center justify-center border border-gray-200">
                            <div className="w-1/3 bg-transparent h-20 flex items-center justify-center icon-step">
                              <svg
                                className="h-12 w-12 text-teal-900"
                                stroke="currentColor"
                                fill="none"
                                viewBox="0 0 24 24"
                              >
                                <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                              </svg>
                            </div>
                            <div className="w-2/3 bg-white h-24 flex flex-col items-center justify-center px-1 rounded-r-lg body-step">
                              <h2 className="font-bold text-sm">
                                {" "}
                                Find your favourite hospitals and doctors
                              </h2>
                            </div>
                          </div>
                        </div>

                        <div className="mt-2 md:hidden">
                          <svg
                            className="h-12 w-12 text-teal-900 m-auto"
                            stroke="currentColor"
                            fill="none"
                            viewBox="0 0 24 24"
                          >
                            <path d="M19 9l-7 7-7-7"></path>
                          </svg>
                        </div>
                        <div className="mt-6 hidden md:inline flex-1 flex items-center justify-center">
                          <svg
                            className="h-12 w-6 text-teal-900 font-bold"
                            stroke="currentColor"
                            fill="none"
                            viewBox="0 0 24 24"
                          >
                            <path d="M9 5l7 7-7 7"></path>
                          </svg>
                        </div>
                      </div>

                      <div className="md:flex mx-4 md:mx-0">
                        <div className="w-full text-center">
                          <div className="bg-teal-100 rounded-lg flex items-center justify-center border border-gray-200">
                            <div className="w-1/3 bg-transparent h-20 flex items-center justify-center icon-step">
                              <svg
                                className="h-12 w-12 text-teal-900"
                                stroke="currentColor"
                                fill="none"
                                viewBox="0 0 24 24"
                              >
                                <path d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z"></path>
                              </svg>
                            </div>
                            <div className="w-2/3 bg-white h-24 flex flex-col items-center justify-center px-1 rounded-r-lg body-step">
                              <h2 className="font-bold text-sm">
                                {" "}
                                pay online{" "}
                              </h2>
                            </div>
                          </div>
                        </div>

                        <div className="mt-2 md:hidden">
                          <svg
                            className="h-12 w-12 text-teal-900 m-auto"
                            stroke="currentColor"
                            fill="none"
                            viewBox="0 0 24 24"
                          >
                            <path d="M19 9l-7 7-7-7"></path>
                          </svg>
                        </div>
                        <div className="mt-6 hidden md:inline flex-1 flex items-center justify-center">
                          <svg
                            className="h-12 w-6 text-teal-900 font-bold"
                            stroke="currentColor"
                            fill="none"
                            viewBox="0 0 24 24"
                          >
                            <path d="M9 5l7 7-7 7"></path>
                          </svg>
                        </div>
                      </div>

                      <div className="md:flex mx-4 md:mx-0">
                        <div className="w-full text-center">
                          <div className="bg-teal-100 rounded-lg flex items-center justify-center border border-gray-200">
                            <div className="w-1/3 bg-transparent h-20 flex items-center justify-center icon-step">
                              <svg
                                className="h-12 w-12 text-teal-900"
                                stroke="currentColor"
                                fill="none"
                                viewBox="0 0 24 24"
                              >
                                <path d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z"></path>
                                <path d="M15 13a3 3 0 11-6 0 3 3 0 016 0z"></path>
                              </svg>
                            </div>
                            <div className="w-2/3 bg-white h-24 flex flex-col items-center justify-center px-1 rounded-r-lg body-step">
                              <h2 className="font-bold text-sm">
                                Speak to a doctor
                              </h2>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="rounded-md shadow-sm mt-6">
                      <NavLink to="/patient/register/step-1">
                        <button className="w-3/4 ml-10 items-center px-6 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-teal-900 hover:bg-teal-500 focus:outline-none focus:border-teal-700 focus:shadow-outline-teal active:bg-teal-700 transition ease-in-out duration-150">
                          Yes, I want to Sign Up
                        </button>
                      </NavLink>
                    </div>
                  </div>
                </div>
              </div>

              <svg
                className="hidden lg:block absolute right-full transform translate-x-1/2 translate-y-12"
                width="404"
                height="784"
                fill="none"
                viewBox="0 0 404 784"
              >
                <defs>
                  <pattern
                    id="64e643ad-2176-4f86-b3d7-f2c5da3b6a6d"
                    x="0"
                    y="0"
                    width="20"
                    height="20"
                    patternUnits="userSpaceOnUse"
                  >
                    <rect
                      x="0"
                      y="0"
                      width="4"
                      height="4"
                      className="text-blue-200"
                      fill="currentColor"
                    />
                  </pattern>
                </defs>
                <rect
                  width="404"
                  height="784"
                  fill="url(#64e643ad-2176-4f86-b3d7-f2c5da3b6a6d)"
                />
              </svg>

              <div className="relative mt-12 py-12 px-6 sm:mt-16 lg:mt-24">
                <div className="lg:grid lg:grid-flow-row-dense lg:grid-cols-2 lg:gap-8 lg:items-center">
                  <div className="lg:col-start-2">
                    <h4 className="text-2xl leading-8 font-extrabold text-blue-900 tracking-tight sm:text-3xl sm:leading-9">
                      FOR HOSPITALS & DOCTORS
                    </h4>
                    <ul className="mt-10">
                      <li>
                        <div className="flex">
                          <div className="flex-shrink-0">
                            <div className="flex items-center justify-center h-12 w-12 text-blue-900">
                              <svg
                                className="h-8 w-8 font-extrabold"
                                stroke="currentColor"
                                fill="none"
                                viewBox="0 0 24 24"
                              >
                                <path d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z"></path>
                                <path d="M15 13a3 3 0 11-6 0 3 3 0 016 0z"></path>
                              </svg>
                            </div>
                          </div>
                          <div className="ml-4 mt-3">
                            <h5 className="text-lg leading-6 font-medium text-blue-900">
                              Video & Audio Chat with patients
                            </h5>
                          </div>
                        </div>
                      </li>
                      <li className="mt-1">
                        <div className="flex">
                          <div className="flex-shrink-0">
                            <div className="flex items-center justify-center h-12 w-12 text-blue-900">
                              <svg
                                className="h-8 w-8 font-extrabold"
                                stroke="currentColor"
                                fill="none"
                                viewBox="0 0 24 24"
                              >
                                <path d="M12 6V4m0 2a2 2 0 100 4m0-4a2 2 0 110 4m-6 8a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4m6 6v10m6-2a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4"></path>
                              </svg>
                            </div>
                          </div>
                          <div className="ml-4 mt-3">
                            <h5 className="text-lg leading-6 font-medium text-blue-900">
                              Stay competitive amidst the COVID19 outbreak
                            </h5>
                          </div>
                        </div>
                      </li>
                      <li className="mt-1">
                        <div className="flex">
                          <div className="flex-shrink-0">
                            <div className="flex items-center justify-center h-12 w-12 text-blue-900">
                              <svg
                                className="h-8 w-8 font-extrabold"
                                stroke="currentColor"
                                fill="none"
                                viewBox="0 0 24 24"
                              >
                                <path d="M5 3v4M3 5h4M6 17v4m-2-2h4m5-16l2.286 6.857L21 12l-5.714 2.143L13 21l-2.286-6.857L5 12l5.714-2.143L13 3z"></path>
                              </svg>
                            </div>
                          </div>
                          <div className="ml-4 mt-3">
                            <h5 className="text-lg leading-6 font-medium text-blue-900">
                              Standalone platform – use it alongside any EMR or
                              paper
                            </h5>
                          </div>
                        </div>
                      </li>
                      <li className="mt-1">
                        <div className="flex">
                          <div className="flex-shrink-0">
                            <div className="flex items-center justify-center h-12 w-12 text-blue-900">
                              <svg
                                className="h-8 w-8 font-extrabold"
                                stroke="currentColor"
                                fill="none"
                                viewBox="0 0 24 24"
                              >
                                <path d="M18.364 18.364A9 9 0 005.636 5.636m12.728 12.728A9 9 0 015.636 5.636m12.728 12.728L5.636 5.636"></path>
                              </svg>
                            </div>
                          </div>
                          <div className="ml-4 mt-3">
                            <h5 className="text-lg leading-6 font-medium text-blue-900">
                              No need to download anything
                            </h5>
                          </div>
                        </div>
                      </li>
                      <li className="mt-1">
                        <div className="flex">
                          <div className="flex-shrink-0">
                            <div className="flex items-center justify-center h-12 w-12 text-blue-900">
                              <svg
                                className="h-8 w-8 font-extrabold"
                                stroke="currentColor"
                                fill="none"
                                viewBox="0 0 24 24"
                              >
                                <path d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                              </svg>
                            </div>
                          </div>
                          <div className="ml-4 mt-3">
                            <h5 className="text-lg leading-6 font-medium text-blue-900">
                              Free & Easy Set up: Get up and running in less
                              than 5 mins
                            </h5>
                          </div>
                        </div>
                      </li>
                      <li className="mt-1">
                        <div className="flex">
                          <div className="flex-shrink-0">
                            <div className="flex items-center justify-center h-12 w-12 text-blue-900">
                              <svg
                                className="h-8 w-8 font-extrabold"
                                stroke="currentColor"
                                fill="none"
                                viewBox="0 0 24 24"
                              >
                                <path d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z"></path>
                              </svg>
                            </div>
                          </div>
                          <div className="ml-4 mt-3">
                            <h5 className="text-lg leading-6 font-medium text-blue-900">
                              See patient’s safely, minimise risk
                            </h5>
                          </div>
                        </div>
                      </li>
                      <li className="mt-1">
                        <div className="flex">
                          <div className="flex-shrink-0">
                            <div className="flex items-center justify-center h-12 w-12 text-blue-900">
                              <svg
                                className="h-8 w-8 font-extrabold"
                                stroke="currentColor"
                                fill="none"
                                viewBox="0 0 24 24"
                              >
                                <path d="M12 8c-1.657 0-3 .895-3 2s1.343 2 3 2 3 .895 3 2-1.343 2-3 2m0-8c1.11 0 2.08.402 2.599 1M12 8V7m0 1v8m0 0v1m0-1c-1.11 0-2.08-.402-2.599-1M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                              </svg>
                            </div>
                          </div>
                          <div className="ml-4 mt-3">
                            <h5 className="text-lg leading-6 font-medium text-blue-900">
                              Charge your own prices
                            </h5>
                          </div>
                        </div>
                      </li>
                      <li className="mt-1">
                        <div className="flex">
                          <div className="flex-shrink-0">
                            <div className="flex items-center justify-center h-12 w-12 text-blue-900">
                              <svg
                                className="h-8 w-8 font-extrabold"
                                stroke="currentColor"
                                fill="none"
                                viewBox="0 0 24 24"
                              >
                                <path d="M3 10h18M7 15h1m4 0h1m-7 4h12a3 3 0 003-3V8a3 3 0 00-3-3H6a3 3 0 00-3 3v8a3 3 0 003 3z"></path>
                              </svg>
                            </div>
                          </div>
                          <div className="ml-4 mt-3">
                            <h5 className="text-lg leading-6 font-medium text-blue-900">
                              Collect payments online before consults{" "}
                            </h5>
                          </div>
                        </div>
                      </li>
                      <li className="mt-1">
                        <div className="flex">
                          <div className="flex-shrink-0">
                            <div className="flex items-center justify-center h-12 w-12 text-blue-900">
                              <svg
                                className="h-8 w-8 font-extrabold"
                                stroke="currentColor"
                                fill="none"
                                viewBox="0 0 24 24"
                              >
                                <path d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                              </svg>
                            </div>
                          </div>
                          <div className="ml-4 mt-3">
                            <h5 className="text-lg leading-6 font-medium text-blue-900">
                              Set your own time limits to each session{" "}
                            </h5>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>

                  <div className="mt-10 mx-4 relative lg:mt-0 lg:col-start-1">
                    <div>
                      <h2 className="bg-gray-100 py-3 text-3xl tracking-tight leading-10 font-extrabold text-blue-900 text-center sm:text-1xl sm:leading-none md:text-1xl">
                        How It Works
                        <span className="ml-3 inline-flex items-center px-6 py-2 rounded-full text-xs font-medium leading-4 bg-blue-100 text-blue-800 shadow-lg">
                          Hospitals & Doctors
                        </span>
                      </h2>
                      <div className="flex grid md:grid-cols-4 sm:grid-cols-1">
                        <div className="md:flex mx-4 md:mx-0">
                          <div className="w-full text-center">
                            <div className="bg-blue-100 rounded-lg flex items-center justify-center border border-gray-200">
                              <div className="w-1/3 bg-transparent h-20 flex items-center justify-center icon-step">
                                <svg
                                  className="h-12 w-12 text-blue-900"
                                  stroke="currentColor"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                >
                                  <path d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01"></path>
                                </svg>
                              </div>
                              <div className="w-2/3 bg-white h-24 flex flex-col items-center justify-center px-1 rounded-r-lg body-step">
                                <h2 className="font-bold text-sm">
                                  Set up Hospital Account
                                </h2>
                              </div>
                            </div>
                          </div>

                          <div className="mt-2 md:hidden">
                            <svg
                              className="h-12 w-12 text-blue-900 m-auto"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 24 24"
                            >
                              <path d="M19 9l-7 7-7-7"></path>
                            </svg>
                          </div>
                          <div className="mt-6 hidden md:inline flex-1 flex items-center justify-center">
                            <svg
                              className="h-12 w-6 text-blue-900 font-bold"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 24 24"
                            >
                              <path d="M9 5l7 7-7 7"></path>
                            </svg>
                          </div>
                        </div>

                        <div className="md:flex mx-4 md:mx-0">
                          <div className="w-full text-center">
                            <div className="bg-blue-100 rounded-lg flex items-center justify-center border border-gray-200">
                              <div className="w-1/3 bg-transparent h-20 flex items-center justify-center icon-step">
                                <svg
                                  className="h-12 w-12 text-blue-900"
                                  stroke="currentColor"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                >
                                  <path d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"></path>
                                </svg>
                              </div>
                              <div className="w-2/3 bg-white h-24 flex flex-col items-center justify-center px-1 rounded-r-lg body-step">
                                <h2 className="font-bold text-sm">
                                  {" "}
                                  Register your doctors
                                </h2>
                              </div>
                            </div>
                          </div>

                          <div className="mt-2 md:hidden">
                            <svg
                              className="h-12 w-12 text-blue-900 m-auto"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 24 24"
                            >
                              <path d="M19 9l-7 7-7-7"></path>
                            </svg>
                          </div>
                          <div className="mt-6 hidden md:inline flex-1 flex items-center justify-center">
                            <svg
                              className="h-12 w-6 text-blue-900 font-bold"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 24 24"
                            >
                              <path d="M9 5l7 7-7 7"></path>
                            </svg>
                          </div>
                        </div>

                        <div className="md:flex mx-4 md:mx-0">
                          <div className="w-full text-center">
                            <div className="bg-blue-100 rounded-lg flex items-center justify-center border border-gray-200">
                              <div className="w-1/3 bg-transparent h-20 flex items-center justify-center icon-step">
                                <svg
                                  className="h-12 w-12 text-blue-900"
                                  stroke="currentColor"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                >
                                  <path d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"></path>
                                </svg>
                              </div>
                              <div className="w-2/3 bg-white h-24 flex flex-col items-center justify-center px-1 rounded-r-lg body-step">
                                <h2 className="font-bold text-sm">
                                  {" "}
                                  Invite patients{" "}
                                </h2>
                              </div>
                            </div>
                          </div>

                          <div className="mt-2 md:hidden">
                            <svg
                              className="h-12 w-12 text-blue-900 m-auto"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 24 24"
                            >
                              <path d="M19 9l-7 7-7-7"></path>
                            </svg>
                          </div>
                          <div className="mt-6 hidden md:inline flex-1 flex items-center justify-center">
                            <svg
                              className="h-12 w-6 text-blue-900 font-bold"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 24 24"
                            >
                              <path d="M9 5l7 7-7 7"></path>
                            </svg>
                          </div>
                        </div>

                        <div className="md:flex mx-4 md:mx-0">
                          <div className="w-full text-center">
                            <div className="bg-blue-100 rounded-lg flex items-center justify-center border border-gray-200">
                              <div className="w-1/3 bg-transparent h-20 flex items-center justify-center icon-step">
                                <svg
                                  className="h-12 w-12 text-blue-900"
                                  stroke="currentColor"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                >
                                  <path d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z"></path>
                                  <path d="M15 13a3 3 0 11-6 0 3 3 0 016 0z"></path>
                                </svg>
                              </div>
                              <div className="w-2/3 bg-white h-24 flex flex-col items-center justify-center px-1 rounded-r-lg body-step">
                                <h2 className="font-bold text-sm">
                                  Start consulting
                                </h2>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="rounded-md shadow-sm mt-6">
                        <NavLink to="/register/step-1">
                          <button className="w-3/4 ml-10 items-center px-6 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-blue-900 hover:bg-blue-500 focus:outline-none focus:border-blue-700 focus:shadow-outline-blue active:bg-blue-700 transition ease-in-out duration-150">
                            Start My Telemedicine Journey
                          </button>
                        </NavLink>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Home;
