import { ACTIONS } from "../../app.config";

export const authenticated = (user, token) => ({
  type: ACTIONS.IS_LOGGED_IN,
  user,
  token,
});
export const userFetched = (user) => ({
  type: ACTIONS.USER_IS_FETCHED,
  user,
});
export const streamsFetched = (stream) => ({
  type: ACTIONS.STREAMS_ARE_FETCHED,
  stream,
});
export const specialtiesFetched = (specialties) => ({
  type: ACTIONS.SPECIALTIES_ARE_FETCHED,
  specialties,
});
export const streamFetched = (stream) => ({
  type: ACTIONS.STREAM_IS_FETCHED,
  stream,
});
export const fetchHospitalDoctors = (doctors) => ({
  type: ACTIONS.FETCH_HOSPITAL_DOCTORS,
  doctors,
});
export const fetchHospitalPatients = (patients) => ({
  type: ACTIONS.FETCH_HOSPITAL_PATIENTS,
  patients,
});
export const fetchHospitalTeamMembers = (teamMembers) => ({
  type: ACTIONS.FETCH_HOSPITAL_TEAM_MEMBERS,
  teamMembers,
});
export const fetchHospitalEarningsData = (earningsData) => ({
  type: ACTIONS.FETCH_HOSPITAL_EARNINGS_DATA,
  earningsData,
});
export const fetchWaitingVideoNotifications = (notifications) => ({
  type: ACTIONS.FETCH_MY_WAITING_VIDEO_NOTIFICATIONS,
  notifications,
});
export const myPatientsWaitingStreamsAreFetched = (streams) => ({
  type: ACTIONS.MY_PATIENTS_WAITING_STREAMS_ARE_FETCHED,
  streams,
});
export const allHospitalsFetched = (hospitals) => ({
  type: ACTIONS.ALL_HOSPITALS_FETCHED,
  hospitals,
});
export const hospitalCreated = (hospital) => ({
  type: ACTIONS.HOSPITAL_CREATED,
  hospital,
});
