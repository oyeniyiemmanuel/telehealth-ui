import { ACTIONS } from "../../app.config";

// initial state
const initialState = {
  specialties: [],
};

const specialtyReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SPECIALTIES_ARE_FETCHED: {
      return {
        specialties: action.specialties,
      };
    }

    default:
      return state;
  }
};

export default specialtyReducer;
