import { ACTIONS } from "../../app.config";

// initial state
const initialState = {
  myPatientsWaitingStreams: [],
  myWaitingVideoNotifications: [],
  streams: [],
  stream: [],
};

const streamReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.STREAM_IS_FETCHED: {
      return {
        ...state,
        stream: action.stream,
      };
    }
    case ACTIONS.STREAMS_ARE_FETCHED: {
      return {
        ...state,
        streams: action.streams,
      };
    }
    case ACTIONS.MY_PATIENTS_WAITING_STREAMS_ARE_FETCHED: {
      return {
        ...state,
        myPatientsWaitingStreams: action.streams,
      };
    }
    case ACTIONS.FETCH_MY_WAITING_VIDEO_NOTIFICATIONS: {
      return {
        ...state,
        myWaitingVideoNotifications: action.notifications,
      };
    }

    default:
      return state;
  }
};

export default streamReducer;
