import { combineReducers } from "redux";
import userAuthReducer from "./userAuth";
import hospitalReducer from "./hospital";
import streamReducer from "./stream";
import specialtyReducer from "./specialty";

export default combineReducers({
  userAuth: userAuthReducer,
  hospital: hospitalReducer,
  streamReducer: streamReducer,
  specialtyReducer: specialtyReducer,
});
