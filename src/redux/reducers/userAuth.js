import { ACTIONS } from "../../app.config";

// initial state
const initialState = {
  isLoggedIn: false,
};

const userAuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.IS_LOGGED_IN: {
      let isLoggedIn = !state.isLoggedIn;
      return {
        isLoggedIn,
        user: action.user,
        token: action.token,
      };
    }
    case ACTIONS.USER_IS_FETCHED: {
      return {
        user: action.user,
      };
    }

    default:
      return state;
  }
};

export default userAuthReducer;
