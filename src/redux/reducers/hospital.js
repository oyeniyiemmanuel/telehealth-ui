import { ACTIONS } from "../../app.config";

// initial state
const initialState = {
  doctors: [],
  teamMembers: [],
  earningsData: [],
  hospitals: [],
};

const hospitalReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.FETCH_HOSPITAL_DOCTORS: {
      return { ...state, doctors: action.doctors };
    }
    case ACTIONS.FETCH_HOSPITAL_PATIENTS: {
      return { ...state, patients: action.patients };
    }
    case ACTIONS.FETCH_HOSPITAL_TEAM_MEMBERS: {
      return { ...state, teamMembers: action.teamMembers };
    }
    case ACTIONS.FETCH_HOSPITAL_EARNINGS_DATA: {
      return { ...state, earningsData: action.earningsData };
    }
    case ACTIONS.ALL_HOSPITALS_FETCHED: {
      return { ...state, hospitals: action.hospitals };
    }

    default:
      return state;
  }
};

export default hospitalReducer;
