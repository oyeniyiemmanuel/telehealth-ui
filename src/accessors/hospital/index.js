import axios from "axios";
import { CONFIG } from "../../app.config";

const getHospitalDoctors = (hospitalId) => {
  let hospital_data = { hospital_id: hospitalId };
  return axios.post(CONFIG.SERVER_API + "hospital/doctors", hospital_data).then(
    (res) => {
      return res.data.doctors;
    },
    (err) => {
      console.log(err);
    }
  );
};

const getHospitalPatients = (hospitalId) => {
  let hospital_data = { hospital_id: hospitalId };
  return axios
    .post(CONFIG.SERVER_API + "hospital/patients", hospital_data)
    .then(
      (res) => {
        return res.data.patients;
      },
      (err) => {
        console.log(err);
      }
    );
};

const getHospitalTeamMembers = (hospitalId) => {
  let hospital_data = { hospital_id: hospitalId };
  return axios
    .post(CONFIG.SERVER_API + "hospital/team-members", hospital_data)
    .then(
      (res) => {
        return res.data.teamMembers;
      },
      (err) => {
        console.log(err);
      }
    );
};

const getHospitalEarningsData = (hospitalId) => {
  let hospital_data = { hospital_id: hospitalId };
  return axios
    .post(CONFIG.SERVER_API + "hospital/earnings-data", hospital_data)
    .then(
      (res) => {
        console.log(res.data);
        return res.data;
      },
      (err) => {
        console.log(err);
      }
    );
};

const getAllHospitals = () => {
  return axios.post(CONFIG.SERVER_API + "hospital/all", null).then(
    (res) => {
      return res.data.hospitals;
    },
    (err) => {
      console.log(err);
    }
  );
};

export {
  getHospitalDoctors,
  getHospitalPatients,
  getAllHospitals,
  getHospitalTeamMembers,
  getHospitalEarningsData,
};
