import axios from "axios";
import { CONFIG } from "../../app.config";

const getDoctorDetails = (doctorId) => {
  const doctor_data = { doctor_id: doctorId };

  return axios.post(CONFIG.SERVER_API + "doctor/get-doctor", doctor_data).then(
    (res) => {
      return res.data.doctor[0];
    },
    (err) => {
      console.log(err);
    }
  );
};

export { getDoctorDetails };
