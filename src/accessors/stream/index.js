import axios from "axios";
import { CONFIG } from "../../app.config";

const getMyPatientsWaitingStreams = (userId) => {
  console.log(userId);
  // make sure the value of userId is a user->user_id, not user->id because the server side searches for primary key on user's Model not on User's Model
  let user_data = { user_id: userId };
  return axios
    .post(CONFIG.SERVER_API + "stream/doctor/my-waiting-streams", user_data)
    .then(
      (res) => {
        return res.data.streams;
      },
      (err) => {
        console.log(err);
      }
    );
};

const getStreamById = (streamId) => {
  let stream_data = { stream_id: streamId };
  return axios.post(CONFIG.SERVER_API + "stream/get-by-id", stream_data).then(
    (res) => {
      console.log(res.data.stream);
      return res.data.stream[0];
    },
    (err) => {
      console.log(err);
    }
  );
};

const getStreamByResourceId = (resourceId) => {
  let stream_data = { resource_id: resourceId };
  return axios
    .post(CONFIG.SERVER_API + "stream/get-by-resource_id", stream_data)
    .then(
      (res) => {
        console.log(res.data.stream);
        return res.data.stream[0];
      },
      (err) => {
        console.log(err);
      }
    );
};

export { getMyPatientsWaitingStreams, getStreamById, getStreamByResourceId };
