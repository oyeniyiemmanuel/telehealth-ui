import axios from "axios";
import { CONFIG } from "../../app.config";

const getLoggedInUser = (token) => {
  const options = {
    headers: { Authorization: "Bearer " + token },
  };

  return axios.post(CONFIG.SERVER_API + "get-user", null, options).then(
    (res) => {
      return res.data.user;
    },
    (err) => {
      console.log(err);
    }
  );
};

const getWaitingVideoNotifications = (userId) => {
  let user_data = { user_id: userId };
  return axios
    .post(
      CONFIG.SERVER_API + "user/get-incoming-video-notifications",
      user_data
    )
    .then(
      (res) => {
        return res.data.notifications;
      },
      (err) => {
        console.log(err);
      }
    );
};

const markVideoNotificationsAsSeen = () => {
  let token = localStorage.getItem("token");

  (async () => {
    const user = await getLoggedInUser(token);

    let user_data = { user_id: user.id };
    return axios
      .post(
        CONFIG.SERVER_API + "user/mark-video-notifications-as-seen",
        user_data
      )
      .then(
        (res) => {
          // redirect to patients waiting list for doctor
          window.location = `${CONFIG.CLIENT_URL}doctor/select-patients`;

          return res.data.notifications;
        },
        (err) => {
          console.log(err);
        }
      );
  })();
};

const logoutUser = (userId) => {
  let user_data = { user_id: userId };

  return axios.post(CONFIG.SERVER_API + "logout", user_data).then(
    (res) => {
      return res.data.user;
    },
    (err) => {
      console.log(err);
    }
  );
};

export {
  markVideoNotificationsAsSeen,
  getWaitingVideoNotifications,
  getLoggedInUser,
  logoutUser,
};
