import axios from "axios";
import { CONFIG } from "../../app.config";

const getAllSpecialties = () => {
  return axios.post(CONFIG.SERVER_API + "specialty/all", null).then(
    (res) => {
      return res.data.specialties;
    },
    (err) => {
      console.log(err);
    }
  );
};

const getHospitalSpecialties = (hospitalId) => {
  let hospital_data = { hospital_id: hospitalId };
  return axios
    .post(CONFIG.SERVER_API + "specialty/hospital", hospital_data)
    .then(
      (res) => {
        return res.data.specialties;
      },
      (err) => {
        console.log(err);
      }
    );
};

export { getAllSpecialties, getHospitalSpecialties };
