import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import SideNav from "../components/widgets/SideNav";
import Navbar from "../components/Navbar";
import { getLoggedInUser } from "../accessors/user";
import { PreloaderLines } from "../components/widgets/Preloader";
import { compose } from "redux";
import {
  userFetched,
  streamFetched,
  streamsFetched,
  specialtiesFetched,
  fetchHospitalDoctors,
  fetchHospitalPatients,
  fetchHospitalTeamMembers,
  fetchHospitalEarningsData,
  fetchWaitingVideoNotifications,
  myPatientsWaitingStreamsAreFetched,
} from "../redux/actions";
import {
  getHospitalDoctors,
  getHospitalPatients,
  getHospitalTeamMembers,
  getHospitalEarningsData,
} from "../accessors/hospital";
import { getWaitingVideoNotifications } from "../accessors/user";
import { getHospitalSpecialties } from "../accessors/specialty";

const generalDash = (PassedComponent) => {
  return class extends Component {
    constructor(props) {
      super(props);

      // get the current user details
      let token = localStorage.getItem("token");

      getLoggedInUser(token).then((res) => {
        // trigger redux action to add user data to store
        this.props.userFetched(res);

        // get all waiting video notifications for doctors at some time interval
        if (this.props.user && this.props.user.doctor_id) {
          setInterval(async () => {
            const notifications = await getWaitingVideoNotifications(
              this.props.user.id
            );
            // save in redux store
            props.fetchWaitingVideoNotifications(notifications);
          }, 10000);
        }

        // run these functions if user has been attached to a hospital
        if (res && res.hospital !== null) {
          // get the current user's hospital doctors
          getHospitalDoctors(res.hospital.id).then((res) => {
            // trigger redux action to add doctors data to store
            this.props.fetchHospitalDoctors(res);
          });
          // get the current user's hospital team members
          getHospitalTeamMembers(res.hospital.id).then((res) => {
            // trigger redux action to add team members data to store
            this.props.fetchHospitalTeamMembers(res);
          });

          // get the current user's hospital patients
          getHospitalPatients(res.hospital.id).then((res) => {
            // trigger redux action to add patients data to store
            this.props.fetchHospitalPatients(res);
          });

          // get the current user's hospital earnings data
          getHospitalEarningsData(res.hospital.id).then((res) => {
            // trigger redux action to add earnings data to store
            this.props.fetchHospitalEarningsData(res);
          });

          // get all specialties asynchronously and wait till the promise gets resolved before assigning the variable
          (async () => {
            const specialties = await getHospitalSpecialties(res.hospital.id);
            // save in redux store
            this.props.specialtiesFetched(specialties);
          })();
        }
      });
    }
    render() {
      const user = this.props.user;
      if (user !== undefined) {
        return (
          <Fragment>
            {/* Navbar Component */}
            <Navbar {...this.props} />

            {/* Other part of the page */}
            <div className="h-screen flex overflow-hidden bg-gray-100 pb-12">
              <SideNav {...this.props} />

              <main
                className="flex-1 relative z-0 overflow-y-auto py-6 focus:outline-none"
                tabIndex="0"
              >
                <PassedComponent {...this.props} />
              </main>
            </div>
          </Fragment>
        );
      } else {
        return <PreloaderLines />;
      }
    }
  };
};

const mapStateToProps = (state) => {
  return {
    user: state.userAuth.user,
    doctors: state.hospital.doctors,
    myHospitalPatients: state.hospital.patients,
    teamMembers: state.hospital.teamMembers,
    hospitalEarningsData: state.hospital.earningsData,
    stream: state.streamReducer.stream,
    myPatientsWaitingStreams: state.streamReducer.myPatientsWaitingStreams,
    myWaitingVideoNotifications:
      state.streamReducer.myWaitingVideoNotifications,
    specialties: state.specialtyReducer.specialties,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userFetched: (user) => dispatch(userFetched(user)),
    streamFetched: (stream) => dispatch(streamFetched(stream)),
    streamsFetched: (streams) => dispatch(streamsFetched(streams)),
    myPatientsWaitingStreamsAreFetched: (streams) =>
      dispatch(myPatientsWaitingStreamsAreFetched(streams)),
    specialtiesFetched: (specialties) =>
      dispatch(specialtiesFetched(specialties)),
    fetchHospitalDoctors: (user) => dispatch(fetchHospitalDoctors(user)),
    fetchHospitalPatients: (patients) =>
      dispatch(fetchHospitalPatients(patients)),
    fetchHospitalEarningsData: (earningsData) =>
      dispatch(fetchHospitalEarningsData(earningsData)),
    fetchHospitalTeamMembers: (members) =>
      dispatch(fetchHospitalTeamMembers(members)),
    fetchWaitingVideoNotifications: (notifications) =>
      dispatch(fetchWaitingVideoNotifications(notifications)),
  };
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  generalDash
);
