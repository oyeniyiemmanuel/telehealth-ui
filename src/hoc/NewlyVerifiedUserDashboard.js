import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import SideNav from "../components/widgets/SideNav";
import Navbar from "../components/Navbar";
import { getLoggedInUser } from "../accessors/user";
import {
  userFetched,
  streamFetched,
  streamsFetched,
  specialtiesFetched,
} from "../redux/actions";
import { PreloaderLines } from "../components/widgets/Preloader";
import { compose } from "redux";
import {
  fetchHospitalDoctors,
  myPatientsWaitingStreamsAreFetched,
} from "../redux/actions";

const newlyVerifiedUserDash = (PassedComponent) => {
  return class extends Component {
    constructor(props) {
      super(props);

      let token = props.match.params.token;

      // save token in localstorage
      localStorage.setItem("token", token);

      getLoggedInUser(token).then((res) => {
        // trigger redux action to add user data to store
        props.userFetched(res);

        if (localStorage.getItem("token") !== undefined) {
          res.doctor_id
            ? props.history.push("/settings/doctor/complete-profile")
            : props.history.push("/register/step-2");
        }
      });
    }
    render() {
      const user = this.props.user;
      if (user !== undefined) {
        return (
          <Fragment>
            {/* Navbar Component */}
            <Navbar {...user} />

            {/* Other part of the page */}
            <div className="h-screen flex overflow-hidden bg-gray-100 pb-12">
              <SideNav {...user} />

              <main
                className="flex-1 relative z-0 overflow-y-auto py-6 focus:outline-none"
                tabIndex="0"
              >
                <PassedComponent {...this.props} />
              </main>
            </div>
          </Fragment>
        );
      } else {
        return <PreloaderLines />;
      }
    }
  };
};

const mapStateToProps = (state) => {
  return {
    user: state.userAuth.user,
    doctors: state.hospital.doctors,
    stream: state.streamReducer.stream,
    myPatientsWaitingStreams: state.streamReducer.myPatientsWaitingStreams,
    specialties: state.specialtyReducer.specialties,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userFetched: (user) => dispatch(userFetched(user)),
    streamFetched: (stream) => dispatch(streamFetched(stream)),
    streamsFetched: (streams) => dispatch(streamsFetched(streams)),
    myPatientsWaitingStreamsAreFetched: (streams) =>
      dispatch(myPatientsWaitingStreamsAreFetched(streams)),
    specialtiesFetched: (specialties) =>
      dispatch(specialtiesFetched(specialties)),
    fetchHospitalDoctors: (user) => dispatch(fetchHospitalDoctors(user)),
  };
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  newlyVerifiedUserDash
);
