import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import SideNav from "../components/widgets/SideNav";
import { getLoggedInUser } from "../accessors/user";
import { userFetched, streamFetched } from "../redux/actions";
import { PreloaderLines } from "../components/widgets/Preloader";
import { compose } from "redux";

const doctorsDash = (PassedComponent) => {
  return class extends Component {
    constructor(props) {
      super(props);
      let token = localStorage.getItem("token");

      // get the current user details
      getLoggedInUser(token).then((res) => {
        // trigger redux action to add user data to store
        this.props.userFetched(res);
      });
    }
    render() {
      const user = this.props.user;
      if (user !== undefined && user.hospital_id !== null) {
        return (
          <Fragment>
            <div className="h-screen flex overflow-hidden bg-gray-100 pb-12">
              <SideNav {...user} />

              <main
                className="flex-1 relative z-0 overflow-y-auto py-6 focus:outline-none"
                tabIndex="0"
              >
                <PassedComponent {...this.props} />
              </main>
            </div>
          </Fragment>
        );
      } else {
        return <PreloaderLines />;
      }
    }
  };
};

const mapStateToProps = (state) => {
  return {
    user: state.userAuth.user,
    stream: state.streamReducer.stream,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userFetched: (user) => dispatch(userFetched(user)),
    streamFetched: (stream) => dispatch(streamFetched(stream)),
  };
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  doctorsDash
);
